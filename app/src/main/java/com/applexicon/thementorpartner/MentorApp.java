package com.applexicon.thementorpartner;

import android.app.Application;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.utilities.LanguageUtils;

/**
 * Created by Hamdy on 8/1/2017.
 */

public class MentorApp extends Application {


    public Mentor mentor;

    @Override
    public void onCreate() {
        super.onCreate();

        mentor = Mentor.getInstance();

        mentor.fillUserData(getApplicationContext());

        String lang = LanguageUtils.getUsedLang(getApplicationContext());

        LanguageUtils.setLanguage(getApplicationContext() , lang);




    }
}
