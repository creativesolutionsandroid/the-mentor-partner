package com.applexicon.thementorpartner.network;


import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.utilities.Constants;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
 
    public static final String BASE_URL = Constants.BASE_URL;
    private static Retrofit retrofit = null;
    private static String TAG="ApiClient";


    public static Retrofit getClient(final Context context) {

        if (retrofit==null) {

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES);
            // add your other interceptors …

            // add logging as last interceptor
            httpClient.addInterceptor(logging);  // <-- this is the important line!

            Log.e(TAG , "token : " + Mentor.getInstance().token);

            httpClient.addInterceptor(new Interceptor() {
                                          @Override
                                          public Response intercept(Interceptor.Chain chain) throws IOException {
                                              //to disable menu toggle button while request is processing
                                              /*if (context instanceof MainActivity) {
                                                  MainActivity.enableToggle(false);
                                              }*/
                                              Request original = chain.request();

                                              Request request = original.newBuilder()
                                                      .header("authorization", "Bearer " +Mentor.getInstance().token)
                                                      .header("lang", LanguageUtils.getUsedLang(context))
                                                      .method(original.method(), original.body())
                                                      .build();

                                              Response response = chain.proceed(request);

                                              /*//to enable menu toggle button after processing
                                              if (context instanceof MainActivity) {
                                                  MainActivity.enableToggle(true);
                                              }
*/
                                              return response;
                                          }
                                      }
            );

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;


    }


}