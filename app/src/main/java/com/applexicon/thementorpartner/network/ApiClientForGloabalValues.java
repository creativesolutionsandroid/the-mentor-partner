package com.applexicon.thementorpartner.network;



import com.applexicon.thementorpartner.utilities.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientForGloabalValues {

    public static String BASE_URL = Constants.BASE_URL_GLOBAL_VALUES;
    private static Retrofit retrofit = null;
    private static String TAG = "ApiClient";

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

//    public static Retrofit getClient(final Context context) {
//
//        if (retrofit == null) {
//
//            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//            // set your desired log level
//            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
//                    .connectTimeout(5, TimeUnit.MINUTES)
//                    .readTimeout(5, TimeUnit.MINUTES);
//            // add your other interceptors …
//
//            // add logging as last interceptor
//            httpClient.addInterceptor(logging);  // <-- this is the important line!
//
//
//            httpClient.addInterceptor(new Interceptor() {
//                                          @Override
//                                          public Response intercept(Chain chain) throws IOException {
//
//                                         /*     //to disable menu toggle button while request is processing
//                                              if (context instanceof MainActivity) {
//                                                  MainActivity.enableToggle(false);
//                                              }*/
//                                              Request original = chain.request();
//
//                                              Request request = original.newBuilder()
//                                                      .header("authorization", "Bearer " + Student.getInstance().token)
//                                                      .header("lang", LanguageUtils.getUsedLang(context))
//                                                      .method(original.method(), original.body())
//                                                      .build();
//                                              Response response = chain.proceed(request);
//
//                                            /*  //to enable menu toggle button after processing
//                                              if (context instanceof MainActivity) {
//                                                  MainActivity.enableToggle(true);
//                                              }*/
//                                              return response;
//
//                                          }
//                                      }
//            );
//
//
//            Gson gson = new GsonBuilder()
//                    .setLenient()
//                    .create();
//
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                    .client(httpClient.build())
//                    .build();
//        }
//        return retrofit;
//    }
}