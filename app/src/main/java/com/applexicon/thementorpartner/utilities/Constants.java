package com.applexicon.thementorpartner.utilities;

import java.text.DecimalFormat;

/**
 * Created by Hamdy on 8/2/2017.
 */

public class Constants {

    //PC-IP: 192.168.1.5

    //Base Url :: http://PC-IP/mentor/public/api/v1
    //Student App :: Base Url/student
    //Mentor App :: Base Url/teacher

    public static final DecimalFormat distanceFormat = new DecimalFormat("##,##,##0.00");
    //public final static String IP = "http://18.220.121.119";
    public final static String IP = "http://www.thementoredu.com";
    //public final static String BASE_URL = IP + "/mentor/tst/public/api/v1/teacher/";
    public final static String BASE_URL = IP+"/api/v1/teacher/";
    public final static String BASE_URL_GLOBAL_VALUES = IP+"/api/v1/";
  /*  public final static String IP = "http://192.168.1.250";
    public final  static String BASE_URL =     IP +"/mentor/public/api/v1/teacher/";*/

    public static String convertToArabic(String value) {
        String newValue = (value
                .replaceAll("١", "1").replaceAll("٢", "2")
                .replaceAll("٣", "3").replaceAll("٤", "4")
                .replaceAll("٥", "5").replaceAll("٦", "6")
                .replaceAll("٧", "7").replaceAll("٨", "8")
                .replaceAll("٩", "9").replaceAll("٠", "0")
                .replaceAll("٫", ".").replaceAll(",", "."));
        return newValue;
    }

    public final static String GLOBAL_VALUES = "allGlobalValues";
    public static final String GET_CITIES = "cities";

    public final static String  LOGIN_POST_URL = "auth/login";
    public final static String  REGISTER_POST_URL =  "auth/register";
    public final static String  VERIFY_PHONE_POST_URL = "auth/verifyPhone";
    public final static String  LOGOUT_POST_URL = "auth/logout";
    public final static String  CHANGE_PASSWORD_POST_URL = "auth/changePassword";

    public static final String RESEND = "auth/resendSMS";
    public static final String FORGOT_PASSWORD = "auth/forgotPassword";
    public static final String OFFICE_DETAILS_GET_URL ="getOfficeDetails";
    public static final String IMAGES_URL = BASE_URL +  "images/";
    public static final String RESET_FORGOT_PASSWORD_POST_URL = "auth/resetForgotPassword";
    public static final String GET_COURSES = "getCourses";
    public static final String FETCH_SESSIONS = "getSessionRequests";
    public static final String ACCEPT_URL_POST = "acceptSession";
    public static final String REJECT_URL_POST = "rejectSession";
    public static final String FETCH_PAYMENT_HISTORY = "paymentHistory";
//    public static final String FETCH_PAYMENT_HISTORY = "sessionPaymentHistory";
    public static final String FETCH_PROFILE_URL_GET = "getTeacherProfile";
    public static final String FETCH_AVAILABLE_URL_GET = "getTeacherStatus";
    public static final String CHANGE_AVAILABLE_URL_GET = "changeStatus";
    public static final String FETCH_NOTIFICATION_STATUS_URL_GET = "getNotificationStatus";
    public static final String CHANGE_NOTIFICATION_URL_GET = "changeNotificationStatus";
    public static final String EDIT_PROFILE_POST_URL = "editProfile";
    public static final String FETCH_DAY_SESSIONS = "getSessionsInDay";
    public static final String FETCH_UPCOMING_EVENTS = "getUpComingSessionDays";
    public static final String FETCH_SESSION_DETAILS = "getSessionInfo";
    public static final String FETCH_CONFIRMED_SESSIONS = "confirmedSessions";
    public static final String FETCH_COMPLETED_SESSIONS = "completedSessions";
    public static final String FETCH_CANCELED_SESSIONS = "cancelledSessions";
    public static final String START_URL_POST = "start-session";
    public static final String CANCEL_URL_POST = "cancelSession";
    public static final String PAYMNET_RECEIVED = "paymentReceived";
    public static final String PAYMNET_REJECTED = "paymentNotReceived";
    public static final String SESSION_SUMMARY = "getSessionSummary";
    public static final String ADD_REVIEW = "addReviews";
    public static final String END_URL_POST = "endSession";
    public static final String SEND_FIREBASE_TOKEN = "storeDeviceToken";
    public static final String HELP_URL = IP + "/api/v1/help?key=%s&lang=%s&type=";
    public static final String PAYMENT_REQUEST = "requestPay";
    public static final String CHANGE_LANG = "changeLang";
    public static final String REVIEWS_URL_GET = "getReviews";

    public static int payment_percentage = 0;
    public static int monthly_discount_percentage = 0;
    public static String completedSessionId = "0";

}
