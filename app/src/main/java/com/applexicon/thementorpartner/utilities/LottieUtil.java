package com.applexicon.thementorpartner.utilities;

import android.view.View;

/**
 * Created by Hamdy on 6/13/2017.
 */

public class LottieUtil {


    public static void hideAnimation(View view){

        view.setVisibility(View.GONE);
    }


    public static void showAnimation(View view){

        view.setVisibility(View.VISIBLE);
    }


}
