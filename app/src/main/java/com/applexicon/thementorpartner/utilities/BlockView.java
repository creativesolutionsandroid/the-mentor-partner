package com.applexicon.thementorpartner.utilities;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by Hamdy on 11/5/2017.
 */

public class BlockView {

    public static void enableView(Activity activity , View view , boolean enable) {

        if (activity == null)
            return;

        if(enable){
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        }
        else {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        if(view != null) {
            view.setEnabled(enable);
            view.setAlpha(enable ? 1f : 0.5f);
        }
    }
}
