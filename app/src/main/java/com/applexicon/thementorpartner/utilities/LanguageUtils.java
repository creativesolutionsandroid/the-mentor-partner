package com.applexicon.thementorpartner.utilities;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by hamdy on 29/01/17.
 */
public class LanguageUtils {


    public static final String LANGUAGE = "language";

    public static String getUsedLang(Context context) {

        //return Locale.getDefault().getLanguage().toString();

        String usedLang = SharedPref.getString(context , LANGUAGE);
       return usedLang != null ? usedLang : Locale.getDefault().getLanguage().toString();
    }

    public static void changeToArabic(Context context) {



        String languageToLoad  = "ar";
        Locale locale = new Locale(languageToLoad);

        Locale.setDefault(locale);

        Configuration config = new Configuration();

        config.setLocale( locale);

        context.getResources().updateConfiguration(config,context.getResources().getDisplayMetrics());


        SharedPref.putString(context , LANGUAGE ,languageToLoad );
    }

    public static void changeToEnglish(Context context) {


        String languageToLoad  = "en";
        Locale locale = new Locale(languageToLoad);

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale( locale);

//        context.createConfigurationContext(config);
        context.getResources().updateConfiguration(config,context.getResources().getDisplayMetrics());

        SharedPref.putString(context , LANGUAGE ,languageToLoad );
    }

    public static void setLanguage(Context context, String lang) {

        String languageToLoad  = lang;
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);

        Configuration config = new Configuration();
        config.setLocale(locale);

        context.createConfigurationContext(config);

        context.getResources().updateConfiguration(config,context.getResources().getDisplayMetrics());


//        Configuration overrideConfiguration = context.getResources().getConfiguration();
//        overrideConfiguration.setLocale(locale);
//
//        Context context  = createConfigurationContext(overrideConfiguration);
//        Resources resources = context.getResources();

        SharedPref.putString(context , LANGUAGE ,languageToLoad );

    }
}
