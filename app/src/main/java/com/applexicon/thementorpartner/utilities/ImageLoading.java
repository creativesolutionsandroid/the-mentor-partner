package com.applexicon.thementorpartner.utilities;

import androidx.databinding.ObservableInt;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class ImageLoading {


    private static final String TAG = "ImageLoading";

    public static void loadImage(final ImageView view, String url, final int DrawableError ,final ObservableInt progressBar) {

        if (url != null && !url.equals("")) {

            String imageUrl = Constants.IP + "/mentor/tst/" + url;
            Log.e(TAG, "Image url : " + imageUrl);
            progressBar.set(View.VISIBLE);

            Glide.with(view.getContext())
                    .load(imageUrl)
                    .error(DrawableError)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progressBar.set(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.set(View.GONE);
                            return false;
                        }
                    })
                    .into(view);
        }
    }

    public static void loadImage(final ImageView view, String url, final int DrawableError ,final View  progressBar) {

        if (url != null && !url.equals("")) {

            String imageUrl = Constants.IP + "/mentor/tst/" + url;
            Log.e(TAG, "Image url : " + imageUrl);
            progressBar.setVisibility(View.VISIBLE);

            Glide.with(view.getContext())
                    .load(imageUrl)
                    .error(DrawableError)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(view);
        }
    }
}