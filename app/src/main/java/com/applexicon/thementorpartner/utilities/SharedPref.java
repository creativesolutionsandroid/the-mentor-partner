package com.applexicon.thementorpartner.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Hamdy on 5/7/2017.
 */

public class SharedPref {


    private static final String PREF_KEY = "mentor_partner_shared_pref";
    public static final String PREF_AUTH = "mentor_partner_logged";

    private static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences  pref = context.getSharedPreferences(PREF_KEY , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return editor;
    }

    public static void putBoolean(Context context, String key, boolean value){

        SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(key, value);
        editor.apply();

    }



    public static boolean getBoolean(Context context, String key){

        SharedPreferences  pref = context.getSharedPreferences(PREF_KEY , Context.MODE_PRIVATE);

        return pref.getBoolean(key , false);

    }

    public static void clear(Context context) {

        SharedPreferences  pref = context.getSharedPreferences(PREF_KEY , Context.MODE_PRIVATE);

        pref.edit().clear().apply();

    }

    public static void putString(Context context ,String key, String value) {
        SharedPreferences.Editor editor = getEditor(context);

        editor.putString(key, value);
        editor.apply();

    }

    public static String getString(Context context, String key) {

        SharedPreferences  pref = context.getSharedPreferences(PREF_KEY , Context.MODE_PRIVATE);

        return pref.getString(key , null);
    }
}
