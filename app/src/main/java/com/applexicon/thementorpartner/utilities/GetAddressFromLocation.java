package com.applexicon.thementorpartner.utilities;

import android.content.Context;
import android.util.Log;


import com.applexicon.thementorpartner.response.Response;
import com.google.gson.annotations.SerializedName;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by MOHAMED on 15/02/2017.
 */

public class GetAddressFromLocation {

    private static final String TAG = "GetAddressFromLocation";
    public static String country="";
    public static void getCompleteAddressString(final Context context,
                                                  double latitude,
                                                  double longitude,
                                                 final Consumer<getAddressFromLocationResponse> consumer ,
                                                 final Response.ResponseErrorCodeHandler responseErrorCodeHandler ) {

        String reqURL = "http://maps.googleapis.com/" ;


        GetAddressFromLocationAPI api =
               getClient(reqURL).create(GetAddressFromLocationAPI.class);

        Observable<getAddressFromLocationResponse> response = api.getAddress( latitude+","+longitude , true);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "getCompleteAddressString : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }



    public static Retrofit getClient(String baseUrl) {

        Retrofit retrofit = null;
        if (retrofit==null) {

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS);
            // add your other interceptors …

            // add logging as last interceptor
            httpClient.addInterceptor(logging);  // <-- this is the important line!


            httpClient.addInterceptor(new Interceptor() {
                                          @Override
                                          public okhttp3.Response intercept(Chain chain) throws IOException {
                                              Request original = chain.request();

                                              Request request = original.newBuilder()
                                                      .method(original.method(), original.body())
                                                      .build();

                                              return chain.proceed(request);
                                          }
                                      }
            );



            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;


    }

    private interface  GetAddressFromLocationAPI{
        @GET("maps/api/geocode/json")
        Observable<getAddressFromLocationResponse> getAddress(
                @Query("latlng") String json,
                @Query("sensor") boolean sensor

        );
    }

    public class getAddressFromLocationResponse {

        @SerializedName("results")
        public List<AddressObject> addressList;

        @SerializedName("status")
        public String status;


        public class AddressObject{
            @SerializedName("formatted_address")
            public String address;
        }
    }
}
