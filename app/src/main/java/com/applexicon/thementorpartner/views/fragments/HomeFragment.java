package com.applexicon.thementorpartner.views.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.HomeFragmentVM;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.views.adapters.SessionRequestsAdapter;
import com.applexicon.thementorpartner.databinding.FragmentHomeBinding;

import java.util.Observable;
import java.util.Observer;

public class HomeFragment extends Fragment implements Observer {


    private OnFragmentInteractionListener mListener;
    private FragmentHomeBinding binding;
    public   HomeFragmentVM vewModel;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.setToolbarTitle(R.string.home);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        View view = binding.getRoot();
         vewModel =  new HomeFragmentVM(getActivity() , binding);
        binding.setHomeFragmentVM(vewModel);
        vewModel.addObserver(this);

        setupListSessionsView(binding.fragmentHomeRecyclarViewRv);

        broadcastReceiverSetupForSession();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            mListener.setHomeFragment(this);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setupListSessionsView(RecyclerView listSessions) {
        SessionRequestsAdapter adapter = new SessionRequestsAdapter(this);
        listSessions.setAdapter(adapter);
        listSessions.setLayoutManager(new LinearLayoutManager(getActivity()));
        listSessions.addItemDecoration(new MyItemDecoration(getContext()));
    }
    @Override
    public void update(Observable observable, Object o) {

        if (observable instanceof HomeFragmentVM) {
            SessionRequestsAdapter sessionRequestsAdapter = (SessionRequestsAdapter) binding.fragmentHomeRecyclarViewRv.getAdapter();
            HomeFragmentVM homeViewModel = (HomeFragmentVM) observable;
            sessionRequestsAdapter.setSessionsRequestsList(homeViewModel.getSessionRequests());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void setHomeFragment(HomeFragment homeFragment);
    }

    public class MyItemDecoration extends RecyclerView.ItemDecoration {

        private final int decorationHeight;
        private Context context;

        public MyItemDecoration(Context context) {
            this.context = context;
            decorationHeight = 15;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            if (parent != null && view != null) {

                int itemPosition = parent.getChildAdapterPosition(view);
                int totalCount = parent.getAdapter().getItemCount();

                if (itemPosition >= 0 && itemPosition < totalCount - 1) {
                    outRect.bottom = decorationHeight;
                }
            }
        }
    }

    public void refreshList() {
        vewModel.fetchSessions();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void broadcastReceiverSetupForSession(){
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                mSessionReceiver, new IntentFilter("Session"));
    }

    private BroadcastReceiver mSessionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context1, Intent intent) {
            refreshList();
        }
    };
}
