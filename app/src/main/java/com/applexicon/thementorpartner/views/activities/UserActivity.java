package com.applexicon.thementorpartner.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;

import android.content.res.Configuration;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.utilities.FileUtils;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.utilities.UserLocation;
import com.applexicon.thementorpartner.viewModel.SignUpFragmentViewModel;
import com.applexicon.thementorpartner.viewModel.UserActivityViewModel;
import com.applexicon.thementorpartner.views.adapters.SliderPagerAdapter;
import com.applexicon.thementorpartner.views.fragments.ForgotPasswordDialogFragment;
import com.applexicon.thementorpartner.views.fragments.HelpFragment;
import com.applexicon.thementorpartner.views.fragments.HelpItemDialogFragment;
import com.applexicon.thementorpartner.views.fragments.NewPasswordDialogFragment;
import com.applexicon.thementorpartner.views.fragments.SignInFragment;
import com.applexicon.thementorpartner.views.fragments.SignUpFragment;
import com.applexicon.thementorpartner.databinding.ActivityUserBinding;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.File;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

import static com.applexicon.thementorpartner.viewModel.SignUpFragmentViewModel.CERTIFICATE_FILE_SELECT_CODE;
import static com.applexicon.thementorpartner.viewModel.SignUpFragmentViewModel.ID_FILE_SELECT_CODE;
import static com.applexicon.thementorpartner.viewModel.SignUpFragmentViewModel.RESUME_FILE_SELECT_CODE;

public class UserActivity extends AppCompatActivity implements Observer,
        SignInFragment.OnFragmentInteractionListener,
        SignUpFragment.OnFragmentInteractionListener,
        ForgotPasswordDialogFragment.OnFragmentInteractionListener,
        NewPasswordDialogFragment.OnFragmentInteractionListener,
        HelpFragment.OnFragmentInteractionListener,
        HelpItemDialogFragment.OnFragmentInteractionListener {

    private final String TAG = "UserActivity";
    private final int FILE_SELECT_CODE = 0;

    public ActivityUserBinding activityUserBinding;
    public UserActivityViewModel viewModel;
    private SignUpFragment signUpFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String languageToLoad = LanguageUtils.getUsedLang(getApplicationContext());
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        initDataBinding();
        setUpAutoScrollViewPager();
        setUpObservable(this);

    }

    private void setUpObservable(Observer observer) {

    }

    private void setUpAutoScrollViewPager() {

        AutoScrollViewPager pager = activityUserBinding.pager;

        pager.setInterval(5000);
        pager.startAutoScroll();

//        AutoScrollViewPagerAdapter adapter = new AutoScrollViewPagerAdapter(this).setInfiniteLoop(true);
        SliderPagerAdapter adapter = new SliderPagerAdapter(this);

//        pager.setAdapter(adapter);

        CirclePageIndicator indicator = activityUserBinding.activityUserIndicatorCPI;

//        indicator.setViewPager(pager);

    }

    private void initDataBinding() {

        activityUserBinding = DataBindingUtil.setContentView(this, R.layout.activity_user);
        viewModel = new UserActivityViewModel(this);
        activityUserBinding.setUserActivityVM(viewModel);

        SignInFragment loginFragment = SignInFragment.newInstance();

        FragmentTransaction bt = getSupportFragmentManager().beginTransaction();
        bt.replace(R.id.activityLogin_container_fl, loginFragment, "SignInFragment");
        bt.commit();

    }

    @Override
    public void update(Observable observable, Object o) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "onActivityResult : requestCode " + requestCode);
        Log.e(TAG, "onActivityResult : resultCode " + resultCode);

        Uri uri;
        String path;
        File file;

        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case 1000:
                    signUpFragment.viewModel.getCurrentLocation();
                    break;
                case ID_FILE_SELECT_CODE:
                    try {

                        // Get the Uri of the selected file
                        uri = data.getData();
                        Log.e(TAG, "File Uri: " + uri.toString());

                        path = FileUtils.getPath(this, uri);
                        Log.e(TAG, "File Path: " + path);

                        file = new File(path);

                        signUpFragment.viewModel.signUp.setIdFile(file);
                    } catch (Exception e) {

                        Log.e(TAG, "ID_FILE_SELECT_CODE Exception: " + e.getMessage());
                    }
                    break;
                case CERTIFICATE_FILE_SELECT_CODE:
                    try {
                        // Get the Uri of the selected file
                        uri = data.getData();
                        Log.e(TAG, "File Uri: " + uri.toString());

                        path = FileUtils.getPath(this, uri);

                        Log.e(TAG, "File Path: " + path);

                        file = new File(path);
                        signUpFragment.viewModel.signUp.setCertificateFile(file);
                    } catch (Exception e) {
                        Log.e(TAG, "CERTIFICATE_FILE_SELECT_CODE Exception: " + e.getMessage());

                    }
                    break;

                case RESUME_FILE_SELECT_CODE:
                    try {
                        uri = data.getData();
                        Log.e(TAG, "File Uri: " + uri.toString());

                        path = FileUtils.getPath(this, uri);

                        Log.e(TAG, "File Path: " + path);

                        file = new File(path);
                        signUpFragment.viewModel.signUp.setResumeFile(file);
                    } catch (Exception e) {
                        Log.e(TAG, "RESUME_FILE_SELECT_CODE Exception: " + e.getMessage());

                    }
                    break;

            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

   /**/


    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == SignUpFragmentViewModel.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                signUpFragment.viewModel.openFileChooser();

            } else {
                // User refused to grant permission.
            }
        }

        if (requestCode == UserLocation.LOCATION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                signUpFragment.viewModel.getCurrentLocation();

            }
        }
    }


    public static void start(Context context) {
        Intent i = new Intent(context, UserActivity.class);
        context.startActivity(i);
    }

    public void setSignUpFragment(SignUpFragment signUpFragment) {
        this.signUpFragment = signUpFragment;
    }

    public SignUpFragment getSignUpFragment() {
        return signUpFragment;
    }
}
