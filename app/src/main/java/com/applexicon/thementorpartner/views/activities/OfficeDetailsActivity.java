package com.applexicon.thementorpartner.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.GlobalValuesData;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.utilities.Constants;
import com.applexicon.thementorpartner.utilities.LanguageUtils;

import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

public class OfficeDetailsActivity extends AppCompatActivity {

    private ProgressBar mProgressBar;
    private String registrationMessage;
    WebView wv;
    ImageView backBtn;
    String data, url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_office_details);



        String languageToLoad = LanguageUtils.getUsedLang(getApplicationContext());
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());



        backBtn = (ImageView) findViewById(R.id.back_btn);

        wv = (WebView) findViewById(R.id.webView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
        mProgressBar.setVisibility(View.VISIBLE);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserActivity.start(OfficeDetailsActivity.this);
            }
        });

        url = String.format(Constants.HELP_URL, "user_registration_messsage", LanguageUtils.getUsedLang(OfficeDetailsActivity.this));

//        wv.loadDataWithBaseURL("", data, "text/html", "UTF-8", "");
        wv.loadUrl(url);
        wv.setWebViewClient(new MyWebViewClient());
        wv.getSettings().setLoadsImagesAutomatically(true);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

//        wv.loadDataWithBaseURL("", data, "text/html", "UTF-8", "");
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(url != null && url.startsWith("https://api.whatsapp.com")) {
                String[] splitURL = url.split("=");
                String[] mobileNumber = splitURL[1].split("&");
                openWhatsapp(mobileNumber[0]);
            }
            else if(url != null && url.startsWith("https://mailto:")) {
                String[] splitUrl = url.split(":");
                openGmail(splitUrl[2]);
            }
            else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            Log.d("TAG", "onReceivedError: ");
            super.onReceivedError(view, request, error);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mProgressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }

    private void openGmail(String email){
        email = email.replace("/", "");
        Intent i = null;
        try {
            i = new Intent(Intent.ACTION_SEND);
            i.setType("plain/text");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
            i.putExtra(Intent.EXTRA_SUBJECT, "Account activation");
            i.putExtra(Intent.EXTRA_TITLE  , "Account activation");
            final PackageManager pm = getPackageManager();
            final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
            String className = null;
            for (final ResolveInfo info : matches) {
                if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                    className = info.activityInfo.name;

                    if(className != null && !className.isEmpty()){
                        break;
                    }
                }
            }
            i.setClassName("com.google.android.gm", className);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(OfficeDetailsActivity.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void openWhatsapp(String MobileNumber){
        try {
            PackageManager packageManager = getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url1 = "whatsapp://send?phone="+ MobileNumber +"&text=" + URLEncoder.encode("Activate", "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url1));
            if (i.resolveActivity(packageManager) != null) {
                startActivity(i);
            }
        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(OfficeDetailsActivity.this, "Whatsapp not installed ", Toast.LENGTH_SHORT).show();
        }
    }

    public static void start(Context context ) {
        Intent i = new Intent(context, OfficeDetailsActivity.class);
        context.startActivity(i);
    }

    @Override
    public void onBackPressed() {
        UserActivity.start(this);
        super.onBackPressed();

    }

    private void getGloabalData() {

        GlobalValuesData.APIInterface apiService =
                ApiClient.getClient(this).create(GlobalValuesData.APIInterface.class);

        Call<GlobalValuesData> call = apiService.fetchGlobalValues("teacher", Mentor.getInstance().token);
        call.enqueue(new Callback<GlobalValuesData>() {
            @Override
            public void onResponse(Call<GlobalValuesData> call, retrofit2.Response<GlobalValuesData> response) {
                if (response.isSuccessful()) {
                    GlobalValuesData verifyMobileResponse = response.body();
                    try {
                        if (verifyMobileResponse.getResult()) {
                            List<GlobalValuesData.Data.GlobalValues> globalValuesData = verifyMobileResponse.getContent().getData().getGlobalValues();

                            for (int i = 0; i < globalValuesData.size(); i++) {
                                if (globalValuesData.get(i).getKey().equals("user_registration_messsage")){
                                    registrationMessage = globalValuesData.get(i).getValue();
                                }
                            }

                            Log.d("TAG", "onResponse: "+registrationMessage);
                            wv.loadDataWithBaseURL("", registrationMessage, "text/html", "UTF-8", "");
//                            wv.loadUrl(String.format(registrationMessage));
                            wv.setWebViewClient(new MyWebViewClient());
                            wv.getSettings().setLoadsImagesAutomatically(true);
                            wv.getSettings().setJavaScriptEnabled(true);
                            wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
//                        Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GlobalValuesData> call, Throwable t) {
            }
        });
    }
}
