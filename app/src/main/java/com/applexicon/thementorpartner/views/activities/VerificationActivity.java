package com.applexicon.thementorpartner.views.activities;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;

import android.content.res.Configuration;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.viewModel.VerificationVM;
import com.applexicon.thementorpartner.views.fragments.NewPasswordDialogFragment;
import com.applexicon.thementorpartner.databinding.ActivityVerificationBinding;

import java.util.Locale;

public class VerificationActivity extends AppCompatActivity implements
        NewPasswordDialogFragment.OnFragmentInteractionListener{

    private static final String EXTRA_PHONE = "extra_phone";
    private static final String EXTRA_IS_FORGOT_PASSWORD = "is_forgot_password";
    private String phone;
    public boolean isForgotPassword= false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        String languageToLoad = LanguageUtils.getUsedLang(getApplicationContext());
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());




        if(getIntent()!= null){
            phone = getIntent().getStringExtra(EXTRA_PHONE);
            isForgotPassword = getIntent().getBooleanExtra(EXTRA_IS_FORGOT_PASSWORD , false);
        }


        initDataBinding();
    }


    private void initDataBinding() {

        ActivityVerificationBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_verification);
        VerificationVM viewModel = new VerificationVM(this , binding , phone);
        binding.setVerificationVM(viewModel);

    }

    public static void start(Context context , String phone , boolean isForgetPassword ) {
        Intent i = new Intent(context, VerificationActivity.class);

        i.putExtra(EXTRA_PHONE , phone);
        i.putExtra(EXTRA_IS_FORGOT_PASSWORD , isForgetPassword);

        context.startActivity(i);
    }


    @Override
    public void onBackPressed() {
        UserActivity.start(this);
        finish();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
