/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.views.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import com.applexicon.thementorpartner.models.CompletedBooking;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.ItemSessionCompletedVM;
import com.applexicon.thementorpartner.views.fragments.MyCompletedSessionsFragment;
import com.applexicon.thementorpartner.databinding.MySessionsCompletedItemBinding;

import java.util.Collections;
import java.util.List;

public class SessionCompletedAdapter extends RecyclerView.Adapter<SessionCompletedAdapter.ViewHolder> {

  public final MyCompletedSessionsFragment mMyCompletedSessionsFragment;
  private List<CompletedBooking> dataList;

  public SessionCompletedAdapter(MyCompletedSessionsFragment fragment) {
    mMyCompletedSessionsFragment = fragment;
    this.dataList = Collections.emptyList();
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    MySessionsCompletedItemBinding Binding =
        DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.my_sessions_completed_item,
            parent, false);
      return new ViewHolder(Binding);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindPeople(dataList.get(position) , mMyCompletedSessionsFragment);
      }

  @Override public int getItemCount() {
    return dataList.size();
  }

  public void setDataList(List<CompletedBooking> dataList) {
    this.dataList = dataList;
    notifyDataSetChanged();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    MySessionsCompletedItemBinding itemBinding;

    public ViewHolder(MySessionsCompletedItemBinding itemBinding) {
      super(itemBinding.itemContainerRl);
      this.itemBinding = itemBinding;
    }

    void bindPeople(CompletedBooking model, MyCompletedSessionsFragment fragment) {

      if (itemBinding.getItemSessionCompletedVM() == null) {
        itemBinding.setItemSessionCompletedVM(
            new ItemSessionCompletedVM(model,  fragment , itemBinding));
      } else {
        itemBinding.getItemSessionCompletedVM().setModel(model);
      }
    }
  }
}
