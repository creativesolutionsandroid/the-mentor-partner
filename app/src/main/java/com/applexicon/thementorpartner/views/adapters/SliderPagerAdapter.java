package com.applexicon.thementorpartner.views.adapters;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.applexicon.thementorpartner.R;

public class SliderPagerAdapter extends PagerAdapter {

    public Context mContext;

    private SliderItemModel sliderItemModels[] ;

    public SliderPagerAdapter(Context context) {
        mContext = context;


        sliderItemModels = new SliderItemModel[]{

                new SliderItemModel(R.drawable.slider_image_1 , R.string.text_slider_first),
                new SliderItemModel(R.drawable.slider_image_2 , R.string.text_slider_two),
                new SliderItemModel(R.drawable.slider_image_2 , R.string.text_slider_three)
        } ;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        SliderItemModel sliderItemModel = sliderItemModels[position];


        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.slider_layout, collection, false);

        ImageView background = (ImageView)layout.findViewById(R.id.sliderLayout_background_iv);
        TextView text = (TextView)layout.findViewById(R.id.sliderLayout_text_tv);

        background.setImageResource(sliderItemModel.backgroundImageId);
        text.setText(sliderItemModel.text);

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return sliderItemModels.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return null;
    }


    private class SliderItemModel{

        public   int backgroundImageId;
        public String text;

        public SliderItemModel(int backgroundImageId, int textId) {
            this.backgroundImageId = backgroundImageId;
            this.text= mContext.getString(textId);
        }
    }
}