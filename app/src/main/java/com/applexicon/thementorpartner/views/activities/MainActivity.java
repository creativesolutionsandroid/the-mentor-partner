package com.applexicon.thementorpartner.views.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;

import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applexicon.thementorpartner.BuildConfig;
import com.applexicon.thementorpartner.MentorApp;
import com.applexicon.thementorpartner.models.GlobalValuesData;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.ImageLoading;
import com.applexicon.thementorpartner.utilities.SharedPref;
import com.applexicon.thementorpartner.viewModel.EditProfileFragmentVM;
import com.applexicon.thementorpartner.viewModel.MainActivityVM;
import com.applexicon.thementorpartner.viewModel.SignUpFragmentViewModel;
import com.applexicon.thementorpartner.views.fragments.BookingDetailsDialogFragment;
import com.applexicon.thementorpartner.views.fragments.ChangePasswordDialogFragment;
import com.applexicon.thementorpartner.views.fragments.EditProfileFragment;
import com.applexicon.thementorpartner.views.fragments.HelpFragment;
import com.applexicon.thementorpartner.views.fragments.HelpItemDialogFragment;
import com.applexicon.thementorpartner.views.fragments.HomeFragment;
import com.applexicon.thementorpartner.views.fragments.MyCalendarFragment;
import com.applexicon.thementorpartner.views.fragments.MyCanceledSessionsFragment;
import com.applexicon.thementorpartner.views.fragments.MyCompletedSessionsFragment;
import com.applexicon.thementorpartner.views.fragments.MySessionsFragment;
import com.applexicon.thementorpartner.views.fragments.PaymentHistoryFragment;
import com.applexicon.thementorpartner.views.fragments.PreferencesFragment;
import com.applexicon.thementorpartner.views.fragments.ProfileFragment;
import com.applexicon.thementorpartner.views.fragments.ReviewsFragment;
import com.applexicon.thementorpartner.views.fragments.SessionAddedDialogFragment;
import com.applexicon.thementorpartner.views.fragments.SessionDetailsDialogFragment;
import com.applexicon.thementorpartner.views.fragments.SessionDetailsFragment;
import com.applexicon.thementorpartner.views.fragments.SessionListDialogFragment;
import com.applexicon.thementorpartner.views.fragments.SessionRejectAlertDialog;
import com.applexicon.thementorpartner.views.fragments.SessionsListFragment;
import com.applexicon.thementorpartner.views.fragments.SettingsFragment;
import com.applexicon.thementorpartner.databinding.ActivityMainBinding;
import com.applexicon.thementorpartner.databinding.NavHeaderMainBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity
        implements
        NavigationView.OnNavigationItemSelectedListener,
        HomeFragment.OnFragmentInteractionListener,
        MyCalendarFragment.OnFragmentInteractionListener,
        PaymentHistoryFragment.OnFragmentInteractionListener,
        MySessionsFragment.OnFragmentInteractionListener,
        SettingsFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener,
        ReviewsFragment.OnFragmentInteractionListener,
        HelpFragment.OnFragmentInteractionListener,
        BookingDetailsDialogFragment.OnFragmentInteractionListener,
        SessionAddedDialogFragment.OnFragmentInteractionListener,
        SessionRejectAlertDialog.OnFragmentInteractionListener, Response.ResponseErrorCodeHandler,
        EditProfileFragment.OnFragmentInteractionListener,
        ChangePasswordDialogFragment.OnFragmentInteractionListener,
        SessionListDialogFragment.OnFragmentInteractionListener,
        SessionDetailsDialogFragment.OnFragmentInteractionListener,
        MyCompletedSessionsFragment.OnFragmentInteractionListener,
        MyCanceledSessionsFragment.OnFragmentInteractionListener,
        HelpItemDialogFragment.OnFragmentInteractionListener,
        PreferencesFragment.OnFragmentInteractionListener,
        SessionsListFragment.OnFragmentInteractionListener,
        SessionDetailsFragment.OnFragmentInteractionListener {

    private FragmentTransaction transactionFragment;
    private Toolbar toolbar;
    public NavigationView navigationView;
    private DrawerLayout drawer;
    private boolean BacKFlag = true;
    private Handler handler;
    private Toast myToast;
    private Runnable myRunnable;
    private String TAG = "MainActivity";
    public TextView nameTV;
    private CircleImageView imageIV;
    public ActivityMainBinding mainBinding;
    public NavHeaderMainBinding navHeaderbinding;
    private SwitchCompat switchCompact;
    private EditProfileFragment editProfileFragment;
    public HomeFragment homeFragment;
    public BookingDetailsDialogFragment bookingDetailsDialogFragment;
    private TextView toolBarTitleTV;
    private CircleImageView userImageCIV;
    private View progressBar;
    public static ActionBarDrawerToggle toggle;
    public static MainActivity mainActivity;
    private ImageView backButton;
    public static List<GlobalValuesData.Data.GlobalValues> globalValuesData = new ArrayList<>();
    RelativeLayout progressDialogLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String languageToLoad = LanguageUtils.getUsedLang(getApplicationContext());
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());



//        setContentView(R.layout.activity_main);
        final Mentor mentor = ((MentorApp) getApplication()).mentor;
        Log.d(TAG, "token: "+Mentor.getInstance().token);
//        Log.d(TAG, "sex_to_teach: "+Mentor.getInstance().sex_to_teach);
//        Log.d(TAG, "session_type: "+Mentor.getInstance().session_type);
//        Log.d(TAG, "prefered_session_location: "+Mentor.getInstance().prefered_session_location);
        if (!mentor.isLoggedIn(getApplicationContext())) {
            UserActivity.start(this);
            finish();
        } else {

            mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
            MainActivityVM viewModel = new MainActivityVM(this);
            mainBinding.setMainActivityVM(viewModel);

            Bundle extra = getIntent().getExtras();

//            if (extra != null) {
//
//                Log.e(TAG , "extra : " + extra.toString());
//
//                if(Integer.parseInt(extra.getString("notificationType")) == MyFirebaseMessagingService.SESSION_STARTED) {
//                    String sessionId = extra.getString("session_id") ;
//                    String sessionDate =  extra.getString("date");
//                    String sessionTimeTo =  extra.getString("to");
//
//                    SessionStartedBroadcast.start(this , sessionId , sessionDate , sessionTimeTo);
//                }
//            }


//            SessionStartedBroadcast.start(this , "12" , "2017-11-1" , "2:19 PM");

            initViews();
            setToolBar();
            setNavigationItem();
            startHomeFragment();

            switchCompact = (SwitchCompat) findViewById(R.id.appBarMain_available_sc);

            switchCompact.setClickable(false);

            fetchAvailableStatus();

            switchCompact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAvailableStatus(!switchCompact.isChecked() ? 1 : 0);
                    changeAvailableStatus(!switchCompact.isChecked());
                }
            });
        }
    }

    private void changeAvailableStatus(final boolean b) {

        switchCompact.setEnabled(false);

        Mentor.getInstance().status = b ? 1 : 0;

        Mentor.getInstance().changeAvailableStatus(this, new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                if (response.getResult()) {
                    setAvailableStatus(b ? 1 : 0);
                    Toast.makeText(getApplicationContext(), b ? getString(R.string.your_are_online) : getString(R.string.your_are_offline), Toast.LENGTH_SHORT).show();

                }

                switchCompact.setEnabled(true);
            }
        }, this);
    }

    private void fetchAvailableStatus() {
        Mentor.getInstance().fetchAvailableStatus(this, new Consumer<Response<Mentor.FetchStatusContentData>>() {
            @Override
            public void accept(Response<Mentor.FetchStatusContentData> response) throws Exception {

                int status = 0;

                if (response.getResult()) {
                    status = response.getContent().getData().status;
                }

                setAvailableStatus(status);

                Toast.makeText(getApplicationContext(), status == 1 ? getString(R.string.your_are_online) : getString(R.string.your_are_offline), Toast.LENGTH_SHORT).show();

            }
        }, this);
    }

    private void setAvailableStatus(int status) {
        switchCompact.setChecked(status == 1 ? true : false);
    }

    private void initViews() {
        progressDialogLayout = (RelativeLayout) findViewById(R.id.progress_dialog);
        backButton = (ImageView) findViewById(R.id.back_button);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolBarTitleTV = (TextView) findViewById(R.id.toolbar_title);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        View navigationViewHeader = navigationView.getHeaderView(0);

        Button viewProfileBtn = (Button) navigationViewHeader.findViewById(R.id.navHeaderMain_viewProfile_btn);

        progressDialogLayout.setVisibility(View.VISIBLE);
        getGloabalData();

        viewProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onViewProfileBtn(view);
            }
        });

        nameTV = (TextView) navigationViewHeader.findViewById(R.id.navHeaderMain_name_tv);


        nameTV.setText(Mentor.getInstance().name);

        userImageCIV = (CircleImageView) navigationViewHeader.findViewById(R.id.navHeaderMain_userImage_civ);
        progressBar = navigationViewHeader.findViewById(R.id.image_loading);


        displayUserImage();

    }

    public void displayUserImage() {
        ImageLoading.loadImage(userImageCIV,
                Mentor.getInstance().imageUrl,
                R.drawable.avatar,
                progressBar);
    }

    private void onViewProfileBtn(View view) {
        Log.e(TAG, "onViewProfileBtn");

        if (getCheckedItem(navigationView) > 0)
            navigationView.getMenu().getItem(getCheckedItem(navigationView)).setChecked(false);

        Fragment newFragment = ProfileFragment.newInstance();
        transactionFragment = getSupportFragmentManager().beginTransaction();
        transactionFragment.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transactionFragment.replace(R.id.main_container, newFragment);
        transactionFragment.commitAllowingStateLoss();

        drawer.closeDrawers();
    }

    private void setToolBar() {
        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();
    }

    private void setNavigationItem() {
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void startHomeFragment() {

        transactionFragment = getSupportFragmentManager().beginTransaction();
        HomeFragment newFragment = new HomeFragment();
        transactionFragment.add(R.id.main_container, newFragment).commit();
        navigationView.setCheckedItem(R.id.nav_home);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK && data != null) {

            switch (requestCode) {
                case EditProfileFragmentVM.PROFILE_IMAGE_SELECT_CODE:
                    editProfileFragment.viewModel.setImageProfileFile(data);
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == SignUpFragmentViewModel.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                editProfileFragment.viewModel.openFileChooser();

            } else {
                // User refused to grant permission.
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {

                if (!BacKFlag) {
                    handler.removeCallbacks(myRunnable);
                    if (myToast != null)
                        myToast.cancel();
                    super.onBackPressed();
                    return;
                }

                BacKFlag = false;
                handler = new Handler();
                myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        BacKFlag = true;

                        myToast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.click_twice), Toast.LENGTH_SHORT);
                        myToast.show();
                    }
                };

                handler.postDelayed(myRunnable, 1000);
            } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                super.onBackPressed();
//                transactionFragment = getSupportFragmentManager().beginTransaction();
//                HomeFragment homeFragment = new HomeFragment();
//                transactionFragment.replace(R.id.main_container, homeFragment).commit();
//                navigationView.setCheckedItem(R.id.nav_home);
            } else {
                super.onBackPressed();
                finishAffinity();
            }

        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        // nav_home , nav_calendar ,  nav_payment_history , nav_my_sessions , nav_settings , nav_share , nav_help

        int id = item.getItemId();

        Fragment newFragment = null;
        switch (id) {
            case R.id.nav_home:
                newFragment = HomeFragment.newInstance();
                break;
            case R.id.nav_calendar:
                newFragment = MyCalendarFragment.newInstance();
                break;
            case R.id.nav_payment_history:
                newFragment = PaymentHistoryFragment.newInstance();
                break;
            case R.id.nav_my_sessions:
                newFragment = MySessionsFragment.newInstance();
                break;
            case R.id.nav_preferences:
                newFragment = PreferencesFragment.newInstance();
                break;
            case R.id.nav_reviews:
                newFragment = ReviewsFragment.newInstance();
                break;
            case R.id.nav_settings:
                newFragment = SettingsFragment.newInstance();
                break;
            case R.id.nav_share:

                shareApp(this);
                break;
            case R.id.nav_help:
                newFragment = HelpFragment.newInstance();
                break;
        }

        if (newFragment != null) {
            transactionFragment = getSupportFragmentManager().beginTransaction();
            transactionFragment.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transactionFragment.replace(R.id.main_container, newFragment);
            transactionFragment.commitAllowingStateLoss();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void start(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(i);
        ((AppCompatActivity) context).finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void setBookingDetailsDialogFragment(BookingDetailsDialogFragment bookingDetailsDialogFragment) {
        this.bookingDetailsDialogFragment = bookingDetailsDialogFragment;
    }

    @Override
    public void setHomeFragment(HomeFragment homeFragment) {
        this.homeFragment = homeFragment;
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG, "handleResponseErrorCodeForUser : " + message);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        switchCompact.setEnabled(true);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG, "handleResponseErrorCodeForDebug : " + message);
        switchCompact.setEnabled(true);


    }

    public void replaceFragment(Fragment fragment) {
        transactionFragment = getSupportFragmentManager().beginTransaction();
        transactionFragment.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transactionFragment.replace(R.id.main_container, fragment);
        transactionFragment.commitAllowingStateLoss();

    }

    public void addFragment(Fragment fragment) {
        transactionFragment = getSupportFragmentManager().beginTransaction();
        transactionFragment.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transactionFragment.replace(R.id.main_container, fragment);
        transactionFragment.addToBackStack(fragment.getTag());
        transactionFragment.commitAllowingStateLoss();

    }

    public void setEditProfileFragment(EditProfileFragment editProfileFragment) {
        this.editProfileFragment = editProfileFragment;
    }

    public EditProfileFragment getEditProfileFragment() {
        return editProfileFragment;
    }

    public void setToolbarTitle(int titleId) {

        toolBarTitleTV.setText(titleId);
    }

    public void setBackButton(){
        toolbar.setVisibility(View.INVISIBLE);
        backButton.setVisibility(View.VISIBLE);
    }

    private void shareApp(Context context) {

        final String appPackageName = BuildConfig.APPLICATION_ID;
        final String appName = context.getString(R.string.app_name);
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        String shareBodyText = "https://play.google.com/store/apps/details?id=" +
                appPackageName;
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, appName);
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
        context.startActivity(Intent.createChooser(shareIntent, context.getString(R.string
                .share_with)));
    }


    private int getCheckedItem(NavigationView navigationView) {
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.isChecked()) {
                return i;
            }
        }
        return -1;
    }

    private void getGloabalData() {

        GlobalValuesData.APIInterface apiService =
                ApiClient.getClient(this).create(GlobalValuesData.APIInterface.class);

        Call<GlobalValuesData> call = apiService.fetchGlobalValues("teacher", Mentor.getInstance().token);
        call.enqueue(new Callback<GlobalValuesData>() {
            @Override
            public void onResponse(Call<GlobalValuesData> call, retrofit2.Response<GlobalValuesData> response) {
                if (response.isSuccessful()) {
                    GlobalValuesData verifyMobileResponse = response.body();
                    try {
                        if (verifyMobileResponse.getResult()) {
                            globalValuesData = verifyMobileResponse.getContent().getData().getGlobalValues();
                        } else {
//                                                      status false case
                            String failureResponse = response.body().getContent().getMessage();
                            if(failureResponse.equalsIgnoreCase("Your account is inactive: contact the office")) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                                String title= "", msg= "", posBtn = "", negBtn = "";
                                posBtn = getResources().getString(R.string.ok);
                                title = getResources().getString(R.string.app_name);
                                msg = failureResponse;

                                // set title
                                alertDialogBuilder.setTitle(title);

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage(msg)
                                        .setCancelable(false)
                                        .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                                logout();
                                            }
                                        });

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }
                            else if(failureResponse.equalsIgnoreCase("Token has expired") ||
                                    failureResponse.equalsIgnoreCase("token_invalid")) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                                String title= "", msg= "", posBtn = "", negBtn = "";
                                posBtn = getResources().getString(R.string.ok);
                                title = getResources().getString(R.string.app_name);
                                msg = getResources().getString(R.string.session_expired);

                                // set title
                                alertDialogBuilder.setTitle(title);

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage(msg)
                                        .setCancelable(false)
                                        .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                                SharedPref.clear(MainActivity.this);
                                                UserActivity.start(MainActivity.this);
                                                finish();
                                            }
                                        });

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
//                        Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                progressDialogLayout.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<GlobalValuesData> call, Throwable t) {
                progressDialogLayout.setVisibility(View.GONE);
            }
        });
    }

    private void logout() {
        Mentor.getInstance().logout(MainActivity.this, new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                if (response.getResult()) {
                    SharedPref.clear(MainActivity.this);
                    UserActivity.start(MainActivity.this);
                    Toast.makeText(MainActivity.this, response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this);
    }
}
