/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.views.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.ConfirmedSessionFiltered;
import com.applexicon.thementorpartner.viewModel.ItemSessionConfirmedVM;
import com.applexicon.thementorpartner.views.fragments.MyConfirmedSessionsFragment;
import com.applexicon.thementorpartner.databinding.MySessionsConfirmedItemBinding;

import java.util.Collections;
import java.util.List;

public class SessionConfirmedAdapter extends RecyclerView.Adapter<SessionConfirmedAdapter.ViewHolder> {

  public final MyConfirmedSessionsFragment mMyConfirmedSessionsFragment;
  private List<ConfirmedSessionFiltered> dataList;

  public SessionConfirmedAdapter(MyConfirmedSessionsFragment fragment) {
    mMyConfirmedSessionsFragment = fragment;
    this.dataList = Collections.emptyList();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    MySessionsConfirmedItemBinding Binding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.my_sessions_confirmed_item,
                    parent, false);
    return new ViewHolder(Binding);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    holder.bindPeople(dataList.get(position) , mMyConfirmedSessionsFragment);
  }

  @Override
  public int getItemCount() {
    return dataList.size();
  }

  public void setDataList(List<ConfirmedSessionFiltered> dataList) {
    this.dataList = dataList;
    notifyDataSetChanged();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    MySessionsConfirmedItemBinding itemBinding;

    public ViewHolder(MySessionsConfirmedItemBinding itemBinding) {
      super(itemBinding.itemContainerRl);
      this.itemBinding = itemBinding;
    }

    void bindPeople(ConfirmedSessionFiltered model, MyConfirmedSessionsFragment fragment) {

      if (itemBinding.getItemSessionConfirmedVM() == null) {
        itemBinding.setItemSessionConfirmedVM(
                new ItemSessionConfirmedVM(model,  fragment , itemBinding));
      } else {
        itemBinding.getItemSessionConfirmedVM().setModel(model);
      }

      Log.d("TAG", "bindPeople: "+model.paidAmount);
      if(model.paidAmount == null) {
        itemBinding.fragmentMyConfirmedBookingsCallBtn.setVisibility(View.GONE);
      }
      else {
        itemBinding.fragmentMyConfirmedBookingsCallBtn.setVisibility(View.VISIBLE);
      }
    }
  }
}
