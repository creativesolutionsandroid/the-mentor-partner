/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.views.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.PaymentHistoryFiltered;
import com.applexicon.thementorpartner.viewModel.ItemPaymentHistoryVM;
import com.applexicon.thementorpartner.views.fragments.PaymentHistoryFragment;
import com.applexicon.thementorpartner.databinding.ItemPaymentHistoryLayoutBinding;

import java.util.Collections;
import java.util.List;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder> {

  public final PaymentHistoryFragment mPaymentHistoryFragment;
  private List<PaymentHistoryFiltered> PaymentHistoryList;

  public PaymentHistoryAdapter(PaymentHistoryFragment fragment) {
    mPaymentHistoryFragment = fragment;
    this.PaymentHistoryList = Collections.emptyList();
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ItemPaymentHistoryLayoutBinding Binding =
        DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_payment_history_layout,
            parent, false);
      return new ViewHolder(Binding);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    holder.bindPeople(PaymentHistoryList.get(position) , mPaymentHistoryFragment);
  }

  @Override public int getItemCount() {
    return PaymentHistoryList.size();
  }

  public void setPaymentHistoryList(List<PaymentHistoryFiltered> paymentHistoryList) {
    this.PaymentHistoryList = paymentHistoryList;
    notifyDataSetChanged();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    ItemPaymentHistoryLayoutBinding itemPaymentHistoryLayoutBinding;

    public ViewHolder(ItemPaymentHistoryLayoutBinding itemBinding) {
      super(itemBinding.itemPaymentHistoryItemContainerRl);
      this.itemPaymentHistoryLayoutBinding = itemBinding;
    }

    void bindPeople(PaymentHistoryFiltered paymentHistory, PaymentHistoryFragment historyFragment) {
      if (itemPaymentHistoryLayoutBinding.getItemPaymentHistoryVM() == null) {
        itemPaymentHistoryLayoutBinding.setItemPaymentHistoryVM(
            new ItemPaymentHistoryVM(paymentHistory, historyFragment));
      } else {
        itemPaymentHistoryLayoutBinding.getItemPaymentHistoryVM().setPaymentHistory(paymentHistory);
      }
    }
  }
}
