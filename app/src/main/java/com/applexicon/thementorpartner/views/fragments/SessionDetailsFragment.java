package com.applexicon.thementorpartner.views.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.databinding.FragmentRequestDetailsDialogBinding;
import com.applexicon.thementorpartner.models.ConfirmedSessionFiltered;
import com.applexicon.thementorpartner.models.GlobalValuesData;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.models.SessionsTransactions;
import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.utilities.UserLocation;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;

public class SessionDetailsFragment extends Fragment implements Response.ResponseErrorCodeHandler,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{

    private static final String BOOK_SESSION_OBJECT = "BOOK_SESSION_OBJECT";
    private static final String POSITION = "POSITION";
    private OnFragmentInteractionListener mListener;
    public ConfirmedSessionFiltered requestModel;
    private TextView teacherName, gender, date, time, subjectName, grade, level, location, timer, timerType, sessionType,
            sessionAt, bookingId, sessionId;
    private TextView paymentNetAmount, paymentDescription;
    private Button paymentReceived, paymentDeclined;
    private Button startSession, cancelSession, endSession, call;
    public final MainActivity mainActivity;
    public static Boolean isSessionStarted = false;
//    RelativeLayout progressDialog;
    SessionsTransactions sessionsTransactions;
    CountDownTimer countDownTimer2;
    Calendar startCalendar = Calendar.getInstance();
    int position;
    public GoogleApiClient mGoogleApiClient;
    double latitude, longitude;
    RelativeLayout paymentPendingLayout;
    private String cancellationReason;
    private LinearLayout sessionDetailsLayout, ratingLayout, summaryLayout;
    private RelativeLayout paymentLayout, timerLayout;
    private float ratingGiven = 2.5f;

    private SimpleRatingBar ratingBar, summaryRatingBar;
    private EditText review;
    private Button ratingSubmitBtn;

    private TextView summaryNetAmount, summaryMentorFees, summarySessionAmount, sessionDuration;
    private Button doneBtn;
    private String serverTime, bookingTime;
    private Timer refreshTimer = new Timer();
    private boolean isStudentNotAvailable = false;
    private boolean runTimer = false;


    public FragmentRequestDetailsDialogBinding binding;

    public SessionDetailsFragment() {
        // Required empty public constructor
        mainActivity = (MainActivity) getActivity();
        sessionsTransactions = new SessionsTransactions();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }

    public static SessionDetailsFragment newInstance(Context context, ConfirmedSessionFiltered requestModel, int position) {
        FragmentTransaction transactionFragment;
        transactionFragment = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
        SessionDetailsFragment fragment = new SessionDetailsFragment();
        transactionFragment.replace(R.id.main_container, fragment).addToBackStack("session_details").commit();
        Bundle args = new Bundle();
        args.putSerializable(BOOK_SESSION_OBJECT, requestModel);
        args.putSerializable(POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            requestModel = (ConfirmedSessionFiltered) getArguments().getSerializable(BOOK_SESSION_OBJECT);
            position =  getArguments().getInt(POSITION,0);
        }

        MainActivity mainActivity = (MainActivity) getActivity();
//        mainActivity.setBackButton();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sessions_details, null, false);

        teacherName = (TextView)  view.findViewById(R.id.teacher_name);
        gender = (TextView)  view.findViewById(R.id.teacher_gender);
        date = (TextView)  view.findViewById(R.id.my_bookings_confirmed_item_date);

        bookingId = (TextView) view.findViewById(R.id.booking_id);
        sessionId = (TextView) view.findViewById(R.id.session_id);

        paymentNetAmount = (TextView) view.findViewById(R.id.net_amount);
        paymentDescription = (TextView) view.findViewById(R.id.payment_description);

        paymentReceived = (Button) view.findViewById(R.id.payment_received);
        paymentDeclined = (Button) view.findViewById(R.id.payment_declined);

        sessionDetailsLayout = (LinearLayout) view.findViewById(R.id.layout1);
        paymentLayout = (RelativeLayout) view.findViewById(R.id.net_amount_layout);
        timerLayout = (RelativeLayout) view.findViewById(R.id.timer_layout);
        ratingLayout = (LinearLayout) view.findViewById(R.id.rating_layout);
        summaryLayout = (LinearLayout) view.findViewById(R.id.summary_layout);

        summaryRatingBar = (SimpleRatingBar) view.findViewById(R.id.summary_rating);
        ratingBar = (SimpleRatingBar) view.findViewById(R.id.my_bookings_rate_mentor_rate_bar);
        review = (EditText) view.findViewById(R.id.review);
        ratingSubmitBtn = (Button) view.findViewById(R.id.rating_submit_btn);

        doneBtn = (Button) view.findViewById(R.id.done);

        sessionDuration = (TextView) view.findViewById(R.id.session_duration);
        summarySessionAmount = (TextView) view.findViewById(R.id.session_amount);
        summaryMentorFees = (TextView) view.findViewById(R.id.mentor_fees);
        summaryNetAmount = (TextView) view.findViewById(R.id.summary_net_amount);

        getServerTime();
        initSessionDetailsView(view);

        if (!requestModel.session_to_time.equals("0")) {
            timer.setVisibility(View.VISIBLE);
            isSessionStarted = false;
            if(countDownTimer2 != null)
            countDownTimer2.cancel();

            // session ended and payment pending
            sessionDetailsLayout.setVisibility(View.GONE);
            timerLayout.setVisibility(View.GONE);
            getPaymentDetails();
        }

        ratingSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ratingBar.getRating() == 0) {
                    Toast.makeText(mainActivity, "Please give rating", Toast.LENGTH_SHORT).show();
                }
                else {
                    rateStudent();
                }
            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        refreshTimer.schedule(new MyTimerTask(), 60000, 60000);
        return view;
    }

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            if(getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        getServerTime();
                    }
                });
            }
        }
    }

    private void setUpGoogleAPIClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient
                    .Builder(getContext())
//                    .addApi(Places.GEO_DATA_API)
//                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(getActivity(), this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        UserLocation userLocation = new UserLocation(getActivity());

        if (!UserLocation.isGPSEnabled(getActivity())) {
            userLocation.askForGPS(getActivity(), mGoogleApiClient);
        } else {
            getCurrentLocation();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public void getCurrentLocation() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                UserLocation.requestForLocationPermission(getActivity());
            }

        } else {

            Location mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLocation == null) {
                getCurrentLocation();

            } else {
                latitude = mLocation.getLatitude();
                longitude = mLocation.getLongitude();
            }
        }
    }

    private void startSession(){
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        sessionsTransactions.startSession(getContext(), new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {

                if(response.getResult()){
                    runTimer = true;
                    timer.setVisibility(View.VISIBLE);
                    timerType.setVisibility(View.VISIBLE);
                    Calendar now = Calendar.getInstance();
                    startCalendar.setTime(now.getTime());
                    serverTime = convertLocalToDate();
//                    getServerTime();
                    startTimer();
                    isSessionStarted = true;
                    cancelSession.setVisibility(View.GONE);
                    startSession.setVisibility(View.GONE);
                    endSession.setVisibility(View.VISIBLE);
                }

                Toast.makeText(getContext(), response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
            }
        } , this, latitude, longitude);
        dialog.dismiss();
    }

    private void endSession() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        sessionsTransactions.end(getContext(), new Consumer<Response<SessionsTransactions.fetchEndSessionData>>() {
            @Override
            public void accept(Response<SessionsTransactions.fetchEndSessionData> response) throws Exception {

                if(response.getResult()){
                    isSessionStarted = false;
                    countDownTimer2.cancel();

                    if(response.getContent().getData().futureSessions.status != 3) {
                        // Payment still pending.
                        sessionDetailsLayout.setVisibility(View.GONE);
                        timerLayout.setVisibility(View.GONE);
                        paymentLayout.setVisibility(View.VISIBLE);

                        initPaymentView(response.getContent().getData().futureSessions.session_final_price,
                                response.getContent().getData().futureSessions.payment_type);
                    }
                    else {
                        //session completed
                        Constants.completedSessionId = requestModel.id;
                        sessionDetailsLayout.setVisibility(View.GONE);
                        timerLayout.setVisibility(View.GONE);
                        ratingLayout.setVisibility(View.VISIBLE);
                    }
                }
                Toast.makeText(getContext(), R.string.session_completed, Toast.LENGTH_SHORT).show();
            }
        } , this, latitude, longitude);
        dialog.dismiss();
    }

    private void cancelSession() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        sessionsTransactions.cancel(getContext(), new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {

                if(response.getResult()){
                    Toast.makeText(getContext(), R.string.session_cancelled, Toast.LENGTH_SHORT).show();
                    cancelSession.setVisibility(View.GONE);
                    startSession.setVisibility(View.GONE);
                }
            }
        } , this, requestModel.groupId, cancellationReason);
        dialog.dismiss();
    }

    private void paymentRecieved() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        sessionsTransactions.paymnetReceived(getContext(), new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {

                if(response.getResult()){
                    paymentLayout.setVisibility(View.GONE);
                    ratingLayout.setVisibility(View.VISIBLE);
                }
            }
        } , this);
        dialog.dismiss();
    }

    private void paymentRejected() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        sessionsTransactions.paymnetRejected(getContext(), new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {

                if(response.getResult()){
                    paymentLayout.setVisibility(View.GONE);
                    ratingLayout.setVisibility(View.VISIBLE);
                }
            }
        } , this);
        dialog.dismiss();
    }

    private void rateStudent() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        ratingGiven = ratingBar.getRating();
        String comment = review.getText().toString().trim();

        sessionsTransactions.rateStudent(getContext(), new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {

                if(response.getResult()){
                    ratingLayout.setVisibility(View.GONE);
                    getSessionSummary();
                }
            }
        } , this, ratingGiven, comment);
        dialog.dismiss();
    }

    private void getSessionSummary() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        sessionsTransactions.getSessionSummary(getContext(), new Consumer<Response<SessionsTransactions.sessionSummaryData>>() {
            @Override
            public void accept(Response<SessionsTransactions.sessionSummaryData> response) throws Exception {

                if(response.getResult()){
                    ratingLayout.setVisibility(View.GONE);
                    initSummaryView(response.getContent().getData().sessionData);
                }
            }
        } , this);
        dialog.dismiss();
    }

    private void getPaymentDetails() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(getContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        sessionsTransactions.getSessionSummary(getContext(), new Consumer<Response<SessionsTransactions.sessionSummaryData>>() {
            @Override
            public void accept(Response<SessionsTransactions.sessionSummaryData> response) throws Exception {

                if(response.getResult()){
                    paymentLayout.setVisibility(View.VISIBLE);
                    initPaymentView(response.getContent().getData().sessionData.session_final_price,
                            0);
                }
            }
        } , this);
        dialog.dismiss();
    }

    private void startTimer(){
        Date serverDate = convertDateToLocal(serverTime);
        final Calendar nowCalendar = Calendar.getInstance();
        nowCalendar.setTime(serverDate);
        countDownTimer2 = new CountDownTimer(7200000, 1000) {
            // 1000 means, onTick function will be called at every 1000 milliseconds

            @Override
            public void onTick(long leftTimeInMilliseconds) {
                nowCalendar.add(Calendar.SECOND, 1);
                leftTimeInMilliseconds = nowCalendar.getTimeInMillis() - startCalendar.getTimeInMillis();

                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(leftTimeInMilliseconds),
                        TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.HOURS.toMinutes(1));

                if (isSessionStarted) {
                    timer.setText(Constants.convertToArabic(hms));
                }
            }
            @Override
            public void onFinish() {
                timer.setText("00:00:00");
            }
        }.start();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        countDownTimer2.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        runTimer = true;
        getServerTime();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void enableView(View view, boolean enable) {

        if (enable) {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        view.setAlpha(enable ? 1f : 0.5f);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e("TAG" , "handleResponseErrorCodeForDebug");
    }

    private Date convertDateToLocal(String time) {
        DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date1 = null;
        try {
            date1 = inputFormatter.parse(time);
            return date1;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String convertLocalToDate() {
        Calendar calendar = Calendar.getInstance();
        DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date1 = calendar.getTime();
        String dateStr = inputFormatter.format(date1);

        return dateStr;
    }


    private void showCancellationDialog(){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = LayoutInflater.from(getContext());
        int layout = R.layout.fragment_dialog_cancel_session;
        final View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        Button yesBtn = (Button) dialogView.findViewById(R.id.yes_btn);
        Button noBtn = (Button) dialogView.findViewById(R.id.no_btn);
        TextView description = (TextView) dialogView.findViewById(R.id.description);
        final EditText reasonEt = (EditText) dialogView.findViewById(R.id.cancel_reason);

        if(isStudentNotAvailable) {
            description.setText(getResources().getString(R.string.student_cancel_txt));
            cancellationReason = getResources().getString(R.string.student_not_available_reason);
        }

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancellationReason = reasonEt.getText().toString().trim();
                if(cancellationReason.length() > 0) {
                    cancelSession();
                    finalCustomDialog.dismiss();
                }
                else {
                    Toast.makeText(getActivity(), R.string.cancel_reason_text, Toast.LENGTH_SHORT).show();
                }
            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = window.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.9;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void initSessionDetailsView(View view){
        sessionType = (TextView) view.findViewById(R.id.my_bookings_confirmed_item_type);
        sessionAt = (TextView) view.findViewById(R.id.my_bookings_confirmed_item_session_at);
        paymentPendingLayout = (RelativeLayout) view.findViewById(R.id.payment_pending_layout);
        subjectName = (TextView)  view.findViewById(R.id.my_bookings_confirmed_item_subject_name);
        grade = (TextView)  view.findViewById(R.id.my_bookings_confirmed_item_grade);
        level = (TextView)  view.findViewById(R.id.my_bookings_confirmed_item_level);
        location = (TextView)  view.findViewById(R.id.my_bookings_confirmed_item_location_tv);
        time = (TextView) view.findViewById(R.id.my_bookings_confirmed_item_time_from);
        timer = (TextView) view.findViewById(R.id.timer);
        timerType = (TextView) view.findViewById(R.id.type_title);

        call = (Button) view.findViewById(R.id.fragmentMyConfirmedBookings_call_btn);
        startSession = (Button) view.findViewById(R.id.start_session);
        endSession = (Button) view.findViewById(R.id.end_session);
        cancelSession = (Button) view.findViewById(R.id.cancel_session);

        UserLocation userLocation = new UserLocation(getActivity());

        setUpGoogleAPIClient();
        mGoogleApiClient.connect();

        String[] dates = requestModel.date.split(",");
        String[] timeFrom = requestModel.timeFrom.split(",");
        String[] timeTo = requestModel.timeTo.split(",");
        String[] session_name = requestModel.session_name.split(",");
        String[] id = requestModel.id.split(",");

        bookingId.setText(requestModel.bookingId);
        sessionId.setText(session_name[position]);
        sessionsTransactions.id = id[position];

        bookingTime = dates[position] + " " + timeFrom[position];

        teacherName.setText(requestModel.studentName);
//        gender.setText(requestModel.);
        date.setText(dates[position]);
        String languageToLoad = LanguageUtils.getUsedLang(getContext());
        if (languageToLoad.equalsIgnoreCase("ar")) {
            subjectName.setText(requestModel.course_name_in_ar);
            grade.setText(requestModel.grade_name_in_ar);
            level.setText(requestModel.level_name_in_ar);
        }
        else {
            subjectName.setText(requestModel.subjectName);
            grade.setText(requestModel.grade_name);
            level.setText(requestModel.level_name);
        }
        location.setText(requestModel.location);
        time.setText(timeFrom[position]+" - "+ timeTo[position]);

        if(requestModel.studentGender.equals("0")){
            gender.setText(getResources().getString(R.string.male));
        }
        else{
            gender.setText(getResources().getString(R.string.female));
        }

        if(requestModel.session_type.equals("0")){
            sessionType.setText(getContext().getResources().getString(R.string.once));
        }
        else {
            sessionType.setText(getContext().getResources().getString(R.string.monthly));
        }

        if(requestModel.session_location.equals("0")){
            sessionAt.setText(getContext().getResources().getString(R.string.student_house));
        }
        else{
            sessionAt.setText(getContext().getResources().getString(R.string.teacher_house));
        }

        if(requestModel.paidAmount == null && Float.parseFloat(requestModel.amount) > 0) {
            startSession.setVisibility(View.GONE);
            cancelSession.setVisibility(View.GONE);
            endSession.setVisibility(View.GONE);
            call.setVisibility(View.GONE);
            paymentPendingLayout.setVisibility(View.VISIBLE);
        }

        if(position == 0) {
            if (!requestModel.session_from_time.equalsIgnoreCase("0") && requestModel.session_to_time.equals("0")) {
                isSessionStarted = true;
                startCalendar.setTime(convertDateToLocal(requestModel.session_from_time));
                timer.setVisibility(View.VISIBLE);
                timerType.setVisibility(View.VISIBLE);
                cancelSession.setVisibility(View.GONE);
                startSession.setVisibility(View.GONE);
                endSession.setVisibility(View.VISIBLE);

                if (serverTime != null) {
                    startTimer();
                }
                else {
                    runTimer = true;
                }
            }
        }

        if(!Constants.completedSessionId.equals("0")) {
            timer.setVisibility(View.GONE);
            isSessionStarted = false;
//            countDownTimer2.cancel();
            timerType.setVisibility(View.GONE);
            timer.setVisibility(View.GONE);
            startSession.setVisibility(View.GONE);
        }

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        getActivity().requestPermissions(
                                new String[]{android.Manifest.permission.CALL_PHONE}, 0);
                    }
                }
                else{
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+requestModel.studentPhone));
                    getContext().startActivity(intent);
                }
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+requestModel.teacherLatitude+","+requestModel.teacherLongitude+
                                "&daddr="+requestModel.sessionLatitude+","+requestModel.sessionLongitude));
                getContext().startActivity(intent);
            }
        });

        startSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position == 0) {
                    if (!isSessionStarted) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                        builder.setMessage(getContext().getString(R.string.startSessionConfirmation));

                        builder.setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing but close the dialog
                                startSession();
                            }
                        });

                        builder.setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
                else {
                    Toast.makeText(getContext(), R.string.session_in_process, Toast.LENGTH_SHORT).show();
                }
            }
        });

        endSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                    builder.setMessage(getContext().getString(R.string.endSessionConfirmation));

                    builder.setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog
                            endSession();
                        }
                    });

                    builder.setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else {
                    Toast.makeText(getContext(), R.string.session_in_process, Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCancellationDialog();
            }
        });
    }

    private void initPaymentView(String amount, int paymnetType){
        float paidAmount = 0;
        if (requestModel.paidAmount != null) {
            paidAmount = Float.parseFloat(requestModel.paidAmount);
        }
        paymentNetAmount.setText(Constants.convertToArabic(Constants.distanceFormat.format((Float.parseFloat(amount) - paidAmount))));
        if(paymnetType == 0) {
            paymentDescription.setText(getContext().getResources().getString(R.string.payment_cash_description));
        }
        else if(paymnetType == 1) {
            paymentDescription.setText(getContext().getResources().getString(R.string.payment_card_description));
        }

        paymentReceived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentRecieved();
            }
        });

        paymentDeclined.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentRejected();
            }
        });
    }

    private void initSummaryView(SessionsTransactions sessionsTransactions){
        summaryLayout.setVisibility(View.VISIBLE);
        float sessionAmt = Float.parseFloat(sessionsTransactions.session_final_price);
        float mentorFee = Float.parseFloat(sessionsTransactions.mentor_fee);

        float netAmt = sessionAmt - mentorFee;

        long second = 1000l;
        long minute = 60l * second;
        long hour = 60l * minute;

        Date sessionStartTime = convertDateToLocal(sessionsTransactions.session_from_time);
        Date sessionEndTime = convertDateToLocal(sessionsTransactions.session_to_time);

        long diff = sessionEndTime.getTime() - sessionStartTime.getTime();

        sessionDuration.setText(Constants.convertToArabic(String.format("%02d", diff / hour) + ":"+ String.format("%02d", (diff % hour) / minute)
                + ":"+ String.format("%02d", (diff % minute) / second)));
        summarySessionAmount.setText(Constants.convertToArabic(Constants.distanceFormat.format(sessionAmt)));
        summaryNetAmount.setText(Constants.convertToArabic(Constants.distanceFormat.format(netAmt)));
        summaryMentorFees.setText(Constants.convertToArabic(Constants.distanceFormat.format(mentorFee)));
        summaryRatingBar.setRating(ratingGiven);
    }

    private void getServerTime() {

        final ACProgressFlower dialog = new ACProgressFlower.Builder(getContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.show();

        GlobalValuesData.APIInterface apiService =
                ApiClient.getClient(getContext()).create(GlobalValuesData.APIInterface.class);

        Call<GlobalValuesData> call = apiService.fetchGlobalValues("teacher", Mentor.getInstance().token);
        call.enqueue(new Callback<GlobalValuesData>() {
            @Override
            public void onResponse(Call<GlobalValuesData> call, retrofit2.Response<GlobalValuesData> response) {
                Log.d("TAG", "onResponse: "+response);
                if (response.isSuccessful()) {
                    GlobalValuesData verifyMobileResponse = response.body();
                    try {
                        if (verifyMobileResponse.getResult()) {
                            serverTime = verifyMobileResponse.getContent().getData().getServer_time();
                            isSessionAvailbleForStart();
                            if(runTimer) {
                                startTimer();
                                runTimer = false;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
//                        Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<GlobalValuesData> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void isSessionAvailbleForStart(){
        if(requestModel.session_from_time.equals("0") && !isSessionStarted) {
//            Log.d("TAG", "serverTime: "+serverTime);
//            Log.d("TAG", "bookingTime: "+bookingTime);
            Date serverDate = convertDateToLocal(serverTime);
            Date sessionDate = convertDateToLocal(bookingTime);
            long diffInMillisec = sessionDate.getTime() - serverDate.getTime();
//            long diffMinutes = diff / (60 * 1000) % 60;
            long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(diffInMillisec);

            Log.d("TAG", "diffMinutes: "+diffMinutes);
            if(diffMinutes > 30) {
                // start session not available.
                startSession.setVisibility(View.GONE);
            }
            else if(diffMinutes < 30 && diffMinutes >= -30){
                startSession.setVisibility(View.VISIBLE);
            }
            else if (diffMinutes <-30) {
                startSession.setVisibility(View.GONE);
                isStudentNotAvailable = true;
            }
        }
    }
}
