package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import android.content.DialogInterface;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.HelpItemDialogFragmentVM;
import com.applexicon.thementorpartner.databinding.FragmentHelpItemDialogBinding;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HelpItemDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HelpItemDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelpItemDialogFragment extends DialogFragment{

    private static final String ARG_URL_EXTRA = "arg_url";
    private  String TAG = "HelpItemDialogFragment" ;

    private OnFragmentInteractionListener mListener;
    public FragmentHelpItemDialogBinding binding;
    public Session session;
    public String url;
    private View view;

    public HelpItemDialogFragment() {
        // Required empty public constructor
    }

    public static HelpItemDialogFragment newInstance(String url) {
        HelpItemDialogFragment fragment = new HelpItemDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_URL_EXTRA , url);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            url = getArguments().getString(ARG_URL_EXTRA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_help_item_dialog, container, false);

        binding =  DataBindingUtil.bind(view);

        HelpItemDialogFragmentVM viewModel = new HelpItemDialogFragmentVM(this);

        binding.setHelpItemDialogFragmentVM(viewModel);

//        String summary = "<html><body>You scored <b>192</b> points.</body></html>";
//        webView.loadData(summary, "text/html", null);


        return view;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
