package com.applexicon.thementorpartner.views.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.applexicon.thementorpartner.views.fragments.MyCompletedSessionsFragment;
import com.applexicon.thementorpartner.views.fragments.MyCanceledSessionsFragment;
import com.applexicon.thementorpartner.views.fragments.MyConfirmedSessionsFragment;


/**
 * Created by AppleXIcon on 31/7/2017.
 */

public class MySessionsFragmentAdapter extends FragmentPagerAdapter {
    public static int int_items = 3 ;
    private String mTabsTitle[];

    public MySessionsFragmentAdapter(FragmentManager fm , String tabTitle[]) {
        super(fm);
        this.mTabsTitle = tabTitle;


    }

    /**
     * Return fragment with respect to Position .
     */

    @Override
    public Fragment getItem(int position)
    {
        switch (position){
            case 0 :
                return new MyConfirmedSessionsFragment();
            case 1 :
                return new MyCompletedSessionsFragment();
            case 2 :
                return new MyCanceledSessionsFragment();
        }
        return null;
    }

    @Override
    public int getCount() {

        return   int_items;

    }

    /**
     * This method returns the title of the tab according to the position.
     */

    @Override
    public CharSequence getPageTitle(int position) {

        return mTabsTitle[position];


}

}
