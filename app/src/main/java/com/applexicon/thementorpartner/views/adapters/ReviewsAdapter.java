package com.applexicon.thementorpartner.views.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.Reviews;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;
import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {

    private Context context;
    private List<Reviews> reviewsList = new ArrayList<>();

    public ReviewsAdapter(Context context, List<Reviews> reviewsList) {
        this.context = context;
        this.reviewsList = reviewsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_reviews, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Reviews reviews = reviewsList.get(position);
        String date[] = reviews.created_at.split(" ");
        holder.sessionDate.setText(date[0]);
        holder.sessionId.setText(reviews.session_name);
        holder.studentName.setText(""+reviews.student_id);
        holder.reviewText.setText(reviews.comment);
        holder.ratingBar.setRating(Float.parseFloat(reviews.rate));
    }

    @Override
    public int getItemCount() {
        return reviewsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView sessionDate;
        TextView sessionId;
        TextView studentName;
        TextView reviewText;
        SimpleRatingBar ratingBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            sessionDate = (TextView) itemView.findViewById(R.id.reviews_date);
            sessionId = (TextView) itemView.findViewById(R.id.reviews_session_id);
            studentName = (TextView) itemView.findViewById(R.id.reviews_student_name);
            reviewText = (TextView) itemView.findViewById(R.id.reviews_text);
            ratingBar = (SimpleRatingBar) itemView.findViewById(R.id.reviews_ratingbar);
        }
    }
}
