package com.applexicon.thementorpartner.views.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.ConfirmedSessionFiltered;
import com.applexicon.thementorpartner.utilities.LanguageUtils;


public class SessionsListAdapter extends RecyclerView.Adapter<SessionsListAdapter.MyViewHolder> {

    private Context context;
    String[] date;
    String[] timeFrom;
    String[] timeTo;
    String[] id;
    ConfirmedSessionFiltered model;

    public SessionsListAdapter(Context context, String[] date, String[] timeFrom, String[] timeTo, String[] id,
                               ConfirmedSessionFiltered model){
        this.context = context;
        this.date = date;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.id = id;
        this.model = model;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sessions_list, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.sessionDate.setText(date[position]);
        holder.sessionTime.setText(timeFrom[position] + " - "+ timeTo[position]);
        holder.userName.setText(model.userName);
        holder.studentName.setText(model.studentName);
        String languageToLoad = LanguageUtils.getUsedLang(context);
        if (languageToLoad.equalsIgnoreCase("ar")) {
            holder.subjectName.setText(model.course_name_in_ar);
            holder.grade.setText(model.grade_name_in_ar);
            holder.level.setText(model.level_name_in_ar);
        }
        else {
            holder.subjectName.setText(model.subjectName);
            holder.grade.setText(model.grade_name);
            holder.level.setText(model.level_name);
        }
        holder.bookingId.setText(model.bookingId);
        holder.sessionId.setText(id[position]);

        if(model.studentGender.equals("0")) {
            holder.studentGender.setText(context.getResources().getString(R.string.male));
        }
        else if(model.studentGender.equals("1")) {
            holder.studentGender.setText(context.getResources().getString(R.string.female));
        }
    }

    @Override
    public int getItemCount() {
        return date.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView sessionDate, sessionTime, userName, studentName, studentGender, subjectName, grade, level,
                bookingId, sessionId;
        Button viewButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            sessionDate = (TextView) itemView.findViewById(R.id.my_bookings_confirmed_item_date);
            bookingId = (TextView) itemView.findViewById(R.id.booking_id);
            sessionId = (TextView) itemView.findViewById(R.id.session_id);
            sessionTime = (TextView) itemView.findViewById(R.id.my_bookings_confirmed_item_time_from);
            userName = (TextView) itemView.findViewById(R.id.teacher_name);
            studentName = (TextView) itemView.findViewById(R.id.student_name);
            studentGender = (TextView) itemView.findViewById(R.id.student_gender);
            subjectName = (TextView) itemView.findViewById(R.id.my_bookings_confirmed_item_subject_name);
            grade = (TextView) itemView.findViewById(R.id.my_bookings_confirmed_item_grade);
            level = (TextView) itemView.findViewById(R.id.my_bookings_confirmed_item_level);
            viewButton = (Button) itemView.findViewById(R.id.mySessionsConfirmedItem_requestForPay_btn);

            viewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    com.applexicon.thementorpartner.views.fragments.SessionDetailsFragment.newInstance(context, model, getAdapterPosition());
                }
            });
        }
    }
}
