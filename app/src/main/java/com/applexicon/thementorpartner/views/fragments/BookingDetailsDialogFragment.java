package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.applexicon.thementorpartner.databinding.FragmentRequestDetailsDialogBinding;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.SessionRequestModel;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.viewModel.BookingDetailsVM;
import com.applexicon.thementorpartner.views.activities.MainActivity;

import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BookingDetailsDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BookingDetailsDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingDetailsDialogFragment extends DialogFragment implements Response.ResponseErrorCodeHandler {

    private static final String SESSION_ARG = "session_arg";
    private  String TAG = "BookingDetail" ;

    private OnFragmentInteractionListener mListener;
    public FragmentRequestDetailsDialogBinding binding;
    public SessionRequestModel sessionRequest;
    private View acceptBtn;
    private View rejectBtn;

    public BookingDetailsDialogFragment() {
        // Required empty public constructor
    }

    public static BookingDetailsDialogFragment newInstance(SessionRequestModel requestModel) {
        BookingDetailsDialogFragment fragment = new BookingDetailsDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(SESSION_ARG , requestModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sessionRequest =(SessionRequestModel) getArguments().getSerializable(SESSION_ARG);

            Mentor.getInstance().setSessionRequest(sessionRequest);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_request_details_dialog, container, false);
        View view = binding.getRoot();
        final BookingDetailsVM vewModel =  new BookingDetailsVM(this);
        binding.setBookingDetailsVM(vewModel);
        acceptBtn = view.findViewById(R.id.fragmentBookingDetailsDialog_accept_btn);
        rejectBtn = view.findViewById(R.id.fragmentBookingDetailsDialog_reject_btn);

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptBtnClicked(view);
            }
        });

        rejectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejectBtnClicked(view);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;

        window.setLayout((int) (width * 0.92), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

    }

    private void acceptBtnClicked(View view) {
        Log.e(TAG , "acceptBtnClicked");

      disableDialog();

        Mentor.getInstance().acceptSession(getActivity(), new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                enableDialog();
                if(response.getResult()){
                    //show dialog accept
                    SessionAddedDialogFragment.newInstance().show(getChildFragmentManager() , "SessionAddedDialogFragment");
//                    addSessionToAlarmManager();
                    refreshSessionRequests();
                    dismiss();
                }

//                Toast.makeText(getActivity(), response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
            }
        } , this);

    }

    private void addSessionToAlarmManager() {
        sessionRequest.addToAlarmManager(getActivity());
    }



    private void rejectBtnClicked(View view) {
        Log.e(TAG , "rejectBtnClicked");

        SessionRejectAlertDialog.newInstance().show(getActivity().getSupportFragmentManager() , "SessionAddedDialogFragment");

        dismiss();
    }


    private void enableDialog() {
        rejectBtn.setEnabled(true);
        rejectBtn.setAlpha(1f);

        acceptBtn.setEnabled(true);
        acceptBtn.setAlpha(1f);
        setCancelable(true);
    }

    private void disableDialog() {
        rejectBtn.setEnabled(false);
        rejectBtn.setAlpha(0.5f);
        acceptBtn.setEnabled(false);
        acceptBtn.setAlpha(0.5f);
        setCancelable(false);
    }



    private void refreshSessionRequests() {
        ((MainActivity)getActivity()).homeFragment.vewModel.fetchSessions();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            mListener.setBookingDetailsDialogFragment(this);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        enableDialog();
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void setBookingDetailsDialogFragment(BookingDetailsDialogFragment bookingDetailsDialogFragment);
    }
}
