package com.applexicon.thementorpartner.views.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.utilities.Constants;
import com.applexicon.thementorpartner.utilities.LanguageUtils;

import java.util.List;
import java.util.Locale;


/**
 * Created by CS on 06-07-2016.
 */
public class WebViewActivity extends AppCompatActivity {
    TextView screenTitle;
    private ProgressBar mProgressBar;
    ImageView backBtn;
    String data;
    String url;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_activity);

        String languageToLoad = LanguageUtils.getUsedLang(getApplicationContext());
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());



        WebView wv = (WebView) findViewById(R.id.webView);
        screenTitle = (TextView) findViewById(R.id.header_title);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        mProgressBar.setVisibility(View.VISIBLE);

        if (languageToLoad.equalsIgnoreCase("ar")) {
            backBtn.setRotation(180);
        }

        screenTitle.setText(getIntent().getExtras().getString("class"));
        if(getIntent().getExtras().getString("class").equals(getResources().getString(R.string.about_the_mentor))){
            if (LanguageUtils.getUsedLang(WebViewActivity.this).equalsIgnoreCase("en")){
                url = String.format(Constants.HELP_URL, "about_en_teacher", LanguageUtils.getUsedLang(WebViewActivity.this));
            }
            else {
                url = String.format(Constants.HELP_URL, "about_ar_teacher", LanguageUtils.getUsedLang(WebViewActivity.this));
            }
        }
        else if(getIntent().getExtras().getString("class").equals(getResources().getString(R.string.contact))) {
            if (LanguageUtils.getUsedLang(WebViewActivity.this).equalsIgnoreCase("en")) {
                url = String.format(Constants.HELP_URL, "contact_en_teacher", LanguageUtils.getUsedLang(WebViewActivity.this));
            }
            else  {
                url = String.format(Constants.HELP_URL, "contact_ar_teacher", LanguageUtils.getUsedLang(WebViewActivity.this));
            }
        }
        else if(getIntent().getExtras().getString("class").equals(getResources().getString(R.string.terms_and_conditions))) {
            if (LanguageUtils.getUsedLang(WebViewActivity.this).equalsIgnoreCase("en")) {
                url = String.format(Constants.HELP_URL, "terms_en_teacher", LanguageUtils.getUsedLang(WebViewActivity.this));
            }
            else {
                url = String.format(Constants.HELP_URL, "terms_ar_teacher", LanguageUtils.getUsedLang(WebViewActivity.this));
            }
        }
        else if(getIntent().getExtras().getString("class").equals(getResources().getString(R.string.privacy_policy))) {
            if (LanguageUtils.getUsedLang(WebViewActivity.this).equalsIgnoreCase("en")) {
                url = String.format(Constants.HELP_URL, "policy_en_teacher", LanguageUtils.getUsedLang(WebViewActivity.this));
            }
            else {
                url = String.format(Constants.HELP_URL, "policy_ar_teacher", LanguageUtils.getUsedLang(WebViewActivity.this));
            }
        }
//        wv.loadDataWithBaseURL("", data, "text/html", "UTF-8", "");
        wv.loadUrl(url);
        wv.setWebViewClient(new MyWebViewClient());
        wv.getSettings().setLoadsImagesAutomatically(true);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(url != null && url.startsWith("https://api.whatsapp.com")) {
                String[] splitURL = url.split("=");
                openWhatsapp(splitURL[1]);
            }
            else if(url != null && url.startsWith("https://mailto:")) {
                String[] splitUrl = url.split(":");
                openGmail(splitUrl[2]);
            }
            else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mProgressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }

    private void openGmail(String email){
        email = email.replace("/", "");
        Intent i = null;
        try {
            i = new Intent(Intent.ACTION_SEND);
            i.setType("plain/text");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
            final PackageManager pm = getPackageManager();
            final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
            String className = null;
            for (final ResolveInfo info : matches) {
                if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                    className = info.activityInfo.name;

                    if(className != null && !className.isEmpty()){
                        break;
                    }
                }
            }
            i.setClassName("com.google.android.gm", className);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(WebViewActivity.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void openWhatsapp(String MobileNumber){
        try {
            PackageManager packageManager = getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url1 = "whatsapp://send?phone="+ MobileNumber;
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url1));
            if (i.resolveActivity(packageManager) != null) {
                startActivity(i);
            }
        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(WebViewActivity.this, "Whatsapp not installed ", Toast.LENGTH_SHORT).show();
        }
    }
}
