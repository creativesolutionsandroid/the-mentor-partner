package com.applexicon.thementorpartner.views.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.MentorApp;
import com.applexicon.thementorpartner.R;

public class SplashActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Mentor mentor = ((MentorApp) getApplication()).mentor;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = "1";
            String channelName = "New Session";
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_HIGH));
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(mentor.isLoggedIn(getApplicationContext())){
                    MainActivity.start(SplashActivity.this);
                }
                else{
                    UserActivity.start(SplashActivity.this);
                }

                finish();

            }
        } , 3000);


    }


}
