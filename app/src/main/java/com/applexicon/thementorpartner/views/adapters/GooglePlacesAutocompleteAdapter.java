package com.applexicon.thementorpartner.views.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;


import com.applexicon.thementorpartner.models.PlaceModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by ${Adel} on 29/01/17.
 */

public class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    public static final String API_KEY = "AIzaSyDZZNRKAws3SM17bZHrBYLRGR6Vf0VSfpU";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    final String LOG_TAG = "TAG";
    private ArrayList resultList;
    private ArrayList<PlaceModel> placeModels;
    private String TAG ="AutocompleteAdapter";


    public GooglePlacesAutocompleteAdapter(Context context, int resource) {
        super(context, resource);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                Log.e(TAG , "performFiltering " + constraint);

                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    resultList = autocomplete(constraint.toString());

                    Log.e("resultList" , resultList.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {

        return resultList.get(position);
    }

    String country ="sa";

    ArrayList autocomplete(String input) {

//        if (!GetAddressFromLocation.country.equals("")||GetAddressFromLocation.country !=null )
//            country = GetAddressFromLocation.country;

        Log.e("Country","Country:"+ country);

        ArrayList resultList = null;
        placeModels=new ArrayList<>();
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:"+country);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            Log.e(TAG , "autocomplete url " + sb.toString()  );

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());

            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                PlaceModel placeMode = new PlaceModel();
                placeMode.setName(predsJsonArray.getJSONObject(i).getString("description"));
                placeMode.setId(predsJsonArray.getJSONObject(i).getString("place_id"));
                resultList.add(placeMode.getName());
                placeModels.add(placeMode);

            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    public ArrayList<PlaceModel> getList() {
        return placeModels;
    }
}
