
package com.applexicon.thementorpartner.views.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.databinding.ItemRequestSessionLayoutBinding;
import com.applexicon.thementorpartner.models.SessionRequestModel;
import com.applexicon.thementorpartner.viewModel.ItemSessionRequestVM;
import com.applexicon.thementorpartner.views.fragments.HomeFragment;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SessionRequestsAdapter extends RecyclerView.Adapter<SessionRequestsAdapter.SessionsAdapterViewHolder> {

  public final HomeFragment mHomeFragment;
  private List<SessionRequestModel> sessionsRequestsList;

  public SessionRequestsAdapter(HomeFragment homeFragment) {
    mHomeFragment = homeFragment;
    this.sessionsRequestsList = Collections.emptyList();
  }

  @Override public SessionsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ItemRequestSessionLayoutBinding itemBinding =
        DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_request_session_layout,
            parent, false);
    return new SessionsAdapterViewHolder(itemBinding);
  }

  @Override public void onBindViewHolder(SessionsAdapterViewHolder holder, int position) {
    holder.bindPeople(sessionsRequestsList.get(position) , mHomeFragment);
  }

  @Override public int getItemCount() {
    return sessionsRequestsList.size();
  }

  public void setSessionsRequestsList(ArrayList<SessionRequestModel> list) {
    this.sessionsRequestsList = list;
    notifyDataSetChanged();
  }

  public static class SessionsAdapterViewHolder extends RecyclerView.ViewHolder {
    ItemRequestSessionLayoutBinding itemBinding;

    public SessionsAdapterViewHolder(ItemRequestSessionLayoutBinding itemBinding) {
      super(itemBinding.itemSessionLayoutItemSessionRl);
      this.itemBinding = itemBinding;
    }

    void bindPeople(SessionRequestModel request, HomeFragment mHomeFragment) {
      if (itemBinding.getItem() == null) {
        itemBinding.setItem(
            new ItemSessionRequestVM(request, mHomeFragment));
      } else {
        itemBinding.getItem().setRequest(request);
      }
    }
  }
}
