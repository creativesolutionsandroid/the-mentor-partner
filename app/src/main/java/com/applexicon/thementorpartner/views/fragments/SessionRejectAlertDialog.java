package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.activities.MainActivity;

import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SessionRejectAlertDialog.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SessionRejectAlertDialog#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SessionRejectAlertDialog extends DialogFragment {


    private OnFragmentInteractionListener mListener;
    private String TAG = "SessRejtAlertDialog";
    private Button yesBtn;
    private Button closeBtn;

    public SessionRejectAlertDialog() {
        // Required empty public constructor
    }

    public static SessionRejectAlertDialog newInstance() {
        return new SessionRejectAlertDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_session_reject_alert_dialog, container, false);

         yesBtn =  (Button)view.findViewById(R.id.fragmentSessionRejectAlertDialog_yes_btn);
         closeBtn =  (Button)view.findViewById(R.id.fragmentSessionRejectAlertDialog_close_btn);

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejectSession();
            }
        });


        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               dismiss();
            }
        });
        return  view;
    }

    private void rejectSession() {

        disableDialog();

        Mentor.getInstance().rejectSession(getActivity(), new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {

                Toast.makeText(getActivity(), response.getContent().getMessage(), Toast.LENGTH_SHORT).show();

                enableDialog();


                if (response.getResult()) {
                    //show dialog reject
//                    refreshSessionRequests();
                    startHomeFragment();

                    dismiss();
                }



            }
        }, new Response.ResponseErrorCodeHandler() {
            @Override
            public void handleResponseErrorCodeForUser(String message) {
                Log.e(TAG , "handleResponseErrorCodeForUser");
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                enableDialog();
            }

            @Override
            public void handleResponseErrorCodeForDebug(String message) {
                Log.e(TAG , "handleResponseErrorCodeForDebug");
                enableDialog();
            }
        });

//        ((MainActivity)getActivity()).bookingDetailsDialogFragment.rejectSession(context);
    }

    private void enableDialog() {
        yesBtn.setEnabled(true);
        yesBtn.setAlpha(1f);

        closeBtn.setEnabled(true);
        closeBtn.setAlpha(1f);
        setCancelable(true);
    }

    private void disableDialog() {
        yesBtn.setEnabled(false);
        yesBtn.setAlpha(0.5f);
        closeBtn.setEnabled(false);
        closeBtn.setAlpha(0.5f);
        setCancelable(false);
    }

    private void startHomeFragment() {
        FragmentTransaction transactionFragment = getActivity().getSupportFragmentManager().beginTransaction();
        HomeFragment newFragment = new HomeFragment();
        transactionFragment.replace(R.id.main_container, newFragment).commit();
        ((MainActivity)getActivity()).navigationView.setCheckedItem(R.id.nav_home);
    }

    @Override
    public void onStart() {
        super.onStart();


        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;

        window.setLayout((int) (width * 0.92), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
