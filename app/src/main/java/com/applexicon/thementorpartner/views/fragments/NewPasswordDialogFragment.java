package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.NewPasswordFragmentDialogVM;
import com.applexicon.thementorpartner.databinding.FragmentDialogNewPasswordBinding;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewPasswordDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewPasswordDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewPasswordDialogFragment extends DialogFragment {


    private static final String PHONE_PARAM = "phone_param";
    private OnFragmentInteractionListener mListener;
    public String phone;

    public NewPasswordDialogFragment() {
        // Required empty public constructor
    }

    public static NewPasswordDialogFragment newInstance(String phone) {
        NewPasswordDialogFragment fragment = new NewPasswordDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString(PHONE_PARAM , phone);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        if(bundle != null){
            phone= bundle.getString(PHONE_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_session_added_dialog, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        FragmentDialogNewPasswordBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dialog_new_password, container, false);
        View view = binding.getRoot();
        NewPasswordFragmentDialogVM vewModel =  new NewPasswordFragmentDialogVM(this, binding);
        binding.setNewPasswordFragmentVM(vewModel);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NewPasswordDialogFragment.OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
