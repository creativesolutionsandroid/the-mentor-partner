/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.views.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.applexicon.thementorpartner.models.CanceledSession;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.ItemSessionCanceledVM;
import com.applexicon.thementorpartner.views.fragments.MyCanceledSessionsFragment;
import com.applexicon.thementorpartner.databinding.MySessionsCanceledItemBinding;

import java.util.Collections;
import java.util.List;

public class SessionCanceledAdapter extends RecyclerView.Adapter<SessionCanceledAdapter.ViewHolder> {

    public final MyCanceledSessionsFragment myCanceledSessionsFragment;
    private List<CanceledSession> dataList;

    public SessionCanceledAdapter(MyCanceledSessionsFragment fragment) {
        myCanceledSessionsFragment = fragment;
        this.dataList = Collections.emptyList();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MySessionsCanceledItemBinding Binding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.my_sessions_canceled_item,
                        parent, false);
        return new ViewHolder(Binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindPeople(dataList.get(position), myCanceledSessionsFragment);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setDataList(List<CanceledSession> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        MySessionsCanceledItemBinding itemBinding;

        public ViewHolder(MySessionsCanceledItemBinding itemBinding) {
            super(itemBinding.itemContainerRl);
            this.itemBinding = itemBinding;
        }

        void bindPeople(CanceledSession model, MyCanceledSessionsFragment fragment) {

            if (itemBinding.getItemSessionCanceledVM() == null) {
                itemBinding.setItemSessionCanceledVM(
                        new ItemSessionCanceledVM(model, fragment));
            } else {
                itemBinding.getItemSessionCanceledVM().setModel(model);
            }

            if (Float.parseFloat(model.cancel_amount) > 0) {
                if(model.cancel_by != null && model.cancel_by.equalsIgnoreCase("teacher")) {
                    itemBinding.cancelLayout.setVisibility(View.VISIBLE);
                }
                else {
                    itemBinding.cancelLayout.setVisibility(View.GONE);
                }
            } else {
                itemBinding.cancelLayout.setVisibility(View.GONE);
            }
        }
    }
}
