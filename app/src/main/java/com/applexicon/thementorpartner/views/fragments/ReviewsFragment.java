package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.Reviews;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.views.adapters.ReviewsAdapter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReviewsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReviewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReviewsFragment extends Fragment implements Response.ResponseErrorCodeHandler{

    private OnFragmentInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private ReviewsAdapter mAdapter;
    private RelativeLayout loadingDialog;
    List<Reviews> reviewsList = new ArrayList<>();
    TextView noReviews;

    public ReviewsFragment() {
        // Required empty public constructor
    }

    public static ReviewsFragment newInstance() {
        ReviewsFragment fragment = new ReviewsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(R.string.my_reviews);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_reviews, container, false);

        loadingDialog = (RelativeLayout) view.findViewById(R.id.dialog_layout);
        noReviews = (TextView) view.findViewById(R.id.no_reviews);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_my_reviews_recycler);

        fetchReview();
        return view;
    }

    private void fetchReview(){
        loadingDialog.setVisibility(View.VISIBLE);
        Reviews reviews = new Reviews();
        reviews.fetchReviews(getContext() ,new Consumer<Response<Reviews.FetchReviewsContentData>>() {
            @Override
            public void accept(Response<Reviews.FetchReviewsContentData> response) throws Exception {
                if (response.getResult()) {
                    loadingDialog.setVisibility(View.GONE);
                    reviewsList = response.getContent().getData().reviews;
                    if(reviewsList.size() > 0){
                        mAdapter = new ReviewsAdapter(getContext(), reviewsList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                        mRecyclerView.setLayoutManager(mLayoutManager);
                        mRecyclerView.addItemDecoration(new MyItemDecoration(getContext()));
                        mRecyclerView.setAdapter(mAdapter);
                    }
                    else {
                        noReviews.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    loadingDialog.setVisibility(View.GONE);
                    handleResponseErrorCodeForUser(response.getContent().getMessage());
                    handleResponseErrorCodeForDebug(response.getContent().getMessage());
                }
            }
        }, this);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e("TAG" , "handleResponseErrorCodeForDebug");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class MyItemDecoration extends RecyclerView.ItemDecoration {

        private final int decorationHeight;
        private Context context;

        public MyItemDecoration(Context context) {
            this.context = context;
            decorationHeight = 15;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            if (parent != null && view != null) {

                int itemPosition = parent.getChildAdapterPosition(view);
                int totalCount = parent.getAdapter().getItemCount();

                if (itemPosition >= 0 && itemPosition < totalCount - 1) {
                    outRect.bottom = decorationHeight;
                }

            }

        }
    }
}
