package com.applexicon.thementorpartner.views.fragments;

import android.content.DialogInterface;
import androidx.databinding.DataBindingUtil;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.databinding.FragmentSessionRateDialogBinding;
import com.applexicon.thementorpartner.viewModel.RatingFragmentDialogVM;


public class RatingDialogFragment extends DialogFragment {

    private static final String ARG_SESSION = "arg_session";
    private static final String ARG_SEARCH = "arg_search";
    private static final java.lang.String ARG_TEACHER_IMAGE_URL = "teacher_image_url";
//    private OnFragmentInteractionListener mListener;
    private String TAG = "RatingDialogFragment";
    public FragmentSessionRateDialogBinding binding;
    public String sessionId;
    public String teacherImageUrl;

    public RatingDialogFragment() {
        // Required empty public constructor
    }

    public static RatingDialogFragment newInstance(String sessionId, String teacherImageUrl) {

        Log.d("TAG", "newInstance: "+sessionId);
        RatingDialogFragment fragment = new RatingDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SESSION, sessionId);
        args.putString(ARG_TEACHER_IMAGE_URL, teacherImageUrl);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            sessionId = getArguments().getString(ARG_SESSION);
            Log.d(TAG, "onCreate: "+sessionId);
            teacherImageUrl = getArguments().getString(ARG_TEACHER_IMAGE_URL);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
      getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

         binding = DataBindingUtil.inflate(inflater, R.layout.fragment_session_rate_dialog, container, false);
        View view = binding.getRoot();
        RatingFragmentDialogVM vewModel = new RatingFragmentDialogVM(this);
        binding.setRatingFragmentDialogVM(vewModel);

        setCancelable(false);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        Log.e(TAG , "onButtonPressed : uri " + uri.toString());
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
//        getActivity().finish();
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    @Override
    public void onStart() {
        super.onStart();


        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;

        window.setLayout((int) (width * 0.90), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

    }

//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//
//    }
}
