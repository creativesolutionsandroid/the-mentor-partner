package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import android.content.DialogInterface;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.PaymentRequestVM;
import com.applexicon.thementorpartner.databinding.FragmentSessionStartAlertDialogBinding;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PaymentRequestDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PaymentRequestDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentRequestDialogFragment extends DialogFragment{

    private static final String SESSION_ARG = "session_arg";
    private  String TAG = "BookingDetail" ;


    private OnFragmentInteractionListener mListener;
    public FragmentSessionStartAlertDialogBinding binding;
    public Session session;
    DialogFragment mDialogFragment;
    public PaymentRequestDialogFragment() {
        // Required empty public constructor
    }


    public static PaymentRequestDialogFragment newInstance(Session session) {
        PaymentRequestDialogFragment fragment = new PaymentRequestDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Session.ARG_SESSION_EXTRA , session);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialogFragment = this;
        mDialogFragment.setCancelable(false);
        if (getArguments() != null) {
            session =(Session) getArguments().getSerializable(Session.ARG_SESSION_EXTRA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_session_start_alert_dialog, container, false);
        View view = binding.getRoot();
        final PaymentRequestVM vewModel =  new PaymentRequestVM(this);
        binding.setPaymentRequestVM(vewModel);

        return view;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        getActivity().finish();
        super.onDismiss(dialog);

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
