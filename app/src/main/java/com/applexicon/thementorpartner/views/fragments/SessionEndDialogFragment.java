package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import android.content.DialogInterface;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.EndSessionVM;
import com.applexicon.thementorpartner.databinding.FragmentSessionEndDialogBinding;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SessionEndDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SessionEndDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SessionEndDialogFragment extends DialogFragment {


    private  String TAG = "SessionEndDialogFragment" ;


    private OnFragmentInteractionListener mListener;
    public FragmentSessionEndDialogBinding binding;
    public Session session;
    DialogFragment mDialogFragment;

    public SessionEndDialogFragment() {
        // Required empty public constructor
    }


    public static SessionEndDialogFragment newInstance(Session session) {
        SessionEndDialogFragment fragment = new SessionEndDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Session.ARG_SESSION_EXTRA , session);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialogFragment = this;
        mDialogFragment.setCancelable(false);

        if (getArguments() != null) {
            session =(Session) getArguments().getSerializable(Session.ARG_SESSION_EXTRA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_session_end_dialog, container, false);
        View view = binding.getRoot();
        final EndSessionVM vewModel =  new EndSessionVM(this);
        binding.setEndSessionVM(vewModel);

        return view;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (getActivity() != null)
            getActivity().finish();

        super.onDismiss(dialog);

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
