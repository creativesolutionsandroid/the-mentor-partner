package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.databinding.FragmentSessionDetailsDialogBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SessionDetailsDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SessionDetailsDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SessionDetailsDialogFragment extends DialogFragment implements Response.ResponseErrorCodeHandler {

    private static final String SESSION_ARG = "session_arg";
    private  String TAG = "BookingDetail" ;


    private OnFragmentInteractionListener mListener;
    public FragmentSessionDetailsDialogBinding binding;
    public Session session;

    public SessionDetailsDialogFragment() {
        // Required empty public constructor
    }


    public static SessionDetailsDialogFragment newInstance(Session session) {
        SessionDetailsDialogFragment fragment = new SessionDetailsDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(SESSION_ARG , session);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            session =(Session) getArguments().getSerializable(SESSION_ARG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_session_details_dialog, container, false);
        View view = binding.getRoot();


//        final BookingDetailsVM vewModel =  new BookingDetailsVM(this);
//        binding.setBookingDetailsVM(vewModel);

        binding.setVm(this);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;

        window.setLayout((int) (width * 0.92), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    public String getStudentName() {
        return session.studentName;
    }

    public String getSessionLocation() {
        return session.sessionLocation;
    }

    public String getTimeFrom() {
        return session.timeFrom;
    }

    public String getTimeTo() {
        return session.timeTo;
    }

    public String getDate() {

        return getData(session.date);
    }


    private String getData(String deliverydate) {
        Log.e(TAG,"deliverydate:" +deliverydate);

        SimpleDateFormat incoming =new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = null;
        try {
            currentDate = (Date) incoming.parse(deliverydate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat converted =new SimpleDateFormat("dd MMMM, yyyy" , Locale.getDefault());

        Log.e(TAG,"format(date):" +converted.format(currentDate));

        return converted.format(currentDate);
    }

    public String getSchedule() {
        String schedule = "";

        switch (session.schedule) {
            case "0":
                schedule = getString(R.string.once);
                break;

            case "1":
                schedule = getString(R.string.monthly);
                break;
        }

        return schedule;
    }
}
