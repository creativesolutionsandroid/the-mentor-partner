package com.applexicon.thementorpartner.views.activities;

import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.views.fragments.PaymentRequestDialogFragment;
import com.applexicon.thementorpartner.views.fragments.SessionEndDialogFragment;

import java.util.Locale;

public class PaymentRequestAlarmActivity extends AppCompatActivity implements
        PaymentRequestDialogFragment.OnFragmentInteractionListener,
        SessionEndDialogFragment.OnFragmentInteractionListener {

    private String TAG ="StartSessAlarmActivity";
    private Session session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        String languageToLoad = LanguageUtils.getUsedLang(getApplicationContext());
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());



        String action =  getIntent().getAction();

         session =  (Session)getIntent().getSerializableExtra(Session.ARG_SESSION_EXTRA);

        Log.e(TAG , "onCreate action : " + action );
        Log.e(TAG , "onCreate sessionRequest id  : " + session.id );
        Log.e(TAG , "onCreate sessionRequest timeFrom  : " + session.timeFrom );
        Log.e(TAG , "onCreate sessionRequest timeTo  : " + session.timeTo );

        if(action.equals(Session.START_SESSION_ACTION)){
            displayStartSessionDialog();
        }

    }

    @Override
    protected void onDestroy() {

        Log.e(TAG , "onDestroy");
        super.onDestroy();
    }

    private void displayStartSessionDialog()
    {

        PaymentRequestDialogFragment.newInstance(session).show(getSupportFragmentManager() , "PaymentRequestDialogFragment");

    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
