package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.EditProfile;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.activities.MainActivity;

import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PreferencesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PreferencesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreferencesFragment extends Fragment implements com.applexicon.thementorpartner.response.Response.ResponseErrorCodeHandler{

    private OnFragmentInteractionListener mListener;
    private AppCompatSpinner genderToTeachSpinner, sessionLocationSpinner, sessionTypeSpinner;
    private TextView genderToTeachError, sessionLocationError, sessionTypeError;
    private String sexToTeach = null;
    private String preferredLocation = null;
    private String sessionType = null;
    private Button updateButton;
    private RelativeLayout loadingDialog;
    private EditProfile editProfile = new EditProfile();

    public PreferencesFragment() {
        // Required empty public constructor
    }

    public static PreferencesFragment newInstance() {
        PreferencesFragment fragment = new PreferencesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(R.string.preferences);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_preferences, container, false);

        genderToTeachSpinner = (AppCompatSpinner) view.findViewById(R.id.fragmentSignUp_sex_to_teach);
        sessionLocationSpinner = (AppCompatSpinner) view.findViewById(R.id.fragmentSignUp_prefered_location);
        sessionTypeSpinner = (AppCompatSpinner) view.findViewById(R.id.fragmentSignUp_session_type);
        updateButton = (Button) view.findViewById(R.id.fragmentPreference_signUp_btn);

        genderToTeachError = (TextView) view.findViewById(R.id.fragmentSignUp_sex_to_teach_text);
        sessionLocationError = (TextView) view.findViewById(R.id.fragmentSignUp_prefered_location_text);
        sessionTypeError = (TextView) view.findViewById(R.id.fragmentSignUp_session_type_text);

        loadingDialog = (RelativeLayout) view.findViewById(R.id.dialog_layout);

        final String[] genders = getContext().getResources().getStringArray(R.array.gender_teach_array);
        ArrayAdapter<String> arrayAdapterGenders=new ArrayAdapter<String>(getContext(),R.layout.layout_item_gender_spinner,genders);
        genderToTeachSpinner.setAdapter(arrayAdapterGenders);

        if(Mentor.getInstance().sex_to_teach.equalsIgnoreCase(getContext().getResources().getString(R.string.male))){
            genderToTeachSpinner.setSelection(1);
        }
        else if(Mentor.getInstance().sex_to_teach.equalsIgnoreCase(getContext().getResources().getString(R.string.female))){
            genderToTeachSpinner.setSelection(2);
        }
        else {
            genderToTeachSpinner.setSelection(3);
        }

        genderToTeachSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position != 0) {
                    sexToTeach = genders[position];
                    editProfile.setSexToTeach(genders[position]);
                }else {
                    sexToTeach = null;
                    editProfile.setSexToTeach(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                sexToTeach = null;
                editProfile.setSexToTeach(null);

            }
        });

        final String[] preferredLoactionArray = getContext().getResources().getStringArray(R.array.prefered_session_array);
        ArrayAdapter<String> arrayAdapterpreferredLocation = new ArrayAdapter<String>(getContext(),R.layout.layout_item_gender_spinner,preferredLoactionArray);
        sessionLocationSpinner.setAdapter(arrayAdapterpreferredLocation);

        if(Mentor.getInstance().prefered_session_location.equalsIgnoreCase("0")){
            sessionLocationSpinner.setSelection(1);
        }
        else if(Mentor.getInstance().prefered_session_location.equalsIgnoreCase("1")){
            sessionLocationSpinner.setSelection(2);
        }
        else if(Mentor.getInstance().prefered_session_location.equalsIgnoreCase("2")){
            sessionLocationSpinner.setSelection(3);
        }

        sessionLocationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position != 0) {
                    preferredLocation = ""+position;
                    editProfile.setPreferedSessionLocation((position-1));
                }else {
                    preferredLocation = null;
                    editProfile.setPreferedSessionLocation(-1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                preferredLocation = null;
                editProfile.setPreferedSessionLocation(-1);
            }
        });

        final String[] sessionTypeArray = getContext().getResources().getStringArray(R.array.session_type_array);
        ArrayAdapter<String> arraySessionType = new ArrayAdapter<String>(getContext(),R.layout.layout_item_gender_spinner,sessionTypeArray);
        sessionTypeSpinner.setAdapter(arraySessionType);

        if(Mentor.getInstance().session_type.equalsIgnoreCase("0")){
            sessionTypeSpinner.setSelection(1);
        }
        else if(Mentor.getInstance().session_type.equalsIgnoreCase("1")){
            sessionTypeSpinner.setSelection(2);
        }
        else if(Mentor.getInstance().session_type.equalsIgnoreCase("2")){
            sessionTypeSpinner.setSelection(3);
        }

        sessionTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position != 0) {
                    sessionType = ""+position;
                    editProfile.setSessionType((position-1));
                }else {
                    sessionType = null;
                    editProfile.setSessionType(-1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                sessionType = null;
                editProfile.setSessionType(-1);
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sexToTeach == null){
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.please_select_gender_to_teach), Toast.LENGTH_SHORT).show();
                }
                else if(preferredLocation == null){
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.please_select_session_location), Toast.LENGTH_SHORT).show();
                }
                else if(sessionType == null){
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.please_select_session_type), Toast.LENGTH_SHORT).show();
                }
                else {
                    updateProfile();
                }
            }
        });

        return view;
    }

    private void updateProfile(){
        loadingDialog.setVisibility(View.VISIBLE);
        editProfile.edit(getContext() ,new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                if (response.getResult()) {
                    loadingDialog.setVisibility(View.GONE);
                    Mentor.getInstance().sex_to_teach = editProfile.getSexToTeach();
                    Mentor.getInstance().prefered_session_location = ""+editProfile.getPreferedSessionLocation();
                    Mentor.getInstance().session_type = ""+editProfile.getSessionType();
                    Mentor.getInstance().setDataToSharedPref(getContext());
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.update_successful), Toast.LENGTH_SHORT).show();
                }
                else {
                    loadingDialog.setVisibility(View.GONE);
                    handleResponseErrorCodeForUser(response.getContent().getMessage());
                    handleResponseErrorCodeForDebug(response.getContent().getMessage());
                }
            }
        }, this);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e("TAG" , "handleResponseErrorCodeForDebug");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
