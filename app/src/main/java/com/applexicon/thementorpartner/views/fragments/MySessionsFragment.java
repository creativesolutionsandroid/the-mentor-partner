package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.views.adapters.MySessionsFragmentAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MySessionsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MySessionsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MySessionsFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    String[] mTabsTitle;

    public MySessionsFragment() {
        // Required empty public constructor
    }

    public static MySessionsFragment newInstance() {
        MySessionsFragment fragment = new MySessionsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(R.string.my_sessions);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_my_sessions, container, false);

        mTabsTitle = new String[]{ getResources().getString(R.string.confirmed) , getResources().getString(R.string.completed) ,  getResources().getString(R.string.canceled)};

        tabLayout = (TabLayout) view.findViewById(R.id.fragmentMySessions_tabLayout);
        viewPager = (ViewPager) view.findViewById(R.id.fragmentMySessions_viewpager);

        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(new MySessionsFragmentAdapter(getChildFragmentManager(),mTabsTitle));
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
