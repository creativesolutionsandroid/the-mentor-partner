package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.CalendarFragmentVM;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.databinding.FragmentMyCalendarBinding;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyCalendarFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyCalendarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyCalendarFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    public MyCalendarFragment() {
        // Required empty public constructor
    }

    public static MyCalendarFragment newInstance() {
        MyCalendarFragment fragment = new MyCalendarFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(R.string.my_calendar);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_my_calendar, container, false);

        FragmentMyCalendarBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_calendar, container, false);
        View view = binding.getRoot();
        CalendarFragmentVM vewModel =  new CalendarFragmentVM(this , binding);
        binding.setCalendarFragmentVM(vewModel);

//        signInFragmentViewModel.addObserver(this);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
