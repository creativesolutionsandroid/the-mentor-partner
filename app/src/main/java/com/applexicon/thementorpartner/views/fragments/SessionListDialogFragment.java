package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.SessionListDialogFragmentVM;
import com.applexicon.thementorpartner.views.adapters.DaySessionsAdapter;
import com.applexicon.thementorpartner.databinding.FragmentSessionListDialogBinding;

import java.util.Observable;
import java.util.Observer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SessionListDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SessionListDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SessionListDialogFragment extends DialogFragment implements Observer {


    private static final String ARG_DATE = "arg_date";
    private OnFragmentInteractionListener mListener;
    private  FragmentSessionListDialogBinding binding;
    public String sessionDate;
public SessionListDialogFragmentVM vewModel;
    public SessionListDialogFragment() {
        // Required empty public constructor
    }

    public static SessionListDialogFragment newInstance(String date) {
        SessionListDialogFragment fragment = new SessionListDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString(ARG_DATE , date);


        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            sessionDate =  getArguments().getString(ARG_DATE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_session_added_dialog, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
         binding = DataBindingUtil.inflate(inflater, R.layout.fragment_session_list_dialog, container, false);
        View view = binding.getRoot();
        vewModel =  new SessionListDialogFragmentVM(this, binding);
        binding.setSessionListDialogFragmentVM(vewModel);

        vewModel.addObserver(this);

        setupRecyclarView(binding.fragmentSessionListDialogRecyclarViewRv);


        return view;
    }

    private void setupRecyclarView(RecyclerView listSessions) {
        DaySessionsAdapter adapter = new DaySessionsAdapter(this);
        listSessions.setAdapter(adapter);
        listSessions.setLayoutManager(new LinearLayoutManager(getActivity()));
    }
    @Override
    public void update(Observable observable, Object o) {

        if (observable instanceof SessionListDialogFragmentVM) {
            DaySessionsAdapter adapter = (DaySessionsAdapter) binding.fragmentSessionListDialogRecyclarViewRv.getAdapter();
            SessionListDialogFragmentVM viewModel = (SessionListDialogFragmentVM) observable;
            adapter.setDataList(viewModel.getDataList());
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
