/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.views.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.ItemSessionListDialogVM;
import com.applexicon.thementorpartner.views.fragments.SessionListDialogFragment;
import com.applexicon.thementorpartner.databinding.ItemSessionListDialogLayoutBinding;

import java.util.Collections;
import java.util.List;

public class DaySessionsAdapter extends RecyclerView.Adapter<DaySessionsAdapter.MyViewHolder> {

  public final SessionListDialogFragment fragment;
  private List<Session> dataList;

  public DaySessionsAdapter(SessionListDialogFragment fragment) {
    this.fragment = fragment;
    this.dataList = Collections.emptyList();
  }

  @Override public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ItemSessionListDialogLayoutBinding itemBinding =
        DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_session_list_dialog_layout,
            parent, false);
    return new MyViewHolder(itemBinding);
  }

  @Override public void onBindViewHolder(MyViewHolder holder, int position) {
    holder.bindPeople(dataList.get(position) , fragment);
  }

  @Override public int getItemCount() {
    return dataList.size();
  }

  public void setDataList(List<Session> dataList) {
    this.dataList = dataList;
    notifyDataSetChanged();
  }

  public static class MyViewHolder extends RecyclerView.ViewHolder {
    ItemSessionListDialogLayoutBinding itemBinding;

    public MyViewHolder(ItemSessionListDialogLayoutBinding itemBinding) {
      super(itemBinding.itemSessionLayoutItemSessionRl);
      this.itemBinding = itemBinding;
    }

    void bindPeople(final Session item, final SessionListDialogFragment fragment) {

      itemBinding.itemSessionLayoutItemSessionRl.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          fragment.vewModel.onItemClicked(item);
        }
      });

      if (itemBinding.getItemSessionListDialogVM() == null) {
        itemBinding.setItemSessionListDialogVM(
            new ItemSessionListDialogVM(item, fragment));
      } else {
        itemBinding.getItemSessionListDialogVM().setItem(item);
      }
    }
  }
}
