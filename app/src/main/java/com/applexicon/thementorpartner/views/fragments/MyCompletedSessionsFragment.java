package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.viewModel.SessionCompletedFragmentVM;
import com.applexicon.thementorpartner.views.adapters.SessionCompletedAdapter;
import com.applexicon.thementorpartner.databinding.FragmentMySessionsCompletedBinding;

import java.util.Observable;
import java.util.Observer;


public class MyCompletedSessionsFragment extends Fragment implements Observer {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private SessionCompletedFragmentVM vewModel;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    public FragmentMySessionsCompletedBinding binding;
    private View view;

    public MyCompletedSessionsFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MyCompletedSessionsFragment newInstance(/*String param1, String param2*/) {
        MyCompletedSessionsFragment fragment = new MyCompletedSessionsFragment();
       /* Bundle args = new Bundle();
      *//*  args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);*//*
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

         view = inflater.inflate(R.layout.fragment_my_sessions_completed, container, false);

        binding = DataBindingUtil.bind(view);

        vewModel = new SessionCompletedFragmentVM(this);

        binding.setSessionsCompletedFragmentVM(vewModel);

        vewModel.addObserver(this);

        setupRecyclerView(binding.fragmentMyBookingsCompletedRecycler);

        return  view;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            vewModel.fetchSessionsCompleted();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (view != null) {
            ViewGroup parentViewGroup = (ViewGroup) view.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    private void setupRecyclerView(RecyclerView listSessions) {
        SessionCompletedAdapter adapter = new SessionCompletedAdapter(this);
        listSessions.setAdapter(adapter);
        listSessions.setLayoutManager(new LinearLayoutManager(getActivity()));
        listSessions.addItemDecoration(new MyItemDecoration(getContext()));
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof SessionCompletedFragmentVM) {
            SessionCompletedAdapter adapter = (SessionCompletedAdapter) binding.fragmentMyBookingsCompletedRecycler.getAdapter();
            SessionCompletedFragmentVM viewModel = (SessionCompletedFragmentVM) observable;
            adapter.setDataList(viewModel.getSessionsleList());
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class MyItemDecoration extends RecyclerView.ItemDecoration {

        private final int decorationHeight;
        private Context context;

        public MyItemDecoration(Context context) {
            this.context = context;
            decorationHeight = 15;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            if (parent != null && view != null) {

                int itemPosition = parent.getChildAdapterPosition(view);
                int totalCount = parent.getAdapter().getItemCount();

                if (itemPosition >= 0 && itemPosition < totalCount - 1) {
                    outRect.bottom = decorationHeight;
                }

            }

        }
    }
}
