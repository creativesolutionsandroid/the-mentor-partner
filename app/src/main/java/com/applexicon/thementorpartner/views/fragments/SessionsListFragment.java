package com.applexicon.thementorpartner.views.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.databinding.FragmentRequestDetailsDialogBinding;
import com.applexicon.thementorpartner.models.ConfirmedSession;
import com.applexicon.thementorpartner.models.ConfirmedSessionFiltered;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.views.adapters.SessionsListAdapter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

public class SessionsListFragment extends Fragment implements Response.ResponseErrorCodeHandler{

    private static final String BOOK_SESSION_OBJECT = "BOOK_SESSION_OBJECT";
    private OnFragmentInteractionListener mListener;
    public ConfirmedSessionFiltered requestModel;
    public final MainActivity mainActivity;
    CountDownTimer countDownTimer2;
    RecyclerView mRecyclerView;
    SessionsListAdapter mAdapater;

    public FragmentRequestDetailsDialogBinding binding;

    public SessionsListFragment() {
        // Required empty public constructor
        mainActivity = (MainActivity) getActivity();
    }

    public static SessionsListFragment newInstance(Context context, ConfirmedSessionFiltered requestModel) {
        FragmentTransaction transactionFragment;
        transactionFragment = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
        SessionsListFragment fragment = new SessionsListFragment();
        transactionFragment.replace(R.id.main_container, fragment).addToBackStack("session_list").commit();
        Bundle args = new Bundle();
        args.putSerializable(BOOK_SESSION_OBJECT, requestModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            requestModel = (ConfirmedSessionFiltered) getArguments().getSerializable(BOOK_SESSION_OBJECT);
        }

        MainActivity mainActivity = (MainActivity) getActivity();
//        mainActivity.setBackButton();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.sessions_list_fragment, null, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.sessions_list);

        fetchSessionsConfirmed();

        String[] dates = requestModel.date.split(",");
        String[] timeFrom = requestModel.timeFrom.split(",");
        String[] timeTo = requestModel.timeTo.split(",");
        String[] id = requestModel.session_name.split(",");

        SessionsListAdapter adapter = new SessionsListAdapter(getContext(), dates, timeFrom, timeTo, id, requestModel);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new MyItemDecoration(getContext()));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void enableView(View view, boolean enable) {

        if (enable) {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        view.setAlpha(enable ? 1f : 0.5f);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e("TAG" , "handleResponseErrorCodeForDebug");
    }

    public class MyItemDecoration extends RecyclerView.ItemDecoration {

        private final int decorationHeight;
        private Context context;

        public MyItemDecoration(Context context) {
            this.context = context;
            decorationHeight = 15;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            if (parent != null && view != null) {

                int itemPosition = parent.getChildAdapterPosition(view);
                int totalCount = parent.getAdapter().getItemCount();

                if (itemPosition >= 0 && itemPosition < totalCount - 1) {
                    outRect.bottom = decorationHeight;
                }
            }
        }
    }

    public void fetchSessionsConfirmed() {

        Mentor.getInstance().fetchSessionsConfirmed(getContext() , new Consumer<Response<Mentor.SessionsConfirmedContentData>>(){

            @Override
            public void accept(Response<Mentor.SessionsConfirmedContentData> response) throws Exception {

                Log.e("TAG" , "fetchRequestList response success " );
                if(response.getResult()){
                    if (response.getContent().getData().confirmedBookingList.size() > 0) {
                        List<ConfirmedSession> confirmedBookingsList = new ArrayList<>();
                        List<ConfirmedSessionFiltered> filteredconfirmedBookings = new ArrayList<>();
                        confirmedBookingsList = response.getContent().getData().confirmedBookingList;

                        for (int i = 0; i < confirmedBookingsList.size(); i++){
                            boolean isGroupIdExists = false;
                            for (int j = 0; j < filteredconfirmedBookings.size(); j++) {
                                if (confirmedBookingsList.get(i).groupId.equals(filteredconfirmedBookings.get(j).groupId)) {
                                    isGroupIdExists = true;
                                    filteredconfirmedBookings.get(j).date = filteredconfirmedBookings.get(j).date +
                                            "," + confirmedBookingsList.get(i).date;
                                    filteredconfirmedBookings.get(j).timeFrom = filteredconfirmedBookings.get(j).timeFrom +
                                            "," + confirmedBookingsList.get(i).timeFrom;
                                    filteredconfirmedBookings.get(j).timeTo = filteredconfirmedBookings.get(j).timeTo +
                                            "," + confirmedBookingsList.get(i).timeTo;
                                    filteredconfirmedBookings.get(j).id = filteredconfirmedBookings.get(j).id +
                                            "," + confirmedBookingsList.get(i).id;
                                    filteredconfirmedBookings.get(j).session_name = filteredconfirmedBookings.get(j).session_name +
                                            "," + confirmedBookingsList.get(i).session_name;
                                }
                            }

                            boolean isBookingIdExists = false;
                            String bookingId = "";
                            for (int j = 0; j < filteredconfirmedBookings.size(); j++) {
                                if (confirmedBookingsList.get(i).groupId.equals(filteredconfirmedBookings.get(j).groupId)) {
                                    bookingId = filteredconfirmedBookings.get(j).bookingId;
                                    isBookingIdExists = true;
                                }
                            }

                            if(!isGroupIdExists) {
                                ConfirmedSessionFiltered confirmedBookingFiltered = new ConfirmedSessionFiltered();
                                if(!isBookingIdExists) {
                                    confirmedBookingFiltered.bookingId = confirmedBookingsList.get(i).id;
                                }
                                else {
                                    confirmedBookingFiltered.bookingId = bookingId;
                                }
                                confirmedBookingFiltered.id = confirmedBookingsList.get(i).id;
                                confirmedBookingFiltered.subjectName = confirmedBookingsList.get(i).subjectName;
                                confirmedBookingFiltered.groupId = confirmedBookingsList.get(i).groupId;
                                confirmedBookingFiltered.studentName = confirmedBookingsList.get(i).studentName;
                                confirmedBookingFiltered.studentGender = confirmedBookingsList.get(i).studentGender;
                                confirmedBookingFiltered.location = confirmedBookingsList.get(i).location;
                                confirmedBookingFiltered.date = confirmedBookingsList.get(i).date;
                                confirmedBookingFiltered.location = confirmedBookingsList.get(i).location;
                                confirmedBookingFiltered.date = confirmedBookingsList.get(i).date;
                                confirmedBookingFiltered.amount = confirmedBookingsList.get(i).amount;
                                confirmedBookingFiltered.currency = confirmedBookingsList.get(i).currency;
                                confirmedBookingFiltered.userName = confirmedBookingsList.get(i).userName;
                                confirmedBookingFiltered.paidAmount = confirmedBookingsList.get(i).paidAmount;
                                confirmedBookingFiltered.grade_name = confirmedBookingsList.get(i).grade_name;
                                confirmedBookingFiltered.level_name = confirmedBookingsList.get(i).level_name;
                                confirmedBookingFiltered.distance_in_km = confirmedBookingsList.get(i).distance_in_km;
                                confirmedBookingFiltered.availabeForStart = confirmedBookingsList.get(i).availabeForStart;
                                confirmedBookingFiltered.timeFrom = confirmedBookingsList.get(i).timeFrom;
                                confirmedBookingFiltered.timeTo = confirmedBookingsList.get(i).timeTo;
                                confirmedBookingFiltered.sessionLongitude = confirmedBookingsList.get(i).sessionLongitude;
                                confirmedBookingFiltered.sessionLatitude = confirmedBookingsList.get(i).sessionLatitude;
                                confirmedBookingFiltered.session_type = confirmedBookingsList.get(i).session_type;
                                confirmedBookingFiltered.groupId = confirmedBookingsList.get(i).groupId;
                                confirmedBookingFiltered.grade_name_in_ar = confirmedBookingsList.get(i).grade_name_in_ar;
                                confirmedBookingFiltered.level_name_in_ar = confirmedBookingsList.get(i).level_name_in_ar;
                                confirmedBookingFiltered.course_name_in_ar = confirmedBookingsList.get(i).course_name_in_ar;
                                confirmedBookingFiltered.session_location = confirmedBookingsList.get(i).session_location;
                                confirmedBookingFiltered.session_from_time = confirmedBookingsList.get(i).session_from_time;
                                confirmedBookingFiltered.session_to_time = confirmedBookingsList.get(i).session_to_time;
                                confirmedBookingFiltered.teacherLatitude = confirmedBookingsList.get(i).teacherLatitude;
                                confirmedBookingFiltered.teacherLongitude = confirmedBookingsList.get(i).teacherLongitude;
                                confirmedBookingFiltered.studentPhone = confirmedBookingsList.get(i).studentPhone;
                                confirmedBookingFiltered.booking_name = confirmedBookingsList.get(i).booking_name;
                                confirmedBookingFiltered.session_name = confirmedBookingsList.get(i).session_name;
                                filteredconfirmedBookings.add(confirmedBookingFiltered);
                            }
                        }

                        for (int i = 0; i < filteredconfirmedBookings.size(); i++){
                            if(filteredconfirmedBookings.get(i).id.equals(requestModel.id)) {
                                requestModel = filteredconfirmedBookings.get(i);
                            }
                        }

                        String[] dates = requestModel.date.split(",");
                        String[] timeFrom = requestModel.timeFrom.split(",");
                        String[] timeTo = requestModel.timeTo.split(",");
                        String[] id = requestModel.session_name.split(",");

                        SessionsListAdapter adapter = new SessionsListAdapter(getContext(), dates, timeFrom, timeTo, id, requestModel);
                        mRecyclerView.setAdapter(adapter);
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        mRecyclerView.addItemDecoration(new MyItemDecoration(getContext()));
                        adapter.notifyDataSetChanged();
                    }

                }else{

                    response.handleResponseErrorCode(getContext() , SessionsListFragment.this);
                }

            }
        } ,this );
    }
}
