package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class SessionsTransactions {

    @SerializedName("session_final_price")
    public String session_final_price;

    @SerializedName("id")
    public String id;

    @SerializedName("status")
    public int status;

    @SerializedName("payment_type")
    public int payment_type;

    @SerializedName("session_from_time")
    public String session_from_time;

    @SerializedName("session_to_time")
    public String session_to_time;

    @SerializedName("mentor_fee")
    public String mentor_fee;


    public void startSession(final Context context,
                             Consumer<Response> consumer,
                             final Response.ResponseErrorCodeHandler responseErrorCodeHandler,
                             double latitude, double longitude) {

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.start(id, latitude, longitude);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e("TAG" , "end : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void end(final Context context,
                    Consumer<Response<fetchEndSessionData>> consumer,
                    final Response.ResponseErrorCodeHandler responseErrorCodeHandler,
                    double latitude, double longitude) {

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<fetchEndSessionData>> response = apiInterface.end(id, latitude, longitude);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e("TAG" , "end : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void cancel(final Context context,
                       Consumer<Response> consumer,
                       final Response.ResponseErrorCodeHandler responseErrorCodeHandler,
                       String groupID, String reason) {

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.cancel(id, groupID, reason);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e("TAG" , "end : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void paymnetReceived(final Context context,
                       Consumer<Response> consumer,
                       final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.paymnetReceived(id);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e("TAG" , "end : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }


    public void paymnetRejected(final Context context,
                                Consumer<Response> consumer,
                                final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.paymnetRejected(id);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e("TAG" , "end : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void rateStudent(final Context context,
                                Consumer<Response> consumer,
                                final Response.ResponseErrorCodeHandler responseErrorCodeHandler,
                                float rating, String review) {

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.addReview(id, rating, review);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e("TAG" , "end : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void getSessionSummary(final Context context,
                                Consumer<Response<sessionSummaryData>> consumer,
                                final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<sessionSummaryData>> response = apiInterface.sessionSummary(id);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e("TAG" , "end : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public interface APIInterface {

        @FormUrlEncoded
        @POST(Constants.END_URL_POST)
        Observable<Response<fetchEndSessionData>> end(
                @Field("session_id") String sessionId,
                @Field("session_end_latitude") double latitude,
                @Field("session_end_longitude") double longitude
        );

        @FormUrlEncoded
        @POST(Constants.START_URL_POST)
        Observable<Response> start(
                @Field("session_id") String sessionId,
                @Field("session_start_latitude") double latitude,
                @Field("session_start_longitude") double longitude
        );

        @FormUrlEncoded
        @POST(Constants.CANCEL_URL_POST)
        Observable<Response> cancel(
                @Field("session_id") String sessionId,
                @Field("group_id") String latitude,
                @Field("reason") String longitude
        );

        @FormUrlEncoded
        @POST(Constants.PAYMNET_RECEIVED)
        Observable<Response> paymnetReceived(
                @Field("session_id") String sessionId
        );

        @FormUrlEncoded
        @POST(Constants.PAYMNET_REJECTED)
        Observable<Response> paymnetRejected(
                @Field("session_id") String sessionId
        );

        @GET(Constants.SESSION_SUMMARY)
        Observable<Response<sessionSummaryData>> sessionSummary(
                @Query("session_id") String sessionId
        );

        @FormUrlEncoded
        @POST(Constants.ADD_REVIEW)
        Observable<Response> addReview(
                @Field("session_id") String sessionId,
                @Field("rate") float rate,
                @Field("comment") String review
        );
    }

    public class fetchEndSessionData implements Serializable{
        @SerializedName("session")
        public SessionsTransactions futureSessions;
    }

    public class sessionSummaryData implements Serializable{
        @SerializedName("sessions")
        public SessionsTransactions sessionData;
    }
}
