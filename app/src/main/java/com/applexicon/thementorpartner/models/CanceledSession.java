package com.applexicon.thementorpartner.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hamdy on 8/13/2017.
 */

public class CanceledSession implements Serializable {


    //    {"id":1,"address":"52327 Hahn Squares\nRicechester, AZ 82309-4998","session_date":"2017-08-15","teacher_id":207,"teacher_name":"shokry1","teacher_rate":2}

    @SerializedName("id")
    public String id;

    @SerializedName("course_name")
    public String subjectName;

    @SerializedName("address")
    public String location;

    @SerializedName("session_date")
    public String date;

    @SerializedName("studentName")
    public String studentName;

    @SerializedName("grade_name")
    public String grade_name;

    @SerializedName("level_name")
    public String level_name;

    @SerializedName("session_location")
    public String session_location;

    @SerializedName("session_type")
    public String session_type;

    @SerializedName("time_from")
    public String time_from;

    @SerializedName("time_to")
    public String time_to;

    @SerializedName("session_name")
    public String session_name;

    @SerializedName("cancel_by")
    public String cancel_by;

    @SerializedName("reason")
    public String reason;

    @SerializedName("cancel_amount")
    public String cancel_amount;
}
