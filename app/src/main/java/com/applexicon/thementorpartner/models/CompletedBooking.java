package com.applexicon.thementorpartner.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hamdy on 8/13/2017.
 */

public class CompletedBooking implements Serializable{

//    {"id":1,"address":"52327 Hahn Squares\nRicechester, AZ 82309-4998","session_date":"2017-08-15","teacher_id":207,"teacher_name":"shokry1","teacher_rate":2}
    @SerializedName("id")
    public String id;

    @SerializedName("booking_id")
    public String booking_id;

    @SerializedName("course_name")
    public String subjectName;

    @SerializedName("subjectNameInUrdu")
    public String subjectNameInUrdu;

    @SerializedName("studentName")
    public String studentName;

    @SerializedName("address")
    public String location;

    @SerializedName("session_date")
    public String date;

    @SerializedName("session_price")
    public String amount;

    @SerializedName("grade_name")
    public String grade_name;

    @SerializedName("grade_name_in_ar")
    public String grade_name_in_ar;

    @SerializedName("level_name")
    public String level_name;

    @SerializedName("level_name_in_ar")
    public String level_name_in_ar;

    @SerializedName("session_location")
    public String session_location;

    @SerializedName("session_type")
    public String session_type;

    @SerializedName("currency")
    public String currency;

    @SerializedName("time_from")
    public String time_from;

    @SerializedName("time_to")
    public String time_to;

    @SerializedName("session_name")
    public String session_name;

    @SerializedName("teacher_rate")
    public float teacher_rate;
}
