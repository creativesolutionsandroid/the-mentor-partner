package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


/**
 * Created by Hamdy on 7/24/2017.
 */

public class ChangePassword {

    String TAG = "ChangePassword";

    private String oldPassword = "";
    private String newPassword = "";

    public ChangePassword() {
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }


    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void send(final Context context ,final Consumer<Response> consumer ,  final  Response.ResponseErrorCodeHandler responseErrorCodeHandler){

        Log.e(TAG , "send ");
        ChangePasswordAPIInterface apiInterface =
                ApiClient.getClient(context).create(ChangePasswordAPIInterface.class);

        Observable<Response> response = apiInterface.send(Mentor.getInstance().token , oldPassword , newPassword);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer , new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "editProfile : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context, responseErrorCodeHandler);
                    }
                });
    }


    public interface ChangePasswordAPIInterface {


        @FormUrlEncoded
        @POST(Constants.CHANGE_PASSWORD_POST_URL)
        Observable<Response> send(
                @Field("token") String token,
                @Field("old_password") String oldPassword,
                @Field("new_password") String newPassword
        );
    }





}
