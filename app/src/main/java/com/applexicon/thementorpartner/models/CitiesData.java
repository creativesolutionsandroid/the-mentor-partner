package com.applexicon.thementorpartner.models;

import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public class CitiesData {

    @Expose
    @SerializedName("errorCode")
    private int errorcode;
    @Expose
    @SerializedName("content")
    private Content content;
    @Expose
    @SerializedName("result")
    private boolean result;

    public int getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(int errorcode) {
        this.errorcode = errorcode;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public static class Content {
        @Expose
        @SerializedName("newToken")
        private int newtoken;
        @Expose
        @SerializedName("data")
        private Data data;

        public int getNewtoken() {
            return newtoken;
        }

        public void setNewtoken(int newtoken) {
            this.newtoken = newtoken;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }
    }

    public static class Data {
        @Expose
        @SerializedName("cities")
        private List<Cities> cities;

        public List<Cities> getCities() {
            return cities;
        }

        public void setCities(List<Cities> cities) {
            this.cities = cities;
        }
    }

    public class Cities {
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("sequence_order")
        private int sequenceOrder;
        @Expose
        @SerializedName("record_status")
        private String recordStatus;
        @Expose
        @SerializedName("updated_at")
        private String updatedAt;
        @Expose
        @SerializedName("created_at")
        private String createdAt;
        @Expose
        @SerializedName("name_in_urdu")
        private String nameInUrdu;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public int getSequenceOrder() {
            return sequenceOrder;
        }

        public void setSequenceOrder(int sequenceOrder) {
            this.sequenceOrder = sequenceOrder;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getNameInUrdu() {
            return nameInUrdu;
        }

        public void setNameInUrdu(String nameInUrdu) {
            this.nameInUrdu = nameInUrdu;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public interface CitiesAPIInterface {
        @GET(Constants.GET_CITIES)
        Call<CitiesData> fetchCities();
    }

    public class GetCitiesDataResponse {
        @SerializedName("cities")
        public List<Cities> cities;
    }
}
