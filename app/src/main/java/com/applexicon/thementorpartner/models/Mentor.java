package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.applexicon.thementorpartner.utilities.SharedPref;
import com.applexicon.thementorpartner.viewModel.SignInFragmentViewModel;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Hamdy on 8/1/2017.
 */

public class Mentor {

    private String TAG = "Mentor";

    private static Mentor mMentor;
    //"id":11,"phone":"+966123456789","name":"mohamed","email":"mohamed@gmail.com","years_of_experience":56,"rate":0,"gender":"0","phone_is_verified":1
    @SerializedName("id")
    public String id;

    @SerializedName("phone")
    public String phone;

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("address")
    public String address;

    @SerializedName("profile_pic")
    public String imageUrl;

    @SerializedName("sex_to_teach")
    public String sex_to_teach;

    @SerializedName("session_type")
    public String session_type;

    @SerializedName("prefered_session_location")
    public String prefered_session_location;

    public String token = "";

    private SessionRequestModel sessionRequest;
    public int status;

    private String USER_PREF_ID = "user_pref_id";
    private String USER_PREF_NAME = "user_pref_name";
    private String USER_PREF_PHONE = "user_pref_phone";
    private String USER_PREF_EMAIL = "user_pref_email";
    private String USER_PREF_ADDRESS = "user_pref_address";
    public static String USER_PREF_IMAGE_URL = "user_pref_image_url";
    private String USER_PREF_TOKEN = "user_pref_token";
    private String USER_PREF_GENDER = "user_pref_gender";
    private String USER_PREF_LOCATION = "user_pref_location";
    private String USER_PREF_SESSION = "user_pref_session";

    public Mentor() {
    }

    public boolean isLoggedIn(Context context) {
        return SharedPref.getBoolean(context , SharedPref.PREF_AUTH);
    }

    public void logout(Context context) {

    }

    public void login(Context context) {
        SharedPref.putBoolean(context ,  SharedPref.PREF_AUTH , true);
        SharedPref.putString(context , USER_PREF_TOKEN, token);
        SharedPref.putString(context , USER_PREF_ID , id);
        setDataToSharedPref(context);
    }

    public void setDataToSharedPref(Context context){
        SharedPref.putString(context , USER_PREF_NAME , name);
        SharedPref.putString(context , USER_PREF_PHONE , phone);
        SharedPref.putString(context , USER_PREF_EMAIL, email);
        SharedPref.putString(context , USER_PREF_ADDRESS, address);
        SharedPref.putString(context , USER_PREF_GENDER, sex_to_teach);
        SharedPref.putString(context , USER_PREF_LOCATION, prefered_session_location);
        SharedPref.putString(context , USER_PREF_SESSION, session_type);
    }

    public void fillUserData(Context context){
        id = SharedPref.getString(context , USER_PREF_ID );
        name = SharedPref.getString(context , USER_PREF_NAME);
        phone = SharedPref.getString(context , USER_PREF_PHONE);
        email =  SharedPref.getString(context , USER_PREF_EMAIL);
        address = SharedPref.getString(context , USER_PREF_ADDRESS);
        imageUrl =  SharedPref.getString(context , USER_PREF_IMAGE_URL);
        token =  SharedPref.getString(context , USER_PREF_TOKEN);
        sex_to_teach =  SharedPref.getString(context , USER_PREF_GENDER);
        prefered_session_location =  SharedPref.getString(context , USER_PREF_LOCATION);
        session_type =  SharedPref.getString(context , USER_PREF_SESSION);
    }


    public static Mentor getInstance() {
        if(mMentor == null){
            mMentor = new Mentor();
        }

        return  mMentor;
    }

    public static void setmMentor(Mentor mMentor) {
        Mentor.mMentor = mMentor;
    }


    public SessionRequestModel getSessionRequest() {
        return sessionRequest;
    }

    public void setSessionRequest(SessionRequestModel sessionRequest) {
        this.sessionRequest = sessionRequest;
    }

    public void acceptSession(final Context context, Consumer<Response> consumer, final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        this.sessionRequest.accept(context , consumer , responseErrorCodeHandler);
    }

    public void rejectSession( final Context context,
                               Consumer<Response> consumer,
                               final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        this.sessionRequest.reject(context , consumer , responseErrorCodeHandler);
    }


    public void fetchNotificationStatus(final Context context,
                                        Consumer<Response<FetchStatusContentData>> consumer,
                                        final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchNotificationStatus ");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<FetchStatusContentData>> response = APIInterface.fetchNotificationStatus(token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "fetchNotificationStatus : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }

    public void changeNotificationStatus(final Context context,
                                         Consumer<Response> consumer,
                                         final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "changeNotificationStatus ");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = APIInterface.changeNotificationStatus(token , status);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "changeNotificationStatus : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }

    public void logout(final Context context,
                       Consumer<Response> consumer,
                       final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG, "logout ");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = APIInterface.logout(token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "logout : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context, responseErrorCodeHandler);
                    }
                });

    }



    public void fetchRequestList(final Context context,
                                 Consumer<Response<FetchRequestsContentData>> consumer,
                                 final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchRequestList "+token);
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<FetchRequestsContentData>> response = APIInterface.fetch(token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "fetchRequestList " + throwable.getMessage());

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }



    public void fetchAvailableStatus(final Context context,
                                     Consumer<Response<FetchStatusContentData>> consumer,
                                     final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchPaymentHistory ");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<FetchStatusContentData>> response = APIInterface.fetchAvailableStatus(token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "fetchAvailableStatus : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }

    public void changeAvailableStatus(final Context context,
                                      Consumer<Response> consumer,
                                      final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "changeAvailableStatus ");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = APIInterface.changeAvailableStatus(token , status);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "changeAvailableStatus : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }
    public void sendFirebaseToken(final Context context,String deviceToken,
                                  Consumer<Response> consumer,
                                  final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "sendFirebaseToken");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = APIInterface.sendFirebaseToken(deviceToken);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "sendFirebaseToken : " + throwable.getMessage());

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }


    public void fetchPaymentHistory(final Context context,
                                    Consumer<Response<FetchPaymentHisContentData>> consumer,
                                    final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchRequestList ");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<FetchPaymentHisContentData>> response = APIInterface.fetchPaymentHistory(token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "fetchPaymentHistory : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }


    public void fetchEventDates(final Context context,
                                Consumer<Response<FetchEventDatesContentData>> consumer,
                                final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchRequestList ");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<FetchEventDatesContentData>> response = APIInterface.fetchEventDates(token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "fetchPaymentHistory : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }


    public void fetchSessionsCanceled(final Context context,
                                      Consumer<Response<SessionsCanceledContentData>> consumer,
                                      final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG, "fetchSessionsCanceled ");

        APIInterface aPIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Log.e(TAG , "fetchSessionsCanceled token : " + token);

        Observable<Response<SessionsCanceledContentData>> response = aPIInterface.fetchBookingConfirmed(token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "fetchSessionsCanceled : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context, responseErrorCodeHandler);
                    }
                });

    }
    public void fetchSessionsCompleted(final Context context,
                                       Consumer<Response<SessionsCompletedContentData>> consumer,
                                       final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchSessionsCompleted ");

        APIInterface aPIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Log.e(TAG , "fetchSessionsCompleted token : " + token);
        Observable<Response<SessionsCompletedContentData>> response = aPIInterface.fetchBookingCompleted(token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "fetchSessionsCompleted : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }

    public void fetchSessionsConfirmed(final Context context,
                                       Consumer<Response<SessionsConfirmedContentData>> consumer,
                                       final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchSessionsConfirmed ");

        APIInterface aPIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Log.e(TAG , "fetchSessionsConfirmed token : " + token);
        Observable<Response<SessionsConfirmedContentData>> response = aPIInterface.fetchConfirmedSessions(token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "fetchSessionsConfirmed : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }

    public void changeLanguage(final Context context, String lang ,
                               Consumer<Response> consumer,
                               final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "changeLanguage ");

        APIInterface aPIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Log.e(TAG , "changeLanguage token : " + token);
        Observable<Response> response = aPIInterface.changeLanguage(lang);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "changeLanguage : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }


    public interface APIInterface {

        @GET(Constants.FETCH_SESSIONS)
        Observable<Response<FetchRequestsContentData>> fetch(
                @Query("token") String token
        );



        @GET(Constants.FETCH_PAYMENT_HISTORY)
        Observable<Response<FetchPaymentHisContentData>> fetchPaymentHistory(
                @Query("token") String token
        );


        @GET(Constants.FETCH_UPCOMING_EVENTS)
        Observable<Response<FetchEventDatesContentData>> fetchEventDates(
                @Query("token") String token
        );


        @GET(Constants.FETCH_AVAILABLE_URL_GET)
        Observable<Response<FetchStatusContentData>> fetchAvailableStatus(
                @Query("token") String token
        );


        @FormUrlEncoded
        @POST(Constants.CHANGE_AVAILABLE_URL_GET)
        Observable<Response> changeAvailableStatus(
                @Field("token") String token,
                @Field("status") int status
        );


        @GET(Constants.FETCH_NOTIFICATION_STATUS_URL_GET)
        Observable<Response<FetchStatusContentData>> fetchNotificationStatus(
                @Query("token") String token
        );


        @FormUrlEncoded
        @POST(Constants.CHANGE_NOTIFICATION_URL_GET)
        Observable<Response> changeNotificationStatus(
                @Field("token") String token,
                @Field("status") int status
        );

        @GET(Constants.FETCH_CONFIRMED_SESSIONS)
        Observable<Response<SessionsConfirmedContentData>> fetchConfirmedSessions(
                @Query("token") String token
        );

        @GET(Constants.FETCH_COMPLETED_SESSIONS)
        Observable<Response<SessionsCompletedContentData>> fetchBookingCompleted(
                @Query("token") String token
        );

        @GET(Constants.FETCH_CANCELED_SESSIONS)
        Observable<Response<SessionsCanceledContentData>> fetchBookingConfirmed(
                @Query("token") String token
        );

        @FormUrlEncoded
        @POST(Constants.SEND_FIREBASE_TOKEN)
        Observable<Response> sendFirebaseToken(
                @Field("device_token") String deviceToken
        );

        @FormUrlEncoded
        @POST(Constants.LOGOUT_POST_URL)
        Observable<Response> logout(
                @Field("token") String token
        );

        @FormUrlEncoded
        @POST(Constants.CHANGE_LANG)
        Observable<Response> changeLanguage(
                @Field("lang") String lang
        );
    }


    public class FetchRequestsContentData {

        @SerializedName("requests")
        public List<SessionRequestModel> sessionRequestsList = new ArrayList<>();
    }

    public class FetchEventDatesContentData {

        @SerializedName("upComingSessionDates")
        public List<Event> eventsList = new ArrayList<>();

        public class Event implements Serializable{

            @SerializedName("session_date")
            public String eventDate;
        }
    }

    public class FetchPaymentHisContentData {

        @SerializedName("paymentHistory")
        public List<PaymentHistory> paymentHistoryList;
    }

    public class FetchStatusContentData {

        @SerializedName("status")
        public int status;
    }

    public class SessionsCompletedContentData {

        @SerializedName("completedSessions")
        public List<CompletedBooking> completedBookingList;
    }

    public class SessionsConfirmedContentData {

        @SerializedName("confirmedSessions")
        public List<ConfirmedSession> confirmedBookingList;
    }

    public class SessionsCanceledContentData {

        @SerializedName("cancelledSessions")
        public List<CanceledSession> list;
    }

}
