package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.SerializedName;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


/**
 * Created by Hamdy on 7/24/2017.
 */

public class Verification {

    String TAG = "Verification";

    @SerializedName("code") private String code = "";
    @SerializedName("phone") private String phone = "";

    public Verification(String phone) {
        this.phone = phone;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void activate(final Context context , Consumer<Response> consumer , final  Response.ResponseErrorCodeHandler responseErrorCodeHandler){

        Log.e(TAG , "activate ");
        ActivateAPIInterface activationInterface =
                ApiClient.getClient(context).create(ActivateAPIInterface.class);

        Observable<Response> response = activationInterface.activate(code , phone);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer , new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "editProfile : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context, responseErrorCodeHandler);
                    }
                });
    }

    public void resend(final Context context ,  Consumer<Response> consumer , final  Response.ResponseErrorCodeHandler responseErrorCodeHandler){

        Log.e(TAG , "resend");
        ActivateAPIInterface activationInterface =
                ApiClient.getClient(context).create(ActivateAPIInterface.class);

        Observable<Response> response = activationInterface.resend(phone);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer ,  new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "editProfile : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context, responseErrorCodeHandler);
                    }
                });
    }


    public interface ActivateAPIInterface {
        /*
        Code:integer,
		phone:string

        * */

        @FormUrlEncoded
        @POST(Constants.VERIFY_PHONE_POST_URL)
        Observable<Response> activate(
//                @Field("api_key") String apiKey,
                @Field("code") String code,
                @Field("phone") String phone
        );

        @FormUrlEncoded
        @POST(Constants.RESEND)
        Observable<Response> resend(
                @Field("phone") String phone
        );
    }





}
