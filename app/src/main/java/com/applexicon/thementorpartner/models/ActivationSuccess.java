package com.applexicon.thementorpartner.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hamdy on 8/1/2017.
 */

public class ActivationSuccess implements Serializable {


    @Expose
    @SerializedName("errorCode")
    private int errorCode;
    @Expose
    @SerializedName("content")
    private Content content;
    @Expose
    @SerializedName("result")
    private boolean result;

    public static class Content {
        @Expose
        @SerializedName("newToken")
        private int newToken;
        @Expose
        @SerializedName("data")
        private Data data;
    }

    public static class Data {
        @Expose
        @SerializedName("responseData")
        private String responseData;
    }
}
