package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.SerializedName;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


/**
 * Created by Hamdy on 7/24/2017.
 */

public class EditProfile {

    String TAG = "EditProfile";

    private String fullname;
    private String email;
    private String address = "";
    private double latitude;
    private double longitude;
    private File imageProfileFile;
    private String sexToTeach;
    private int preferedSessionLocation;
    private int sessionType;


    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexToTeach() {
        return sexToTeach;
    }

    public void setSexToTeach(String sexToTeach) {
        this.sexToTeach = sexToTeach;
    }

    public int getPreferedSessionLocation() {
        return preferedSessionLocation;
    }

    public void setPreferedSessionLocation(int preferedSessionLocation) {
        this.preferedSessionLocation = preferedSessionLocation;
    }

    public int getSessionType() {
        return sessionType;
    }

    public void setSessionType(int sessionType) {
        this.sessionType = sessionType;
    }

    public void edit(final Context context ,
                     Consumer<Response> consumer ,
                     final  Response.ResponseErrorCodeHandler responseErrorCodeHandler){

            EditProfileAPIInterface   signInAPIInterface =
                    ApiClient.getClient(context).create(EditProfileAPIInterface.class);
//
//        RequestBody reqIdFile;
//        MultipartBody.Part profileImageFileBody = null;
//
//        if(imageProfileFile != null) {
//                 reqIdFile = RequestBody.create(MediaType.parse("image/*"), imageProfileFile);
//             profileImageFileBody = MultipartBody.Part.createFormData("profile_pic", imageProfileFile.getName(), reqIdFile);
//            Log.e(TAG, "edit : profileImage : " + imageProfileFile.toString());
//        }


        Mentor mentor =Mentor.getInstance();
        Log.e(TAG , "token " + Mentor.getInstance().token );
            RequestBody reqEmail = RequestBody.create(MediaType.parse("text/plain"), mentor.email);
            RequestBody reqFullname = RequestBody.create(MediaType.parse("text/plain"), mentor.name);
            RequestBody reqAddress = RequestBody.create(MediaType.parse("text/plain"), mentor.address);
//            RequestBody reqLat = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(latitude));
//            RequestBody reqLng = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longitude));
            RequestBody reqToken = RequestBody.create(MediaType.parse("text/plain"), Mentor.getInstance().token);
            RequestBody reqSexToTeach = RequestBody.create(MediaType.parse("text/plain"), sexToTeach);
            RequestBody reqpreferedSessionLocation = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(preferedSessionLocation));
            RequestBody reqsessionType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sessionType));



            Observable<Response> response =
                    signInAPIInterface.edit(
                            reqEmail,
                            reqFullname,
                            reqAddress,
                            reqToken,
                            reqSexToTeach,
                            reqpreferedSessionLocation,
                            reqsessionType
                    );

            response.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(consumer, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {

                            if(responseErrorCodeHandler == null)
                                return;

                            Log.e(TAG, "editProfile : " + throwable.getMessage());
                            Response response = new Response();

                            response.setThroeable(throwable);

                            response.handleResponseErrorCode(context, responseErrorCodeHandler);
                        }
                    });

    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setImageProfileFile(File imageProfileFile) {
        Log.e(TAG  , "setImageProfileFile");
        this.imageProfileFile = imageProfileFile;
    }

    public File getImageProfileFile() {
        return imageProfileFile;
    }

    public interface EditProfileAPIInterface {

        @Multipart
        @POST(Constants.EDIT_PROFILE_POST_URL)
        Observable<Response> edit(
                @Part("email") RequestBody email,
                @Part("name") RequestBody name,
                @Part("address") RequestBody address,
                @Part("token") RequestBody token,
                @Part("sex_to_teach") RequestBody sex_to_teach,
                @Part("prefered_session_location") RequestBody prefered_session_location,
                @Part("session_type") RequestBody session_type
        );
    }
}
