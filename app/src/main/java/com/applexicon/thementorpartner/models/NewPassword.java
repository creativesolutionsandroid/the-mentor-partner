package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


/**
 * Created by Hamdy on 7/24/2017.
 */

public class NewPassword {

    String TAG = "NewPassword";

    private String newPassword;
    private String phone;

    public NewPassword() {
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void send(final Context context , Consumer<Response> consumer , final  Response.ResponseErrorCodeHandler responseErrorCodeHandler){

        Log.e(TAG , "send ");
        NewPasswordAPIInterface apiInterface =
                ApiClient.getClient(context).create(NewPasswordAPIInterface.class);

        Observable<Response> response = apiInterface.send(newPassword , phone);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "editProfile : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context, responseErrorCodeHandler);
                    }
                });
    }


    public interface NewPasswordAPIInterface {


        @FormUrlEncoded
        @POST(Constants.RESET_FORGOT_PASSWORD_POST_URL)
        Observable<Response> send(
                @Field("password") String newPassword,
                @Field("phone") String phone
        );
    }





}
