package com.applexicon.thementorpartner.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hamdy on 8/13/2017.
 */

public class PaymentHistoryFiltered implements Serializable {

    @Expose
    @SerializedName("studentName")
    public String name;
    @Expose
    @SerializedName("amount")
    public double amount;
    @Expose
    @SerializedName("method_type")
    public int method_type;
    @Expose
    @SerializedName("currency")
    public String currency;
    @Expose
    @SerializedName("sessionMon")
    public String month;
    @Expose
    @SerializedName("sessionDay")
    public String day;
    @Expose
    @SerializedName("bookingName")
    public String bookingName;
    @Expose
    @SerializedName("sessionName")
    public String sessionName;
    @Expose
    @SerializedName("sessionId")
    public int sessionId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getMethod_type() {
        return method_type;
    }

    public void setMethod_type(int method_type) {
        this.method_type = method_type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }
}
