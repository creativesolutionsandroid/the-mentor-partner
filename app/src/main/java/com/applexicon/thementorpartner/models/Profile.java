package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.SerializedName;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Hamdy on 8/21/2017.
 */

public class Profile {

    @SerializedName("id")
    public String id;

    @SerializedName("phone")
    public String phone;

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("address")
    public String address;

    @SerializedName("profile_pic")
    public String imageUrl;



    private String TAG = "Profile";


    public void fetchProfile(final Context context,
                             Consumer<Response<Profile>> consumer,
                             final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchProfile ");
        ProfileAPIInterface apiInterface =
                ApiClient.getClient(context).create(ProfileAPIInterface.class);

        Observable<Response<Profile>> response = apiInterface.fetchProfile(Mentor.getInstance().token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "fetchProfile : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }



    private interface  ProfileAPIInterface{
        @GET(Constants.FETCH_PROFILE_URL_GET)
        Observable<Response<Profile>> fetchProfile(
                @Query("token") String token
        );
    }

}
