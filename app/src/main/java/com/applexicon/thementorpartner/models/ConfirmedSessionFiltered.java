package com.applexicon.thementorpartner.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hamdy on 8/13/2017.
 */

public class ConfirmedSessionFiltered implements Serializable{

//    {"id":1,"address":"52327 Hahn Squares\nRicechester, AZ 82309-4998","session_date":"2017-08-15","teacher_id":207,"teacher_name":"shokry1","teacher_rate":2}
    @SerializedName("id")
    public String id;

    public String bookingId;

    @SerializedName("course_name")
    public String subjectName;

    @SerializedName("group_id")
    public String groupId;

    @SerializedName("studentName")
    public String studentName;

    @SerializedName("user_name")
    public String userName;

    @SerializedName("studentGender")
    public String studentGender;

    @SerializedName("payment_type")
    public String payment_type;

    @SerializedName("address")
    public String location;

    @SerializedName("session_date")
    public String date;

    @SerializedName("session_price")
    public String amount;

    @SerializedName("currency")
    public String currency;

    @SerializedName("fees")
    public String paidAmount;

    @SerializedName("grade_name")
    public String grade_name;

    @SerializedName("level_name")
    public String level_name;

    @SerializedName("grade_name_in_ar")
    public String grade_name_in_ar;

    @SerializedName("level_name_in_ar")
    public String level_name_in_ar;

    @SerializedName("course_name_in_ar")
    public String course_name_in_ar;

    @SerializedName("distance_in_km")
    public double distance_in_km;

    @SerializedName("availabe_for_start")
    public boolean availabeForStart;

    @SerializedName("time_from")
    public String timeFrom;

    @SerializedName("time_to")
    public String timeTo;

    @SerializedName("long")
    public double sessionLongitude;

    @SerializedName("lat")
    public String sessionLatitude;

    @SerializedName("teacher_lat")
    public String teacherLatitude;

    @SerializedName("teacher_lng")
    public String teacherLongitude;

    @SerializedName("student_phone")
    public String studentPhone;

    @SerializedName("session_from_time")
    public String session_from_time;

    @SerializedName("session_to_time")
    public String session_to_time;

    @SerializedName("session_type")
    public String session_type;

    @SerializedName("session_location")
    public String session_location;

    @SerializedName("booking_name")
    public String booking_name;

    @SerializedName("session_name")
    public String session_name;

}
