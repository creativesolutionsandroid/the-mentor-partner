package com.applexicon.thementorpartner.models;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.applexicon.thementorpartner.broadCasts.EndSessionAlarmBroadCast;
import com.applexicon.thementorpartner.broadCasts.StartSessionAlarmBroadCast;
import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Hamdy on 10/31/2017.
 */

public class SessionRequestModel implements Serializable {

    @SerializedName("requestId")
    public String id;

    @SerializedName("course_name")
    public String subject;

    @SerializedName("studentName")
    public String studentName;

    @SerializedName("user_name")
    public String userName;

    @SerializedName("address")
    public String sessionLocation;

    @SerializedName("studentGender")
    public String studentGender;

    @SerializedName("studentMobile")
    public String studentMobile;

    @SerializedName("grade_name")
    public String grade_name;

    @SerializedName("level_name")
    public String level_name;

    @SerializedName("grade_name_in_ar")
    public String grade_name_in_ar;

    @SerializedName("level_name_in_ar")
    public String level_name_in_ar;

    @SerializedName("course_name_in_ar")
    public String course_name_in_ar;

    @SerializedName("distance_in_km")
    public String distance;

    @SerializedName("session_location")
    public String session_location;

    @SerializedName("schudule_time")
    public String schedule = "" ;

    @SerializedName("sessions")
    public ArrayList<Session> sessions;
    private String TAG = "SessionRequestModel";

    public void addToAlarmManager(Context context) {
        for(Session session : sessions) {
            session.setStartSessionAlarm(context);
        }
    }

    public void accept(final Context context, Consumer<Response> consumer, final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "accept id " + id);

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.accept(id);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "accept : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });

    }

    public void reject(final Context context, Consumer<Response> consumer, final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "reject ");
        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.reject( id);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "reject : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }


    public interface  APIInterface {
        @FormUrlEncoded
        @POST(Constants.REJECT_URL_POST)
        Observable<Response> reject(
              @Field("request_id") String sessionId
        );

        @FormUrlEncoded
        @POST(Constants.ACCEPT_URL_POST)
        Observable<Response> accept(
//                @Header("authorization") String token,
                @Field("request_id") String request_id
        );

    }
}
