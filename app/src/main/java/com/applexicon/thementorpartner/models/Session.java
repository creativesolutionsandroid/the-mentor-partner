package com.applexicon.thementorpartner.models;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.applexicon.thementorpartner.broadCasts.EndSessionAlarmBroadCast;
import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.broadCasts.StartSessionAlarmBroadCast;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Hamdy on 8/13/2017.
 */

public class Session  implements Serializable{

    public Float rate;
    public String comment;

    public static final String START_SESSION_ACTION = "com.applexicon.thementorpartner.start_session";
    public static final String END_SESSION_ACTION = "com.applexicon.thementorpartner.end_session";
    public static final String ARG_SESSION_EXTRA = "arg_session_extra";
    private static final long ALARM_INTERVAL = 1000 * 2 * 1;
    //{"sessionInfo":{"id":3,"day":"28 August, 2017","timeFrom":"06:00 PM","timeTo":"09:00 AM","studentName":"hamdy"}
    @SerializedName("id")
    public String id;

    @SerializedName("subjectName")
    public String subject;

    @SerializedName("address")
    public String sessionLocation;

    @SerializedName("timeFrom")
    public String timeFrom;

    @SerializedName("timeTo")
    public String timeTo;

    @SerializedName("studentName")
    public String studentName;

    @SerializedName("date")
    public String date ;

    @SerializedName("session_name")
    public String session_name = "";

    @SerializedName("schudule_time")
    public String schedule = "" ;

    private String TAG = "SessionModel";

    public void fetchDaySessions(final Context context,
                                 Consumer<Response<FetchDaySessionsContentData>> consumer,
                                 final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchDaySessions ");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<FetchDaySessionsContentData>> response = APIInterface.fetchDaySessions(Mentor.getInstance().token , date);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "fetchDaySessions " + throwable.getMessage());

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void fetchSessionDetails(final Context context,
                                 Consumer<Response<FetchSessionDetailsContentData>> consumer,
                                 final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "fetchSessionDetails ");
        APIInterface APIInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<FetchSessionDetailsContentData>> response = APIInterface.fetchSessionDetails(id , Mentor.getInstance().token);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "fetchSessionDetails " + throwable.getMessage());

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }


    public void paymentRequest(final Context context,
                               Consumer<Response> consumer,
                               final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "paymentRequest session_id : " + id);

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.paymentRequest(id);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "paymentRequest : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void rateStudent(final Context context,
                            Consumer<Response> consumer,
                            final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.addReview(id, rate, comment);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e("TAG" , "end : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void end(final Context context,
                    Consumer<Response> consumer,
                    final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        Log.e(TAG , "end ");
        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response> response = apiInterface.end(id);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "end : " + throwable.getMessage()  );

                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void cancelStartSessionAlarmManager(Context context){
        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);

//        Intent i = new Intent(context, StartSessionAlarmBroadCast.class);
//        i.setAction(Session.START_SESSION_ACTION);
//        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
//        alarmManager.cancel(pi);
        Log.e(TAG,"REMOVE ALERT START SESSION");
    }

    private long getEndTimeInMill() {

        String time = date + " " + timeTo;
        Log.e(TAG , "getEndTimeInMill time : "  + time);
        String format = "yyyy-MM-dd hh:mm";
        return  milliseconds(time , format);
    }

    private long getStartTimeInMill() {
//        String time = "2017-09-5 4:00 PM";
        String time = date + " " + timeFrom;
        Log.e(TAG , "getStartTimeInMill time : "  + time);
        String format = "yyyy-MM-dd hh:mm";
        return  milliseconds(time , format);
    }


    private long milliseconds(String date , String format) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    public void setStartSessionAlarm(Context context )
    {

        long startTimeMill = getStartTimeInMill();
        AlarmManager  am =(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

//        Intent i = new Intent(context, StartSessionAlarmBroadCast.class);
//        i.setAction(Session.START_SESSION_ACTION);
//        i.putExtra(Session.ARG_SESSION_EXTRA , this);
//        PendingIntent pi = PendingIntent.getBroadcast(context, Integer.parseInt(id), i, 0);
//
//        am.set(AlarmManager.RTC_WAKEUP, startTimeMill , pi);

        Log.e(TAG , "setStartSessionAlarm id " + id);
        Log.e(TAG , "setStartSessionAlarm date : " + date);
        Log.e(TAG , "setStartSessionAlarm from : " + timeFrom);

//        am.setRepeating(AlarmManager.RTC_WAKEUP, startTimeMill , ALARM_INTERVAL, pi); // Millisec * Second * Minute
    }


    public void setEndSessionAlarm(Context context )
    {
        Log.e(TAG , "setEndSessionAlarm id " + id);
        Log.e(TAG , "setEndSessionAlarm date " + date);
        Log.e(TAG , "setEndSessionAlarm to " + timeTo);

        long endTimeMill = getEndTimeInMill();
        AlarmManager  am =(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, EndSessionAlarmBroadCast.class);
        i.setAction(Session.END_SESSION_ACTION);
        i.putExtra(Session.ARG_SESSION_EXTRA , this);
        PendingIntent pi = PendingIntent.getBroadcast(context,  Integer.parseInt(id) , i, 0);

    am.set(AlarmManager.RTC_WAKEUP, endTimeMill , pi);
       // am.setRepeating(AlarmManager.RTC_WAKEUP, endTimeMill , ALARM_INTERVAL, pi); // Millisec * Second * Minute
    }

    public interface  APIInterface {

        @FormUrlEncoded
//        @POST(Constants.START_URL_POST)
        @POST(Constants.PAYMENT_REQUEST)
        Observable<Response> paymentRequest(
                @Field("session_id") String sessionId
        );

        @FormUrlEncoded
        @POST(Constants.END_URL_POST)
        Observable<Response> end(
                @Field("session_id") String sessionId
        );

        @FormUrlEncoded
        @POST(Constants.ACCEPT_URL_POST)
        Observable<Response> accept(
//                @Header("authorization") String token,
                @Field("session_id") String sessionId
        );

        @FormUrlEncoded
        @POST(Constants.REJECT_URL_POST)
        Observable<Response> reject(
                @Field("token") String token
                , @Field("session_id") String sessionId
        );

        @GET(Constants.FETCH_DAY_SESSIONS)
        Observable<Response<FetchDaySessionsContentData>> fetchDaySessions(
                @Query("token") String token,
                @Query("date") String date
        );

        @GET(Constants.FETCH_SESSION_DETAILS)
        Observable<Response<FetchSessionDetailsContentData>> fetchSessionDetails(
                @Query("session_id") String session_id,
                @Query("token") String token
        );

        @FormUrlEncoded
        @POST(Constants.ADD_REVIEW)
        Observable<Response> addReview(
                @Field("session_id") String sessionId,
                @Field("rate") float rate,
                @Field("comment") String review
        );
    }

    public class FetchDaySessionsContentData{
        @SerializedName("sessionsInDay")
        public List<Session> daySessions;
    }

    public class FetchSessionDetailsContentData{
        @SerializedName("sessionInfo")
        public Session session;
    }
}
