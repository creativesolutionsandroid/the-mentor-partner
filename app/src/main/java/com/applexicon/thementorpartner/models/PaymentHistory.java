package com.applexicon.thementorpartner.models;

import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Hamdy on 8/13/2017.
 */

public class PaymentHistory implements Serializable {

    @Expose
    @SerializedName("errorCode")
    private int errorCode;
    @Expose
    @SerializedName("content")
    private Content content;
    @Expose
    @SerializedName("result")
    private boolean result;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public static class Content {
        @Expose
        @SerializedName("newToken")
        private int newToken;
        @Expose
        @SerializedName("data")
        private Data data;

        public int getNewToken() {
            return newToken;
        }

        public void setNewToken(int newToken) {
            this.newToken = newToken;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }
    }

    public static class Data {
        @Expose
        @SerializedName("paymentHistory")
        private Paymentistory Paymentistory;

        public Paymentistory getPaymentHistory() {
            return Paymentistory;
        }

        public void setPaymentHistory(Paymentistory paymentHistory) {
            this.Paymentistory = paymentHistory;
        }
    }

    public static class Paymentistory {
        @Expose
        @SerializedName("payments")
        private List<Payments> payments;
        @Expose
        @SerializedName("totalPayments")
        private TotalPayments totalPayments;

        public List<Payments> getPayments() {
            return payments;
        }

        public void setPayments(List<Payments> payments) {
            this.payments = payments;
        }

        public TotalPayments getTotalPayments() {
            return totalPayments;
        }

        public void setTotalPayments(TotalPayments totalPayments) {
            this.totalPayments = totalPayments;
        }
    }

    public static class Payments {
        @Expose
        @SerializedName("studentName")
        public String name;
        @Expose
        @SerializedName("amount")
        public float amount;
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("method_type")
        public int method_type;
        @Expose
        @SerializedName("currency")
        public String currency;
        @Expose
        @SerializedName("month")
        public String month;
        @Expose
        @SerializedName("day")
        public String day;
        @Expose
        @SerializedName("bookingName")
        public String bookingName;
        @Expose
        @SerializedName("sessionName")
        public String sessionName;
        @Expose
        @SerializedName("sessionId")
        public int sessionId;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getAmount() {
            return amount;
        }

        public void setAmount(float amount) {
            this.amount = amount;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getMethod_type() {
            return method_type;
        }

        public void setMethod_type(int method_type) {
            this.method_type = method_type;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public String getBookingName() {
            return bookingName;
        }

        public void setBookingName(String bookingName) {
            this.bookingName = bookingName;
        }

        public String getSessionName() {
            return sessionName;
        }

        public void setSessionName(String sessionName) {
            this.sessionName = sessionName;
        }

        public int getSessionId() {
            return sessionId;
        }

        public void setSessionId(int sessionId) {
            this.sessionId = sessionId;
        }
    }

    public static class TotalPayments {
        @Expose
        @SerializedName("pending_card_amount")
        private float pending_card_amount;
        @Expose
        @SerializedName("pending_cash_amount")
        private float pending_cash_amount;
        @Expose
        @SerializedName("paid_card_amount")
        private float paid_card_amount;
        @Expose
        @SerializedName("paid_cash_amount")
        private float paid_cash_amount;

        public float getPending_card_amount() {
            return pending_card_amount;
        }

        public void setPending_card_amount(float pending_card_amount) {
            this.pending_card_amount = pending_card_amount;
        }

        public float getPending_cash_amount() {
            return pending_cash_amount;
        }

        public void setPending_cash_amount(float pending_cash_amount) {
            this.pending_cash_amount = pending_cash_amount;
        }

        public float getPaid_card_amount() {
            return paid_card_amount;
        }

        public void setPaid_card_amount(float paid_card_amount) {
            this.paid_card_amount = paid_card_amount;
        }

        public float getPaid_cash_amount() {
            return paid_cash_amount;
        }

        public void setPaid_cash_amount(float paid_cash_amount) {
            this.paid_cash_amount = paid_cash_amount;
        }
    }

    public interface APIInterface {
        @GET(Constants.FETCH_PAYMENT_HISTORY)
        Call<PaymentHistory> fetchPaymentHistory(
                @Query("token") String token
        );
    }
}
