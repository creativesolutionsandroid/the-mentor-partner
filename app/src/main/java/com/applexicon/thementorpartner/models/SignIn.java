package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.SerializedName;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


/**
 * Created by Hamdy on 7/24/2017.
 */

public class SignIn{

    String TAG = "SignIn";

    @SerializedName("phone") private String phone;
    @SerializedName("phone") private String password;
     private String phoneKey;



    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneKey() {
        return phoneKey;
    }

    public void setPhoneKey(String phoneKey) {
        this.phoneKey = phoneKey;
    }

        public void signIn(final Context context , Consumer<Response<SignInContentData>> consumer , final Response.ResponseErrorCodeHandler responseErrorCodeHandler){

            Log.e(TAG , "signIn "+phoneKey + phone);
            Log.e(TAG , "signIn "+password);
            SignInAPIInterface signInAPIInterface =
                    ApiClient.getClient(context).create(SignInAPIInterface.class);

            Observable<Response<SignInContentData>> response = signInAPIInterface. signIn(phoneKey + phone , password);

            response.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(consumer, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {

                            Response response = new Response();

                            response.setThroeable(throwable);

                            response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                        }
                    });
        }

        public interface SignInAPIInterface {

            @FormUrlEncoded
            @POST(Constants.LOGIN_POST_URL)
            Observable<Response<SignInContentData>> signIn(
                    @Field("phone") String phone ,
                    @Field("password") String password );
        }


        public class SignInContentData {

            @SerializedName("user")
            public Mentor mentor;

            @SerializedName("token")
            public String token;

        }

}
