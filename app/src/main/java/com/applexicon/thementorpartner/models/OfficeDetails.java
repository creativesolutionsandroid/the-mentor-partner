package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.SerializedName;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;

/**
 * Created by Hamdy on 8/7/2017.
 */

public class OfficeDetails {



    String TAG = "OfficeDetails";

    @SerializedName("address") private String address;
    @SerializedName("working_days") private String workingDays;
    @SerializedName("phone") private String phone;
    @SerializedName("email") private String email;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(String workingDays) {
        this.workingDays = workingDays;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void getOfficeDate(final Context context , Consumer<Response<OfficeDetails>> consumer , final  Response.ResponseErrorCodeHandler responseErrorCodeHandler){

        Log.e(TAG , "getOfficeDate");
        OfficeAPIInterface officeAPIInterface =
                ApiClient.getClient(context).create(OfficeAPIInterface.class);

        Observable<Response<OfficeDetails>> response = officeAPIInterface.getOfficeDetails();

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer , new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG, "editProfile : " + throwable.getMessage());
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context, responseErrorCodeHandler);
                    }
                });
    }

    public interface OfficeAPIInterface {

        @GET(Constants.OFFICE_DETAILS_GET_URL)
        Observable<Response<OfficeDetails>> getOfficeDetails();
    }


}
