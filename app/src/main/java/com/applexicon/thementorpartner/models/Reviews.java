package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public class Reviews {

    @Expose
    @SerializedName("created_at")
    public String created_at;
    @Expose
    @SerializedName("session_id")
    public int session_id;
    @Expose
    @SerializedName("student_name")
    public String student_id;
    @Expose
    @SerializedName("teacher_id")
    public int teacher_id;
    @Expose
    @SerializedName("rate")
    public String rate;
    @Expose
    @SerializedName("comment")
    public String comment;
    @Expose
    @SerializedName("id")
    public int id;
    @Expose
    @SerializedName("session_name")
    public String session_name;


    public void fetchReviews(final Context context,
                          Consumer<Response<FetchReviewsContentData>> consumer,
                          final Response.ResponseErrorCodeHandler responseErrorCodeHandler) {

        APIInterface apiInterface =
                ApiClient.getClient(context).create(APIInterface.class);

        Observable<Response<FetchReviewsContentData>> response = apiInterface.start();

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e("TAG" , "end : " + throwable.getMessage()  );

                        Response<FetchReviewsContentData> response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public interface  APIInterface {

        @GET(Constants.REVIEWS_URL_GET)
        Observable<Response<FetchReviewsContentData>> start();
    }

    public class FetchReviewsContentData implements Serializable {
        @SerializedName("reviews")
        public List<Reviews> reviews;
    }

}
