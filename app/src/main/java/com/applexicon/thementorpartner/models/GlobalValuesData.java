package com.applexicon.thementorpartner.models;

import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class GlobalValuesData {
    @Expose
    @SerializedName("result")
    private boolean result;
    @Expose
    @SerializedName("content")
    private Content content;

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public static class Content {
        @Expose
        @SerializedName("data")
        private Data data;

        @Expose
        @SerializedName("message")
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }
    }

    public static class Data {
        @Expose
        @SerializedName("studentSessions")
        private StudentSessions studentSessions;
        @Expose
        @SerializedName("globalValues")
        private List<GlobalValues> globalValues;
        @Expose
        @SerializedName("server_time")
        private String server_time;

        public String getServer_time() {
            return server_time;
        }

        public void setServer_time(String server_time) {
            this.server_time = server_time;
        }

        public StudentSessions getStudentSessions() {
            return studentSessions;
        }

        public void setStudentSessions(StudentSessions studentSessions) {
            this.studentSessions = studentSessions;
        }

        public List<GlobalValues> getGlobalValues() {
            return globalValues;
        }

        public void setGlobalValues(List<GlobalValues> globalValues) {
            this.globalValues = globalValues;
        }

        public static class StudentSessions implements Serializable {
            @Expose
            @SerializedName("updated_at")
            private String updated_at;
            @Expose
            @SerializedName("created_at")
            private String created_at;
            @Expose
            @SerializedName("record_status")
            private String record_status;
            @Expose
            @SerializedName("remarks")
            private String remarks;
            @Expose
            @SerializedName("amount")
            private float amount;
            @Expose
            @SerializedName("user_type")
            private String user_type;
            @Expose
            @SerializedName("user_id")
            private int user_id;
            @Expose
            @SerializedName("session_id")
            private int session_id;
            @Expose
            @SerializedName("id")
            private int id;
            @Expose
            @SerializedName("booking_name")
            private String booking_name;

            public String getBooking_name() {
                return booking_name;
            }

            public void setBooking_name(String booking_name) {
                this.booking_name = booking_name;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getRecord_status() {
                return record_status;
            }

            public void setRecord_status(String record_status) {
                this.record_status = record_status;
            }

            public String getRemarks() {
                return remarks;
            }

            public void setRemarks(String remarks) {
                this.remarks = remarks;
            }

            public float getAmount() {
                return amount;
            }

            public void setAmount(float amount) {
                this.amount = amount;
            }

            public String getUser_type() {
                return user_type;
            }

            public void setUser_type(String user_type) {
                this.user_type = user_type;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public int getSession_id() {
                return session_id;
            }

            public void setSession_id(int session_id) {
                this.session_id = session_id;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }

        public static class GlobalValues {
            @Expose
            @SerializedName("updated_at")
            private String updatedAt;
            @Expose
            @SerializedName("permission")
            private String permission;
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("key")
            private String key;
            @Expose
            @SerializedName("id")
            private int id;

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getPermission() {
                return permission;
            }

            public void setPermission(String permission) {
                this.permission = permission;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }

    }


//    @Expose
//    @SerializedName("updated_at")
//    private String updated_at;
//    @Expose
//    @SerializedName("permission")
//    private String permission;
//    @Expose
//    @SerializedName("value")
//    private String value;
//    @Expose
//    @SerializedName("key")
//    private String key;
//    @Expose
//    @SerializedName("id")
//    private int id;


//    public void fetchGlobalValues() {

//        APIInterface aPIInterface =
//                ApiClientForGloabalValues.getClient(context).create(APIInterface.class);
//
//        Observable<Response<GlobalValuesContentData>> response = aPIInterface.fetchGlobalValues();
//
//        response.observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(consumer, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//
//                        Log.e("TAG", "fetchGlobalValues : " + throwable.getMessage());
//                        Response response = new Response();
//
//                        response.setThroeable(throwable, context);
//
//                        response.handleResponseErrorCode(context, responseErrorCodeHandler);
//                    }
//                });


//    }

    public interface APIInterface {
        @GET(Constants.GLOBAL_VALUES)
        Call<GlobalValuesData> fetchGlobalValues(
                @Query("role") String role,
                @Query("token") String token
        );
    }

    public class GlobalValuesContentData {
        @SerializedName("globalValues")
        public List<GlobalValuesData> globalValuesDataList;
    }
}
