package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.network.ApiClientForGloabalValues;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


/**
 * Created by Hamdy on 7/24/2017.
 */

public class SignUp {

    String TAG = "SignUp";

    @SerializedName("fullname") private String fullname;
    @SerializedName("email") private String email;
    @SerializedName("phone") private String phone;
    @SerializedName("password") private String password;
    @SerializedName("gender") private String gender;
    @SerializedName("id_card_number") private String idCardNumber;
    @SerializedName("sex_to_teach") private String sexToTeach;
    @SerializedName("prefered_session_location") private int preferedSessionLocation;
    @SerializedName("session_type") private int sessionType;
    @SerializedName("yearOfExperience")
    private String yearOfExperience;
    private String address = "";
    private double latitude;
    private double longitude;
    private int cityId;

    private File idFile;
    private File certificateFile;
    private File resumeFile;

    private String phoneKey;
    private String courseId = "0";

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public int getPreferedSessionLocation() {
        return preferedSessionLocation;
    }

    public void setPreferedSessionLocation(int preferedSessionLocation) {
        this.preferedSessionLocation = preferedSessionLocation;
    }

    public int getSessionType() {
        return sessionType;
    }

    public void setSessionType(int sessionType) {
        this.sessionType = sessionType;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getYearOfExperience() {
        return yearOfExperience;
    }

    public void setYearOfExperience(String yearOfExperience) {
        this.yearOfExperience = yearOfExperience;
    }

    public void setPhoneKey(String phoneKey) {
        this.phoneKey = phoneKey;
    }

    public String getPhoneKey() {
        return phoneKey;
    }


    public File getIdFile() {
        return idFile;
    }

    public void setIdFile(File idFile) {
        this.idFile = idFile;
    }

    public File getCertificateFile() {
        return certificateFile;
    }

    public void setCertificateFile(File certificateFile) {
        this.certificateFile = certificateFile;
    }

    public File getResumeFile() {
        return resumeFile;
    }

    public void setResumeFile(File resumeFile) {
        this.resumeFile = resumeFile;
    }
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSexToTeach() {
        return sexToTeach;
    }

    public void setSexToTeach(String sexToTeach) {
        this.sexToTeach = sexToTeach;
    }

    public void signUp(final Context context ,
                       Consumer<Response<SignUpDataResponse >> consumer ,
                       final  Response.ResponseErrorCodeHandler responseErrorCodeHandler){

        Log.e(TAG , "editProfile ");



            SignUpAPIInterface signInAPIInterface =
                    ApiClient.getClient(context).create(SignUpAPIInterface.class);

//            RequestBody reqIdFile = RequestBody.create(MediaType.parse("*/*"), idFile);
//            MultipartBody.Part idFileBody = MultipartBody.Part.createFormData("ID", idFile.getName(), reqIdFile);

//            RequestBody reqCertificateFile = RequestBody.create(MediaType.parse("*/*"), certificateFile);
//            MultipartBody.Part certificateFileBody = MultipartBody.Part.createFormData("certificate", certificateFile.getName(), reqCertificateFile);

//            RequestBody reqResumeFile = RequestBody.create(MediaType.parse("*/*"), resumeFile);
//            MultipartBody.Part resumeFileBody = MultipartBody.Part.createFormData("resume", resumeFile.getName(), reqResumeFile);

            RequestBody reqEmail = RequestBody.create(MediaType.parse("text/plain"), email);
            RequestBody reqFullname = RequestBody.create(MediaType.parse("text/plain"), fullname);
            RequestBody reqPhone = RequestBody.create(MediaType.parse("text/plain"), phoneKey + phone);
            RequestBody reqPassword = RequestBody.create(MediaType.parse("text/plain"), password);
            RequestBody reqGender = RequestBody.create(MediaType.parse("text/plain"), gender);
            RequestBody reqYearOfExperience = RequestBody.create(MediaType.parse("text/plain"), yearOfExperience);
            RequestBody reqCourseId = RequestBody.create(MediaType.parse("text/plain"), "");
            RequestBody reqAddress = RequestBody.create(MediaType.parse("text/plain"), address);
            RequestBody reqLat = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(latitude));
            RequestBody reqLng = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longitude));
            RequestBody sexToTeachBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sexToTeach));
//            RequestBody idCard = RequestBody.create(MediaType.parse("text/plain"), idCardNumber);
            RequestBody preferedSession = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(preferedSessionLocation));
            RequestBody city_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(cityId));
//            RequestBody sessionTypeText = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sessionType));

//            Log.e(TAG, "editProfile : Idfile : " + idFile.toString());
//            Log.e(TAG, "editProfile : certificatefile : " + certificateFile.toString());
//            Log.e(TAG, "editProfile : resumefile : " + resumeFile.toString());

            Observable<Response<SignUpDataResponse>> response =
                    signInAPIInterface.signUp(
                            reqEmail,
                            reqFullname,
                            reqPhone,
                            reqPassword,
                            reqGender,
                            reqYearOfExperience,
                            reqAddress,
                            reqLat,
                            reqLng,
                            sexToTeachBody,
                            preferedSession,
                            city_id
                    );

            response.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(consumer, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {

                            Log.e(TAG, "editProfile : " + throwable.getMessage());
                            Response response = new Response();

                            response.setThroeable(throwable);

                            response.handleResponseErrorCode(context, responseErrorCodeHandler);
                        }
                    });

    }

    public void fetchCourses(final Context context,final  Consumer<Response<GetCoursesDataResponse>> consumer,final  Response.ResponseErrorCodeHandler responseErrorCodeHandler ) {
        SignUpAPIInterface signInAPIInterface =
                ApiClient.getClient(context).create(SignUpAPIInterface.class);

        Observable<Response<GetCoursesDataResponse>> response =
                signInAPIInterface.fetchCourses();

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer ,  new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Log.e(TAG , "fetchCourses " + throwable);
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context ,responseErrorCodeHandler );
                    }
                });
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseId() {
        return courseId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public interface SignUpAPIInterface {

        /*
           Email: string,
           Name: string,
           Phone: string,
           Password: string,
           Year_of_experience: string
           course_id: integer
        */
        @Multipart
        @POST(Constants.REGISTER_POST_URL)
        Observable<Response<SignUpDataResponse>> signUp(
                @Part("email") RequestBody email,
                @Part("name") RequestBody name,
                @Part("phone") RequestBody phone,
                @Part("password") RequestBody password,
                @Part("gender") RequestBody gender,
                @Part("years_of_experience") RequestBody yearOfExperience,
                @Part("address") RequestBody address,
                @Part("lat") RequestBody lat,
                @Part("lng") RequestBody lng,
                @Part("sex_to_teach") RequestBody seXToTeach,
                @Part("prefered_session_location") RequestBody prefered_session_location,
                @Part("city_id") RequestBody city_id
                );

//        @FormUrlEncoded
        @GET(Constants.GET_COURSES)
        Observable<Response<GetCoursesDataResponse>> fetchCourses(
        );

    }


    public class SignUpDataResponse{
        @SerializedName("result")
        public Boolean result;
    }

    public class GetCoursesDataResponse{
        @SerializedName("courses")
        public List<Course> courses;
    }

}
