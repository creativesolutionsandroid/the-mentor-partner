package com.applexicon.thementorpartner.models;

/**
 * Created by Hamdy on 10/29/2017.
 */

public class SessionsTime {


    private String session_date;
    private String time_from;
    private String time_to;

    public String getSession_date() {
        return session_date;
    }

    public void setSession_date(String session_date) {
        this.session_date = session_date;
    }

    public String getTime_from() {
        return time_from;
    }

    public void setTime_from(String time_from) {
        this.time_from = time_from;
    }

    public String getTime_to() {
        return time_to;
    }

    public void setTime_to(String time_to) {
        this.time_to = time_to;
    }
}
