package com.applexicon.thementorpartner.models;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


/**
 * Created by Hamdy on 7/24/2017.
 */

public class ForgotPassword {

    String TAG = "ForgotPassword";

    private String phoneKey;
    private String phone = "";

    public ForgotPassword() {
    }


    public String getPhoneKey() {
        return phoneKey;
    }

    public void setPhoneKey(String phoneKey) {
        this.phoneKey = phoneKey;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void request(final Context context , Consumer<Response> consumer , final Response.ResponseErrorCodeHandler responseErrorCodeHandler){

        Log.e(TAG , "request ");
        ForgotPasswordAPIInterface apiInterface =
                ApiClient.getClient(context).create(ForgotPasswordAPIInterface.class);

        Observable<Response> response = apiInterface.request(phoneKey + phone);

        response.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Response response = new Response();

                        response.setThroeable(throwable);

                        response.handleResponseErrorCode(context, responseErrorCodeHandler);
                    }
                });
    }

    public interface ForgotPasswordAPIInterface {

        @FormUrlEncoded
        @POST(Constants.FORGOT_PASSWORD)
        Observable<Response> request(
                @Field("phone") String phone
        );
    }
}
