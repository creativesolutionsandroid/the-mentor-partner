package com.applexicon.thementorpartner.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import io.reactivex.functions.Consumer;

/**
 * Created by AppleXIcon on 10/4/2017.
 */


public class MyFirebaseMessagingService extends FirebaseMessagingService implements Response.ResponseErrorCodeHandler{

    private static final String TAG = "MyFirebaseMsgService";
    public static final int NEW_SESSION = 1;
    Intent intent;
    private Bundle extras;
    Uri sound;
    String channelName = "Alerts";
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Log.e(TAG, "From: " + remoteMessage.toString());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            //Message data payload: {tripId=1, kidStatus=0, kidId=1, notificationType=3}
            //Message Notification Body: Your Kid first kid status is In Bus

            Log.e(TAG, "Message data payload: " + remoteMessage.getData());

            int type = Integer.parseInt(remoteMessage.getData().get("notificationType"));
            Log.e(TAG, "Message data type " + type);

            if(type == NEW_SESSION) {
                sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tone);
                channelName = "New Session";
            }
            else {
                sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                channelName = "Alerts";
            }

//            if (type == SESSION_STARTED) {
//
//                SessionStartedBroadcast.start(this ,
//                        remoteMessage.getData().get("session_id") ,
//                        remoteMessage.getData().get("date") ,  remoteMessage.getData().get("to")  );
//            }
//            sendNotification(remoteMessage.getData().get(""), remoteMessage.getNotification().getTitle());

            sendBroadcast();
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            String body = remoteMessage.getNotification().getBody();

            if (remoteMessage.getNotification().getSound().equals("tone")){
                sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tone);
            }
            else {
                sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            }

            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            sendNotification(body, remoteMessage.getNotification().getTitle());

        }
    }

    private void sendNotification(String messageBody, String title) {
         intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if(extras != null)
            intent.putExtras(extras);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap notificationLargeIconBitmap = BitmapFactory.decodeResource(
                this.getResources(),
                R.drawable.mentor_logo);

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("time", System.currentTimeMillis());

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Adds the back stack for the Intent (but not the Intent itself).
        stackBuilder.addParentStack(MainActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), resultPendingIntent);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.mentor_logo)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setLargeIcon(notificationLargeIconBitmap)
                .setAutoCancel(true)
                .setSound(sound)
                .setContentIntent(resultPendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (sound != null) {

                String channelId  = "1";

                // Changing Default mode of notification
                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
                // Creating an Audio Attribute
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();
                NotificationChannel channel = new NotificationChannel(channelId,
                        channelName,
                        NotificationManager.IMPORTANCE_HIGH);
                channel.setSound(sound, audioAttributes);
                notificationManager.createNotificationChannel(channel);
            }
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    public void sendBroadcast(){
        Intent intent = new Intent("Session");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void sendTokenToServer(String deviceToken, Context context) {
        Mentor.getInstance().sendFirebaseToken(context,deviceToken, new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                if (response.getResult()) {

                    Log.e(TAG, "sendTokenToServer ");

                }

            }
        }, this);
    }

    @Override
    public void onNewToken(String token) {
        // Get updated InstanceID token.
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendTokenToServer(token, this);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG, "handleResponseErrorCodeForUser ");

    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG, "handleResponseErrorCodeForDebug ");

    }
}
