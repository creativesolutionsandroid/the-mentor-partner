package com.applexicon.thementorpartner.broadCasts;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.views.activities.PaymentRequestAlarmActivity;

public class SessionStartedBroadcast extends BroadcastReceiver {

    public static final String SESSION_STARTED_ACTION = "com.applexicon.thementorpartner.session_started";
    public static final String SESSION_ID = "session_id";
    public static final String SESSION_DATE = "session_date";
    public static final String SESSION_TIME_TO = "session_time_to";
    private static String TAG = "SessionStartedBroadcast";

    @Override
    public void onReceive(Context context, Intent intent) {

//        Log.e(TAG, "action : " + intent.getAction());
        Log.e(TAG, "onReceive");


        String sessionId = intent.getStringExtra(SESSION_ID);
        String sessionDate = intent.getStringExtra(SESSION_DATE);
        String sessionTimeTo = intent.getStringExtra(SESSION_TIME_TO);

        Session  session = new Session();

        session.id = sessionId;
        session.date = sessionDate;
        session.timeTo = sessionTimeTo;

        session.setEndSessionAlarm(context);

//        session.cancelStartSessionAlarmManager(context);
    }

    public static void start(Context context, String sessionId, String date , String to) {
        Log.e(TAG , "start");
        Intent i = new Intent(context , SessionStartedBroadcast.class);
        i.setAction(SessionStartedBroadcast.SESSION_STARTED_ACTION);
        i.putExtra(SessionStartedBroadcast.SESSION_ID , sessionId );
        i.putExtra(SessionStartedBroadcast.SESSION_DATE ,date );
        i.putExtra(SessionStartedBroadcast.SESSION_TIME_TO ,to);

        context.sendBroadcast(i);
    }
}