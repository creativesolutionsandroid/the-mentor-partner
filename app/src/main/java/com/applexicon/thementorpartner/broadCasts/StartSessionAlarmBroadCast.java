package com.applexicon.thementorpartner.broadCasts;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.views.activities.PaymentRequestAlarmActivity;

public class StartSessionAlarmBroadCast extends BroadcastReceiver {

    AlarmManager am;
    private String TAG = "StartSessAlarmBroadCast";

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        // Put here YOUR code.
//        Toast.makeText(context, "StartSessionAlarmBroadCast !!!!!!!!!!", Toast.LENGTH_LONG).show(); // For example

        Log.e(TAG, "action : " + intent.getAction());

        String action = intent.getAction();

        if (action.equals(Session.START_SESSION_ACTION)) {
//            Intent i = new Intent
//                    (context, PaymentRequestAlarmActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            i.setAction(Session.START_SESSION_ACTION);
//            i.putExtra(Session.ARG_SESSION_EXTRA, intent.getSerializableExtra(Session.ARG_SESSION_EXTRA));
//            context.startActivity(i);
        }

        wl.release();
    }
}