package com.applexicon.thementorpartner.broadCasts;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.views.activities.EndSessionAlarmActivity;

public class EndSessionAlarmBroadCast extends BroadcastReceiver
{

    private String TAG ="EndSessBroadCast";

    @Override
    public void onReceive(Context context, Intent intent) 
    {   
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        Log.e(TAG , "action : "+ intent.getAction());

        String action = intent.getAction();

        if(action.equals(Session.END_SESSION_ACTION)){
            Intent i = new Intent(context, EndSessionAlarmActivity.class);
            i.setAction(Session.END_SESSION_ACTION);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(Session.ARG_SESSION_EXTRA , intent.getSerializableExtra(Session.ARG_SESSION_EXTRA));
            context.startActivity(i);
        }


        wl.release();
    }
}