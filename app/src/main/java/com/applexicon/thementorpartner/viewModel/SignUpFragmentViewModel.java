package com.applexicon.thementorpartner.viewModel;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.ObservableInt;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.CitiesData;
import com.applexicon.thementorpartner.models.SignUp;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.network.ApiClientForGloabalValues;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.response.ResponseErrorCode;
import com.applexicon.thementorpartner.response.ResponseValidationErrors;
import com.applexicon.thementorpartner.utilities.BlockView;
import com.applexicon.thementorpartner.utilities.GetAddressFromLocation;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.utilities.UserLocation;
import com.applexicon.thementorpartner.views.activities.UserActivity;
import com.applexicon.thementorpartner.views.activities.VerificationActivity;
import com.applexicon.thementorpartner.views.activities.WebViewActivity;
import com.applexicon.thementorpartner.views.adapters.GooglePlacesAutocompleteAdapter;
import com.applexicon.thementorpartner.views.fragments.SignUpFragment;
import com.applexicon.thementorpartner.databinding.FragmentSignUpBinding;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import vendor.Errors.IVewError;
import vendor.Errors.MyEditText;
import vendor.Errors.MyTextView;
import vendor.Errors.TextViewMessage;
import vendor.Errors.ValidationError;
import vendor.Errors.TextInput;
import vendor.Validation.Email;
import vendor.Validation.Equal;
import vendor.Validation.IValidationHandler;
import vendor.Validation.IValidationRule;
import vendor.Validation.Max;
import vendor.Validation.Minimum;
import vendor.Validation.Mobile;
import vendor.Validation.MobileMax;
import vendor.Validation.NotNull;
import vendor.Validation.Required;
import vendor.Validation.Validation;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class SignUpFragmentViewModel extends Observable
        implements
        IValidationHandler,
        Response.ResponseErrorCodeHandler,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    public static final int ID_FILE_SELECT_CODE = 0;
    public static final int CERTIFICATE_FILE_SELECT_CODE = 1;
    public static final int RESUME_FILE_SELECT_CODE = 2;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 500;

    public final SignUp signUp;

    private final FragmentSignUpBinding binding;
    private final UserActivityViewModel viewModel;
    private final SignUpFragment signUpFragment;
    private String signUpMessage;
    public String TAG = "SignInFragmentViewModel";
    public String message;


    private String errorValidationCourses;
    private String errorValidationUploadId;
    private String errorValidationUploadCertificate;
    private String errorValidationUploadResume;

    public ObservableInt errorValidationCoursesVisibility;
    public ObservableInt errorValidationUploadIdVisibility;
    public ObservableInt errorValidationUploadCertificateVisibility;
    public ObservableInt errorValidationUploadResumeVisibility;
    public ObservableInt signUpMessageVisibility;


    private Context context;
    private String courseId;
    public GoogleApiClient mGoogleApiClient;
    private View view;
    private String sexToTeach = null;
    private String preferredLocation = null;
//    private String sessionType = null;

    private String apiKey = "AIzaSyDZZNRKAws3SM17bZHrBYLRGR6Vf0VSfpU";


    public SignUpFragmentViewModel(@NonNull SignUpFragment signUpFragment, FragmentSignUpBinding binding) {

        this.signUpFragment = signUpFragment;

        this.context = signUpFragment.getActivity();

        this.viewModel = ((UserActivity) signUpFragment.getActivity()).viewModel;

        errorValidationCoursesVisibility = new ObservableInt(View.GONE);
        errorValidationUploadIdVisibility = new ObservableInt(View.GONE);
        errorValidationUploadCertificateVisibility = new ObservableInt(View.GONE);
        errorValidationUploadResumeVisibility = new ObservableInt(View.GONE);
        signUpMessageVisibility = new ObservableInt(View.GONE);

        this.binding = binding;

        signUp = new SignUp();


//        fetchCourses();

        fetchCities();

        actionListner();

        setUpGoogleAPIClient();

        setUpAutoComplete();

        addTextWatcher();

        if (!Places.isInitialized()) {
            // Initialize Places.
            Places.initialize(context, apiKey);
        }

    }


    private void setUpAutoComplete() {
        final GooglePlacesAutocompleteAdapter adapter = new GooglePlacesAutocompleteAdapter(context, R.layout.item_place_layout);

        binding.fragmentSignUpAutoComplete.setAdapter(adapter);

        binding.fragmentSignUpAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mGoogleApiClient.connect();

                if (adapter.getList().size() > 0) {
                    String mId = adapter.getList().get(position).getId();
                    signUp.setAddress(adapter.getList().get(position).getName());
                    signUp.setLatitude(adapter.getList().get(position).getLatitude());
                    signUp.setLongitude(adapter.getList().get(position).getLongitude());
                    if (adapter.getList().get(position).getName().length() > 38) {
                        binding.fragmentSignUpAutoComplete.setText(adapter.getList().get(position).getName().substring(0, 38));
                    }

                        try {
                            // Define a Place ID.
                            String placeId = mId;

                            // Specify the fields to return.
                            List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME,
                                    Place.Field.LAT_LNG);

                            // Construct a request object, passing the place ID and fields array.
                            FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields)
                                    .build();

                            // Create a new Places client instance.
                            PlacesClient placesClient = Places.createClient(context);

                            placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
                                Place mPlace = response.getPlace();

                                LatLng qLoc = mPlace.getLatLng();
                                signUp.setAddress(mPlace.getName().toString());
                                signUp.setLatitude(qLoc.latitude);
                                signUp.setLongitude(qLoc.longitude);

                            }).addOnFailureListener((exception) -> {
                                if (exception instanceof ApiException) {
                                    ApiException apiException = (ApiException) exception;
                                    int statusCode = apiException.getStatusCode();
                                    // Handle error with given status code.
                                    Log.e(TAG, "Place not found: " + exception.getMessage());
                                }
                            });

                    } catch (Exception e) {
                    }
                }
            }
        });
    }

    private void setUpGoogleAPIClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient
                    .Builder(context)
//                    .addApi(Places.GEO_DATA_API)
//                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(signUpFragment.getActivity(), this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    private void addTextWatcher(){

        this.binding.fragmentSignUpFullnameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                EditText et = (EditText) binding.fragmentSignUpFullnameEt;
                validateFullnameOnBlur(et);
            }
        });


        this.binding.fragmentSignUpEmailEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                EditText et = (EditText) binding.fragmentSignUpEmailEt;
                validateEmailOnBlur(et);
            }
        });

        this.binding.fragmentSignUpPasswordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                EditText et = (EditText) binding.fragmentSignUpPasswordEt;
                validatePasswordOnBlur(et);
            }
        });

        this.binding.fragmentSignUpYearsOfExperienceEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                EditText et = (EditText) binding.fragmentSignUpYearsOfExperienceEt;
                validateYearOfExperienceOnBlur(et);
            }
        });

        this.binding.fragmentSignUpAgreeConditionsCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    validateInputs();
                }
            }
        });
    }

    private void actionListner() {

        this.binding.fragmentSignUpFullnameEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    EditText et = (EditText) v;
                    validateFullnameOnBlur(et);
                }
            }
        });

        this.binding.fragmentSignUpEmailEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    EditText et = (EditText) v;
                    validateEmailOnBlur(et);
                }
            }
        });

        this.binding.fragmentSignUpPhoneEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    EditText et = (EditText) v;
                    validatePhoneOnBlur(et);
                }
            }
        });

        this.binding.fragmentSignUpPasswordEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    EditText et = (EditText) v;
                    validatePasswordOnBlur(et);
                }
            }
        });

//        this.binding.fragmentSignUpIdcardEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    EditText et = (EditText) v;
//                    validateIdCardNumberOnBlur(et);
//                }
//            }
//        });

        this.binding.fragmentSignUpYearsOfExperienceEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    EditText et = (EditText) v;
                    validateYearOfExperienceOnBlur(et);
                }
            }
        });

        this.binding.fragmentSignUpPhoneKeysSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) view).setTextColor(Color.GRAY);
                signUp.setPhoneKey(context.getResources().getStringArray(R.array.phone_keys)[i].replace("00","+"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                signUp.setPhoneKey(context.getResources().getStringArray(R.array.phone_keys)[0].replace("00","+"));
            }
        });

        this.binding.fragmentHomeGenderSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTextColor(Color.GRAY);
                signUp.setGender(String.valueOf(position));
                if (position != 0) {
                    validateInputs();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final String[] genders = context.getResources().getStringArray(R.array.gender_teach_array);
        ArrayAdapter<String> arrayAdapterGenders=new ArrayAdapter<String>(context,R.layout.layout_item_gender_spinner,genders);
        binding.fragmentSignUpSexToTeach.setAdapter(arrayAdapterGenders);
        binding.fragmentSignUpSexToTeach.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position != 0) {
                    validateInputs();
                    sexToTeach = genders[position];
                    signUp.setSexToTeach(genders[position]);
                }else {
                    sexToTeach = null;
                    signUp.setSexToTeach(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                sexToTeach = null;
                signUp.setSexToTeach(null);

            }
        });

        final String[] preferredLoactionArray = context.getResources().getStringArray(R.array.prefered_session_array);
        ArrayAdapter<String> arrayAdapterpreferredLocation = new ArrayAdapter<String>(context,R.layout.layout_item_gender_spinner,preferredLoactionArray);
        binding.fragmentSignUpPreferedLocation.setAdapter(arrayAdapterpreferredLocation);
        binding.fragmentSignUpPreferedLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position != 0) {
                    validateInputs();
                    preferredLocation = ""+position;
                    signUp.setPreferedSessionLocation((position-1));
                }else {
                    preferredLocation = null;
                    signUp.setPreferedSessionLocation(-1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                preferredLocation = null;
                signUp.setPreferedSessionLocation(-1);
            }
        });

//        final String[] sessionTypeArray = context.getResources().getStringArray(R.array.session_type_array);
//        ArrayAdapter<String> arraySessionType = new ArrayAdapter<String>(context,R.layout.layout_item_gender_spinner,sessionTypeArray);
//        binding.fragmentSignUpSessionType.setAdapter(arraySessionType);
//        binding.fragmentSignUpSessionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                if (position != 0) {
//                    sessionType = ""+position;
//                    signUp.setSessionType((position-1));
//                }else {
//                    sessionType = null;
//                    signUp.setSessionType(-1);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//                sessionType = null;
//                signUp.setSessionType(-1);
//            }
//        });

    }

    private void validateFullnameOnBlur(EditText et) {

        Log.e(TAG, "validateFullnameOnBlur");
        final String fullname = et.getText().toString().trim();

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new TextInput(binding.fragmentSignUpFullnameTil),
                new IValidationRule[]{
                        new Required(fullname),
                });

        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                signUp.setFullname(fullname);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);
            }
        });
    }

    private void validateEmailOnBlur(EditText et) {
        Log.e(TAG, "validateEmailOnBlur");
        final String email = et.getText().toString().trim().toLowerCase();

        Validation validation = new Validation();

        validation.addValidationField(

                new TextInput(binding.fragmentSignUpEmailTil),
                new IValidationRule[]{
                        new Required(email),
                        new Email(email)
                });

        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                signUp.setEmail(email);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);
            }
        });
    }

    private void validatePhoneOnBlur(EditText et) {

        Log.e(TAG, "validatePhoneOnBlur");
        final String phone = et.getText().toString().trim();

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new TextInput(binding.fragmentSignUpPhoneTil),
                new IValidationRule[]{
                        new Required(phone),
                        new Mobile(phone),
                        new Equal(phone, 9)
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                signUp.setPhone(phone);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);

            }
        });
    }

    private void validatePasswordOnBlur(EditText v) {

        final String password = v.getText().toString().trim();

        Validation validation = new Validation();


        validation.addValidationField(

                new TextInput(binding.fragmentSignUpPasswordTil),
                new IValidationRule[]{
                        new Required(password),
                        new Minimum(password, 5)
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                signUp.setPassword(""+password);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);
            }
        });
    }

//    private void validateIdCardNumberOnBlur(EditText v) {
//
//        final String idCard = v.getText().toString().trim();
//
//        Validation validation = new Validation();
//
//
//        validation.addValidationField(
//
//                new TextInput(binding.fragmentSignUpPasswordTil),
//                new IValidationRule[]{
//                        new Required(idCard),
//                        new Minimum(idCard, 1)
//                });
//
//
//        validation.validate(context, new IValidationHandler() {
//            @Override
//            public void onValidationSuccessfull() {
//                signUp.setIdCardNumber(idCard);
//            }
//
//            @Override
//            public void onValidationFailed(List<ValidationError> validationErrors) {
//                onFieldValidationFailed(validationErrors);
//            }
//        });
//    }

    private void validateYearOfExperienceOnBlur(EditText et) {
        Log.e(TAG, "validateYearOfExperienceOnBlur");
        final String yearOfExperience = et.getText().toString().trim();

        Validation validation = new Validation();

        validation.addValidationField(

                new TextInput(binding.fragmentSignUpYearsOfExperienceTil),
                new IValidationRule[]{
                        new Required(yearOfExperience),
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                signUp.setYearOfExperience(yearOfExperience);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);
            }
        });
    }


    public void setFullname(final String fullnameValue) {

        final String fullname = fullnameValue.trim();


        Validation validation = new Validation();

        validation.addValidationField(

                new TextInput(binding.fragmentSignUpPhoneTil),
                new IValidationRule[]{
                        new Max(fullname, 20, context.getString(R.string.validation_equal))
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                signUp.setFullname(fullname);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {

                onFieldValidationFailed(validationErrors);

            }
        });

    }

    public void setEmail(String email) {
        signUp.setEmail(email.trim());
    }

    public void setYearsOfExperience(String yearOfExperience) {
        signUp.setYearOfExperience(yearOfExperience.trim());
    }

    public void setPhone(final String phoneValue) {
//        Log.e(TAG , "setCode : " + phone);

        final String phone = phoneValue.trim();

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new TextInput(binding.fragmentSignUpPhoneTil),
                new IValidationRule[]{
                        new MobileMax(phone, 9, context.getString(R.string.validation_mobile_equal))
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                signUp.setPhone(phone);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {

                onFieldValidationFailed(validationErrors);

            }
        });


    }

//    public void setIdCardNumber(final String passwordValue) {
////        Log.e(TAG , "setPassword : " + password);
//
//        final String password = passwordValue.trim();
//
//        Validation validation = new Validation();
//
//        //phone
//        validation.addValidationField(
//
//                new TextInput(binding.fragmentSignUpIdcardTil),
//                new IValidationRule[]{
//                        new Max(password, 30)
//                });
//
//
//        validation.validate(context, new IValidationHandler() {
//            @Override
//            public void onValidationSuccessfull() {
//                signUp.setIdCardNumber(password);
//            }
//
//            @Override
//            public void onValidationFailed(List<ValidationError> validationErrors) {
//
//                onFieldValidationFailed(validationErrors);
//
//            }
//        });
//
//
//    }

    public void setPassword(final String passwordValue) {
//        Log.e(TAG , "setPassword : " + password);

        final String password = passwordValue.trim();

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new TextInput(binding.fragmentSignUpPasswordTil),
                new IValidationRule[]{
                        new Max(password, 30)
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                signUp.setPassword(password);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {

                onFieldValidationFailed(validationErrors);

            }
        });
    }


    public String getFullname() {
        return signUp.getFullname();
    }

    public String getEmail() {
        return signUp.getEmail();
    }

    public String getYearsOfExperience() {
        return signUp.getYearOfExperience();
    }

    public String getPhone() {
        return signUp.getPhone();
    }

    public String getPassword() {
        return signUp.getPassword();
    }

//    public String getIdCardNumber() {
//        return signUp.getIdCardNumber();
//    }

    private void onFieldValidationFailed(List<ValidationError> validationErrors) {
//        this.validationErrors.clear();
//        this.validationErrors = validationErrors;

        Log.e(TAG, "onFieldValidationFailed : " + validationErrors.toString());
        for (ValidationError validationError : validationErrors) {
            validationError.displayError();
        }
    }


    public void onSignUpClick(View view) {
        Log.e(TAG, "onSignInClick");

        this.view = view;

        BlockView.enableView(signUpFragment.getActivity() , view , false);

        viewModel.animationViewVisibility.set(View.VISIBLE);

        validateInputs();

    }

    public void onTermsAndConditionsClick(View view) {
//        Log.e(TAG, "onTermsAndConditionsClick");
//        String url = String.format(Constants.HELP_URL, "terms", LanguageUtils.getUsedLang(context));
//        Log.e(TAG, "url : " + url);
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra("class","Terms and Conditions");
        context.startActivity(intent);
//        HelpItemDialogFragment.newInstance(url).show(signUpFragment.getChildFragmentManager(), "HelpItemDialogFragment");
    }

    private void validateInputs() {

        Log.e(TAG, "validateInputs");
        final String fullname = binding.fragmentSignUpFullnameEt.getText().toString().trim();
        final String email = binding.fragmentSignUpEmailEt.getText().toString().trim();
        final String yearOfExperience = binding.fragmentSignUpYearsOfExperienceEt.getText().toString().trim();
        final String phone = binding.fragmentSignUpPhoneEt.getText().toString().trim();
        final String password = binding.fragmentSignUpPasswordEt.getText().toString();
//        final String idCardNumber = binding.fragmentSignUpIdcardEt.getText().toString();
        final boolean checkValue = binding.fragmentSignUpAgreeConditionsCb.isChecked();

        int position = this.binding.fragmentSignUpPhoneKeysSp.getSelectedItemPosition();
        final String phoneKey = context.getResources().getStringArray(R.array.phone_keys)[position].replace("00","+");

        Validation validation = new Validation();

        validation.addValidationField(
                new TextInput(binding.fragmentSignUpFullnameTil),
                new IValidationRule[]{
                        new Required(fullname),
                        new Max(fullname, 20)
                });

        validation.addValidationField(
                new TextInput(binding.fragmentSignUpEmailTil),
                new IValidationRule[]{
                        new Required(email),
                        new Email(email)
                });


        validation.addValidationField(
                new TextInput(binding.fragmentSignUpYearsOfExperienceTil),
                new IValidationRule[]{
                        new Required(yearOfExperience),
                });


        validation.addValidationField(

                new TextInput(binding.fragmentSignUpPhoneTil),
                new IValidationRule[]{
                        new Required(phone),
                        new Mobile(phone),
                        new Equal(phone, 9)
                });


        validation.addValidationField(

                new TextInput(binding.fragmentSignUpPasswordTil),
                new IValidationRule[]{
                        new Required(password),
                        new Minimum(password, 5)
                });

//        validation.addValidationField(
//
//                new TextInput(binding.fragmentSignUpIdcardTil),
//                new IValidationRule[]{
//                        new Required(idCardNumber),
//                        new Minimum(idCardNumber, 1)
//                });

        validation.addValidationField(
                new MyEditText(binding.fragmentSignUpAutoComplete),
                new IValidationRule[]{
                        new Required(signUp.getAddress()),
                });

//        validation.addValidationField(
//                new TextViewMessage(binding.fragmentSignUpCourseMessage),
//                new IValidationRule[]{
//                        new CustomPattern("[1-9][0-9]*", signUp.getCourseId(), context.getString(R.string.validation_required))
//                });

//        validation.addValidationField(
//
//                new TextViewMessage(binding.fragmentSignUpUploadIdMessage),
//                new IValidationRule[]{
//                        new NotNull(signUp.getIdFile())
//                });

//        validation.addValidationField(
//
//                new TextViewMessage(binding.fragmentSignUpUploadCertificateMessage),
//                new IValidationRule[]{
//                        new NotNull(signUp.getCertificateFile())
//                });

//        validation.addValidationField(
//
//                new TextViewMessage(binding.fragmentSignUpUploadResumeMessage),
//                new IValidationRule[]{
//                        new NotNull(signUp.getResumeFile())
//                });

        validation.addValidationField(
                new MyTextView(binding.fragmentSignUpAgreeConditionsTv),
                new IValidationRule[]{
                        new Required(checkValue),
                });

        /*For Select Gender to teach*/
        validation.addValidationField(
                new TextViewMessage(binding.fragmentSignUpSexToTeachText),
                new IValidationRule[]{
                        new NotNull(sexToTeach),
                });

        /*For Prefered Location to teach*/
        validation.addValidationField(
                new TextViewMessage(binding.fragmentSignUpPreferedLocationText),
                new IValidationRule[]{
                        new NotNull(preferredLocation),
                });

        /*For City*/
        validation.addValidationField(
                new TextViewMessage(binding.signupCityText),
                new IValidationRule[]{
                        new NotNull(signUp.getCityId()),
                });

        /*For Session Type to teach*/
//        validation.addValidationField(
//                new TextViewMessage(binding.fragmentSignUpSessionTypeText),
//                new IValidationRule[]{
//                        new NotNull(sessionType),
//                });

        validation.validate(context, this);
    }


    public void onSignUpResponseSuccessfull(Response<SignUp.SignUpDataResponse> response) {
        Log.e(TAG, "onEditProfileResponseSuccessfull : " + response.toString());

        if (response.getResult()) {
            Toast.makeText(context, response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
            VerificationActivity.start(context, signUp.getPhoneKey() + getPhone(), false);
            signUpFragment.getActivity().finish();
        } else {
            if (response.getErrorCode() == ResponseErrorCode.INVALID_OR_MISSING_PARAMETERS) {
                onValidationFailed(getValidationErrors(response.getContent().getErrors()));
            } else
                response.handleResponseErrorCode(context, this);
        }

        viewModel.animationViewVisibility.set(View.GONE);

        BlockView.enableView(signUpFragment.getActivity() , view , true);

    }

    public void onResponseFailed(Throwable t) {
        Log.e(TAG, "onResponseFailed : " + t.toString());
        viewModel.animationViewVisibility.set(View.GONE);
    }

    @Override
    public void onValidationSuccessfull() {
        Log.e(TAG, "onValidationSuccessfull");

//        if(signUp.getIdFile() == null || signUp.getCertificateFile() == null || signUp.getResumeFile() == null){
//            viewModel.animationViewVisibility.set(View.GONE);
//
//            if(signUp.getIdFile() == null)
//                Toast.makeText(context, context.getString(R.string.id_required), Toast.LENGTH_SHORT).show();
//
//            if(signUp.getCertificateFile() == null)
//                Toast.makeText(context, context.getString(R.string.certificate_required), Toast.LENGTH_SHORT).show();
//
//            if(signUp.getResumeFile()  == null)
//                Toast.makeText(context,context.getString(R.string.resume_required), Toast.LENGTH_SHORT).show();
//
//        }else {

        if(signUp.getCityId() == 0 || signUp.getCityId() == -1) {
                        viewModel.animationViewVisibility.set(View.GONE);

            Toast.makeText(context,context.getString(R.string.city_required), Toast.LENGTH_SHORT).show();
        }
        else {
            signUp.signUp(context, new Consumer<Response<SignUp.SignUpDataResponse>>() {
                @Override
                public void accept(Response<SignUp.SignUpDataResponse> response) throws Exception {
                    Log.e(TAG, "Consumer<SignUp.SignUpDataResponse>()");
                    onSignUpResponseSuccessfull(response);
                    viewModel.animationViewVisibility.set(View.GONE);
                }
            }, this);
        }
//        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> validationErrors) {
        Log.e(TAG, "onValidationFailed");

        onFieldValidationFailed(validationErrors);
        viewModel.animationViewVisibility.set(View.GONE);
        BlockView.enableView(signUpFragment.getActivity() , view , true);

//        setSignInMessage(context.getString(R.string.invalid_login_credential));
    }


    public void onUploadIdClick(View v) {
        Log.e(TAG, "onUploadIdClick");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (signUpFragment.getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                signUpFragment.getActivity().requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                // app-defined int constant that should be quite unique
            } else {
                openFileChooser();
            }
        } else {
            openFileChooser();
        }


    }

    public void onUploadCertificateClick(View v) {
        Log.e(TAG, "onUploadCertificateClick");

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            ((UserActivity) context).startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload Your Certificate"),
                    CERTIFICATE_FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the mentor to the Market with a Dialog
            Toast.makeText(context, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void onUploadResumeClick(View v) {
        Log.e(TAG, "onUploadResumeClick");

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            ((UserActivity) context).startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload Your Resume"),
                    RESUME_FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the mentor to the Market with a Dialog
            Toast.makeText(context, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public String getErrorValidationCourses() {
        return errorValidationCourses;
    }

    public void setErrorValidationCourses(String errorValidationCourses) {
        errorValidationCoursesVisibility.set(View.VISIBLE);
        this.errorValidationCourses = errorValidationCourses;
    }

    public String getErrorValidationUploadId() {
        return errorValidationUploadId;
    }

    public void setErrorValidationUploadId(String errorValidationUploadId) {
        errorValidationUploadIdVisibility.set(View.VISIBLE);
        this.errorValidationUploadId = errorValidationUploadId;
    }

    public String getErrorValidationUploadCertificate() {
        return errorValidationUploadCertificate;
    }

    public void setErrorValidationUploadCertificate(String errorValidationUploadCertificate) {
        errorValidationUploadCertificateVisibility.set(View.VISIBLE);
        this.errorValidationUploadCertificate = errorValidationUploadCertificate;
    }

    public String getErrorValidationUploadResume() {
        return errorValidationUploadResume;
    }

    public void setErrorValidationUploadResume(String errorValidationUploadResume) {
        errorValidationUploadResumeVisibility.set(View.VISIBLE);

        this.errorValidationUploadResume = errorValidationUploadResume;
    }

    public String getSignUpMessage() {
        return signUpMessage;
    }

    public void setSignUpMessage(String signUpMessage) {
        this.signUpMessage = signUpMessage;
    }


    public List<ValidationError> getValidationErrors(ResponseValidationErrors signUpErrors) {

        List<ValidationError> validationErrors = new ArrayList<>();
        addVadationErrors(validationErrors, signUpErrors.emailErrors, new TextInput(binding.fragmentSignUpEmailTil));
        addVadationErrors(validationErrors, signUpErrors.nameErrors, new TextInput(binding.fragmentSignUpFullnameTil));
        addVadationErrors(validationErrors, signUpErrors.phoneErrors, new TextInput(binding.fragmentSignUpPhoneTil));
        addVadationErrors(validationErrors, signUpErrors.passwordErrors, new TextInput(binding.fragmentSignUpPasswordTil));
        addVadationErrors(validationErrors, signUpErrors.yearsOfExperienceErrors, new TextInput(binding.fragmentSignUpYearsOfExperienceTil));
        return validationErrors;
    }

    private void addVadationErrors(List<ValidationError> validationErrors, List<String> errors, IVewError iVewError) {

        if (errors != null) {
            for (String error : errors) {
                validationErrors.add(new ValidationError(error, iVewError));
            }
        }
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        viewModel.animationViewVisibility.set(View.GONE);
        BlockView.enableView(signUpFragment.getActivity() , view , true);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG, "onEditProfileResponseSuccessfull : " + message);
        viewModel.animationViewVisibility.set(View.GONE);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google API client onConnectionFailed");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "Google API client onConnected");

        UserLocation userLocation = new UserLocation(signUpFragment.getActivity());

        if (!UserLocation.isGPSEnabled(signUpFragment.getActivity())) {
            Log.e(TAG, "onConnected GPS not enable");
            userLocation.askForGPS(signUpFragment.getActivity(), mGoogleApiClient);
        } else {
            Log.e(TAG, "onConnected GPS  enable");
            getCurrentLocation();
        }

    }

    public void getCurrentLocation() {

        Log.e(TAG, "getCurrentLocation");


        if (ActivityCompat.checkSelfPermission(signUpFragment.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(signUpFragment.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            Log.e(TAG, "getCurrentLocation permission not granted");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                UserLocation.requestForLocationPermission(signUpFragment.getActivity());
            }

        } else {

            Log.e(TAG, "getCurrentLocation permission granted");

            startLocationUpdates();

            Location mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLocation == null) {
                Log.e(TAG, "location : null ");
//                startLocationUpdates();

            } else {
                Log.e(TAG, "location : " + mLocation.toString());
                getAddressFromLocation(mLocation.getLatitude(), mLocation.getLongitude());
            }
        }
    }


    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(signUpFragment.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(signUpFragment.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, UserLocation.createLocationRequest(), new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    Log.e(TAG, "onLocationChanged location : " + location.toString());

                    getAddressFromLocation(location.getLatitude(), location.getLongitude());

                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                } else
                    Log.e(TAG, "onLocationChanged location : null");

            }
        });
    }

    private void getAddressFromLocation(final double lat, final double lng) {
        GetAddressFromLocation.getCompleteAddressString(
                signUpFragment.getActivity(),
                lat,
                lng,
                new Consumer<GetAddressFromLocation.getAddressFromLocationResponse>() {
                    @Override
                    public void accept(GetAddressFromLocation.getAddressFromLocationResponse response) throws Exception {

                        if (response.status.equals("OK")) {
                            String addressSub = response.addressList.get(0).address.substring(0, 38);

                            signUp.setAddress(addressSub);
                            signUp.setLatitude(lat);
                            signUp.setLongitude(lng);
                            binding.fragmentSignUpAutoComplete.setText(addressSub);

//                            search.setAddress(response.addressList.get(0).address);
//                            search.setLat(lat);
//                            search.setLng(lng);

//                            Log.e(TAG, "getAddressFromLocation currentAddress : " + search.address);
                        }

                    }
                },
                new Response.ResponseErrorCodeHandler() {
                    @Override
                    public void handleResponseErrorCodeForUser(String message) {

                    }

                    @Override
                    public void handleResponseErrorCodeForDebug(String message) {

                    }
                });

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "Google API client onConnectionSuspended");
    }

    public void openFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            ((UserActivity) context).startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    ID_FILE_SELECT_CODE);
        } catch (ActivityNotFoundException ex) {
            // Potentially direct the mentor to the Market with a Dialog
            Toast.makeText(context, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchCities(){
        BlockView.enableView(signUpFragment.getActivity() , view , false);
        viewModel.animationViewVisibility.set(View.VISIBLE);
        CitiesData.CitiesAPIInterface apiService =
                ApiClientForGloabalValues.getClient().create(CitiesData.CitiesAPIInterface.class);
//
        Call<CitiesData> call = apiService.fetchCities();
        call.enqueue(new Callback<CitiesData>() {
            @Override
            public void onResponse(Call<CitiesData> call, retrofit2.Response<CitiesData> response) {
                Log.d(TAG, "onResponse: "+response);
                if (response.isSuccessful()) {
                    setCities(response.body().getContent().getData().getCities());
                } else {
//                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                viewModel.animationViewVisibility.set(View.GONE);
            }

            @Override
            public void onFailure(Call<CitiesData> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                viewModel.animationViewVisibility.set(View.GONE);
            }
        });
    }

    private void setCities(final List<CitiesData.Cities> cityList){
        ArrayList<String> city = new ArrayList<>();
        String languageToLoad = LanguageUtils.getUsedLang(context);
        if (languageToLoad.equalsIgnoreCase("en")) {
            city.add("-- Select City --");
        }
        else {
            city.add("-- اختر المدينة --");
        }
        for (int i = 0; i < cityList.size(); i++) {
            if (languageToLoad.equalsIgnoreCase("en")) {
                city.add(cityList.get(i).getName());
            }
            else {
                city.add(cityList.get(i).getNameInUrdu());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_item_gender_spinner, city);

        this.binding.signupCity.setAdapter(adapter);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.binding.signupCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(position == 0) {
                    signUp.setCityId(-1);
                }
                else {
                    signUp.setCityId(cityList.get((position - 1)).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                signUp.setCityId(-1);
            }
        });

        viewModel.animationViewVisibility.set(View.GONE);

        BlockView.enableView(signUpFragment.getActivity() , view , true);
    }
}
