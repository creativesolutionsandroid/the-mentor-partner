package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.ObservableField;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.fragments.SessionDetailsDialogFragment;
import com.applexicon.thementorpartner.views.fragments.SessionListDialogFragment;
import com.applexicon.thementorpartner.databinding.FragmentSessionListDialogBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class SessionListDialogFragmentVM extends Observable implements Response.ResponseErrorCodeHandler {

    private final FragmentSessionListDialogBinding binding;
    public final ObservableField<Integer> animationViewVisibility;
    public final ObservableField<Integer> messageVisibility;
    private final SessionListDialogFragment sessionListDialogFragment;
    private final Session session;
    private  String message;
    private final ArrayList<Session> dataList;
    private String TAG  = "SessionListDialFragVM";
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();



    public SessionListDialogFragmentVM(SessionListDialogFragment sessionListDialogFragment, FragmentSessionListDialogBinding binding) {

        this.sessionListDialogFragment = sessionListDialogFragment;
        this.context = sessionListDialogFragment.getActivity();
        this.binding = binding;

        this.dataList = new ArrayList<>();

        animationViewVisibility = new ObservableField<>(View.GONE);
        messageVisibility = new ObservableField<>(View.GONE);

        session = new Session();

        session.date = sessionListDialogFragment.sessionDate;


        fetchDaySessions();
    }


    private void fetchDaySessions() {

        initializeViews();

        session.fetchDaySessions(context , new Consumer<Response<Session.FetchDaySessionsContentData>>(){

            @Override
            public void accept(Response<Session.FetchDaySessionsContentData> response) throws Exception {

                Log.e(TAG , "fetchDaySessions response success " );
                if(response.getResult()){
                    if(response.getContent().getData().daySessions.size() > 0){
                        changePeopleDataSet(response.getContent().getData().daySessions);
                        messageVisibility.set(View.GONE);
                    }
                    else{
                        //not sessions found
                        setMessage(context.getString(R.string.no_sessions_found));
                    }

                }else{

                    response.handleResponseErrorCode(context , SessionListDialogFragmentVM.this);
                }

                animationViewVisibility.set(View.GONE);


            }
        } ,this );
    }


    public void initializeViews() {
        animationViewVisibility.set(View.VISIBLE);
        messageVisibility.set(View.GONE);
    }



    private void changePeopleDataSet(List<Session> dataList) {
        this.dataList.addAll(dataList);
        setChanged();
        notifyObservers();
    }

    public List<Session> getDataList() {
        return dataList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        animationViewVisibility.set(View.GONE);
        messageVisibility.set(View.VISIBLE);
        this.message = message;
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {

        animationViewVisibility.set(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        animationViewVisibility.set(View.GONE);
    }

    public void onItemClicked(Session item) {

        Log.e(TAG , "onItemClicked : " + item.id);

        item.fetchSessionDetails(context , new Consumer<Response<Session.FetchSessionDetailsContentData>>(){

            @Override
            public void accept(Response<Session.FetchSessionDetailsContentData> response) throws Exception {

                Log.e(TAG , "onItemClicked onItemClicked response success " );
                if(response.getResult()){

                     SessionDetailsDialogFragment.newInstance(response.getContent().getData().session)
                     .show(sessionListDialogFragment.getActivity().getSupportFragmentManager() , "BookingDetailsDialogFragment");

                    sessionListDialogFragment.dismiss();

                }else{

                    response.handleResponseErrorCode(context , SessionListDialogFragmentVM.this);
                }

                animationViewVisibility.set(View.GONE);


            }
        } ,this );
    }
}
