package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableInt;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.ForgotPassword;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.activities.VerificationActivity;
import com.applexicon.thementorpartner.views.fragments.ForgotPasswordDialogFragment;
import com.applexicon.thementorpartner.databinding.FragmentDialogForgotPasswordBinding;

import java.util.List;

import vendor.Errors.MyEditText;
import vendor.Errors.ValidationError;
import vendor.Validation.Equal;
import vendor.Validation.IValidationHandler;
import vendor.Validation.IValidationRule;
import vendor.Validation.Mobile;
import vendor.Validation.MobileMax;
import vendor.Validation.Required;
import vendor.Validation.Validation;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class ForgotPasswordFragmentVM extends BaseObservable implements Response.ResponseErrorCodeHandler {


    private final FragmentDialogForgotPasswordBinding binding;
    private final ForgotPassword model;
    private final ForgotPasswordDialogFragment forgotPasswordDialogFragment;
    private String TAG  = "ForgotPassFragmentVM";
    private Context context;
    public final ObservableInt progressBar;




    public ForgotPasswordFragmentVM(@NonNull final ForgotPasswordDialogFragment forgotPasswordDialogFragment, FragmentDialogForgotPasswordBinding binding) {

        this.forgotPasswordDialogFragment = forgotPasswordDialogFragment;
        this.context = forgotPasswordDialogFragment.getActivity();
        this.binding = binding;
        progressBar = new ObservableInt(View.GONE);

         model = new ForgotPassword();

        this.binding.fragmentForgotPasswprdPhoneKeysSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                model.setPhoneKey( context.getResources().getStringArray(R.array.phone_keys)[i].replace("00","+"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                model.setPhoneKey(context.getResources().getStringArray(R.array.phone_keys)[0].replace("00","+"));
            }
        });
    }

    public void setPhone(final String phone){
//        Log.e(TAG , "setCode : " + phone);

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new MyEditText(binding.forgotPasswordPhoneET),
                new IValidationRule[]{
                        new MobileMax( phone,9 , context.getString(R.string.validation_mobile_equal))
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                model.setPhone(phone);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {

                onFieldValidationFailed(validationErrors);

            }
        });
    }

    public  String getPhone(){
        return  model.getPhone();
    }

    private void onFieldValidationFailed(List<ValidationError> validationErrors) {
//        this.validationErrors.clear();
//        this.validationErrors = validationErrors;

        Log.e(TAG , "onFieldValidationFailed : " + validationErrors.toString());
        for (ValidationError validationError : validationErrors) {
            validationError.displayError();
        }
    }

    public void requestForgotPassClick(View view){

        progressBar.set(View.VISIBLE);

        Log.e(TAG , "requestForgotPassClick");

        Validation validation = new Validation();

        validation.addValidationField(

                new MyEditText(binding.forgotPasswordPhoneET),
                new IValidationRule[]{
                        new Required( getPhone()),
                        new Mobile( getPhone()),
                        new Equal( getPhone(),9)
                });

        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                onRequestValidationSuccessfull();
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);
                progressBar.set(View.VISIBLE);
            }
        });
    }

    private void onRequestValidationSuccessfull() {

        Log.e(TAG , "onValidationSuccessfull");

        model.request(context , new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                Log.e(TAG , "Consumer<SignUp.SignUpDataResponse>()");

                onForgotPasswordResponseSuccessfull(response);
                progressBar.set(View.GONE);
            }
        }, this);
    }

    private void onForgotPasswordResponseSuccessfull(Response response) {
        Log.e(TAG , "onEditProfileResponseSuccessfull : " + response.toString());

        if(response.getResult()){
            Toast.makeText(context, response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
            //show new password dialog

            VerificationActivity.start(context , model.getPhoneKey() +  getPhone() , true);

            this.forgotPasswordDialogFragment.dismiss();

        }
        else{

            response.handleResponseErrorCode(context , this);
        }
        progressBar.set(View.GONE);
    }


    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        Toast.makeText(context, message , Toast.LENGTH_SHORT).show();
        progressBar.set(View.GONE);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        progressBar.set(View.GONE);
    }
}
