/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.PaymentHistoryFiltered;
import com.applexicon.thementorpartner.utilities.Constants;
import com.applexicon.thementorpartner.views.fragments.PaymentHistoryFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class ItemPaymentHistoryVM extends BaseObservable {

  private final PaymentHistoryFragment paymentHistoryFragment;
  private PaymentHistoryFiltered paymentHistory;
  private Context context;

  public ItemPaymentHistoryVM(PaymentHistoryFiltered paymentHistory, PaymentHistoryFragment historyFragment) {
    this.paymentHistory = paymentHistory;
    this.paymentHistoryFragment = historyFragment;
    context = historyFragment.getActivity();
  }

  public String getDate() {
    Date date = convertDateToLocal(paymentHistory.day);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.HOUR, 1);
    return convertLocalToDate(calendar.getTimeInMillis());
//    return paymentHistory.day + " "+paymentHistory.month;
  }

  public String getPrice() {
    return Constants.convertToArabic(Constants.distanceFormat.format(paymentHistory.amount));
  }

  public String getCurrency() {
    return "SAR";
  }

  public String getStudentName() {
    return paymentHistory.name;
  }

  public String getPaymentType() {
    switch (paymentHistory.method_type){
      case 0:
        return context.getString(R.string.cash_text);
      case 1:
        return context.getString(R.string.credit);
    }
    return context.getString(R.string.no_payment_type);
  }

  public String getSession() {
    return paymentHistory.bookingName;
  }

  public void setPaymentHistory(PaymentHistoryFiltered paymentHistory) {
    this.paymentHistory = paymentHistory;
    notifyChange();
  }

  private Date convertDateToLocal(String time) {
    DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    Date date1 = null;
    try {
      date1 = inputFormatter.parse(time);
      return date1;
    } catch (ParseException e) {
      e.printStackTrace();
    }

    return null;
  }

  private String convertLocalToDate(long millisecs) {
    Calendar calendar = Calendar.getInstance();
    DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    calendar.setTimeInMillis(millisecs);
    Date date1 = calendar.getTime();
    String dateStr = inputFormatter.format(date1);

    return dateStr;
  }

}
