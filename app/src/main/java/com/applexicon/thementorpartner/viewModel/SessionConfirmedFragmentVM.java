package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.ObservableField;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.ConfirmedSession;
import com.applexicon.thementorpartner.models.ConfirmedSessionFiltered;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.fragments.MyConfirmedSessionsFragment;
import com.applexicon.thementorpartner.databinding.FragmentMySessionsConfirmedBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class SessionConfirmedFragmentVM extends Observable implements Response.ResponseErrorCodeHandler {


    private final FragmentMySessionsConfirmedBinding binding;
    public final ObservableField<Integer> animationViewVisibility;
    public final ObservableField<Integer> messageVisibility;
    public   ObservableField<String> message;
    private final ArrayList<ConfirmedSessionFiltered> dataList;
    private String TAG  = "BookingCompletedFragVM";
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public SessionConfirmedFragmentVM(MyConfirmedSessionsFragment fragment) {
        this.context = fragment.getActivity();
        this.binding = fragment.binding;

        this.dataList = new ArrayList<>();

        animationViewVisibility = new ObservableField<>(View.GONE);
        messageVisibility = new ObservableField<>(View.GONE);

        message = new ObservableField<>();

        fetchSessionsConfirmed();
    }


    public void fetchSessionsConfirmed() {

        initializeViews();

        Mentor.getInstance().fetchSessionsConfirmed(context , new Consumer<Response<Mentor.SessionsConfirmedContentData>>(){

            @Override
            public void accept(Response<Mentor.SessionsConfirmedContentData> response) throws Exception {

                Log.e(TAG , "fetchRequestList response success " );
                if(response.getResult()){
                    if (response.getContent().getData().confirmedBookingList.size() > 0) {
                        List<ConfirmedSession> confirmedBookingsList = new ArrayList<>();
                        List<ConfirmedSessionFiltered> filteredconfirmedBookings = new ArrayList<>();
                        confirmedBookingsList = response.getContent().getData().confirmedBookingList;

                        for (int i = 0; i < confirmedBookingsList.size(); i++){
                            boolean isGroupIdExists = false;
                            for (int j = 0; j < filteredconfirmedBookings.size(); j++) {
                                if (confirmedBookingsList.get(i).groupId.equals(filteredconfirmedBookings.get(j).groupId)) {
                                    isGroupIdExists = true;
                                    filteredconfirmedBookings.get(j).date = filteredconfirmedBookings.get(j).date +
                                            "," + confirmedBookingsList.get(i).date;
                                    filteredconfirmedBookings.get(j).timeFrom = filteredconfirmedBookings.get(j).timeFrom +
                                            "," + confirmedBookingsList.get(i).timeFrom;
                                    filteredconfirmedBookings.get(j).timeTo = filteredconfirmedBookings.get(j).timeTo +
                                            "," + confirmedBookingsList.get(i).timeTo;
                                    filteredconfirmedBookings.get(j).id = filteredconfirmedBookings.get(j).id +
                                            "," + confirmedBookingsList.get(i).id;
                                    filteredconfirmedBookings.get(j).session_name = filteredconfirmedBookings.get(j).session_name +
                                            "," + confirmedBookingsList.get(i).session_name;
                                }
                            }

                            boolean isBookingIdExists = false;
                            String bookingId = "";
                            for (int j = 0; j < filteredconfirmedBookings.size(); j++) {
                                if (confirmedBookingsList.get(i).groupId.equals(filteredconfirmedBookings.get(j).groupId)) {
                                    bookingId = confirmedBookingsList.get(i).booking_id;
                                    isBookingIdExists = true;
                                }
                            }

                            if(!isGroupIdExists) {
                                ConfirmedSessionFiltered confirmedBookingFiltered = new ConfirmedSessionFiltered();
                                if(!isBookingIdExists) {
                                    confirmedBookingFiltered.bookingId = confirmedBookingsList.get(i).booking_id;
                                }
                                else {
                                    confirmedBookingFiltered.bookingId = bookingId;
                                }
                                confirmedBookingFiltered.id = confirmedBookingsList.get(i).id;
                                confirmedBookingFiltered.subjectName = confirmedBookingsList.get(i).subjectName;
                                confirmedBookingFiltered.groupId = confirmedBookingsList.get(i).groupId;
                                confirmedBookingFiltered.studentName = confirmedBookingsList.get(i).studentName;
                                confirmedBookingFiltered.studentGender = confirmedBookingsList.get(i).studentGender;
                                confirmedBookingFiltered.location = confirmedBookingsList.get(i).location;
                                confirmedBookingFiltered.date = confirmedBookingsList.get(i).date;
                                confirmedBookingFiltered.location = confirmedBookingsList.get(i).location;
                                confirmedBookingFiltered.date = confirmedBookingsList.get(i).date;
                                confirmedBookingFiltered.amount = confirmedBookingsList.get(i).amount;
                                confirmedBookingFiltered.currency = confirmedBookingsList.get(i).currency;
                                confirmedBookingFiltered.userName = confirmedBookingsList.get(i).userName;
                                confirmedBookingFiltered.paidAmount = confirmedBookingsList.get(i).paidAmount;
                                confirmedBookingFiltered.grade_name = confirmedBookingsList.get(i).grade_name;
                                confirmedBookingFiltered.level_name = confirmedBookingsList.get(i).level_name;
                                confirmedBookingFiltered.grade_name_in_ar = confirmedBookingsList.get(i).grade_name_in_ar;
                                confirmedBookingFiltered.level_name_in_ar = confirmedBookingsList.get(i).level_name_in_ar;
                                confirmedBookingFiltered.course_name_in_ar = confirmedBookingsList.get(i).course_name_in_ar;
                                confirmedBookingFiltered.distance_in_km = confirmedBookingsList.get(i).distance_in_km;
                                confirmedBookingFiltered.availabeForStart = confirmedBookingsList.get(i).availabeForStart;
                                confirmedBookingFiltered.timeFrom = confirmedBookingsList.get(i).timeFrom;
                                confirmedBookingFiltered.timeTo = confirmedBookingsList.get(i).timeTo;
                                confirmedBookingFiltered.sessionLongitude = confirmedBookingsList.get(i).sessionLongitude;
                                confirmedBookingFiltered.sessionLatitude = confirmedBookingsList.get(i).sessionLatitude;
                                confirmedBookingFiltered.session_type = confirmedBookingsList.get(i).session_type;
                                confirmedBookingFiltered.groupId = confirmedBookingsList.get(i).groupId;
                                confirmedBookingFiltered.session_location = confirmedBookingsList.get(i).session_location;
                                confirmedBookingFiltered.session_from_time = confirmedBookingsList.get(i).session_from_time;
                                confirmedBookingFiltered.session_to_time = confirmedBookingsList.get(i).session_to_time;
                                confirmedBookingFiltered.teacherLatitude = confirmedBookingsList.get(i).teacherLatitude;
                                confirmedBookingFiltered.teacherLongitude = confirmedBookingsList.get(i).teacherLongitude;
                                confirmedBookingFiltered.studentPhone = confirmedBookingsList.get(i).studentPhone;
                                confirmedBookingFiltered.booking_name = confirmedBookingsList.get(i).booking_name;
                                confirmedBookingFiltered.session_name = confirmedBookingsList.get(i).session_name;
                                confirmedBookingFiltered.payment_type = confirmedBookingsList.get(i).payment_type;
                                filteredconfirmedBookings.add(confirmedBookingFiltered);
                            }
                        }

                        changeDataSet(filteredconfirmedBookings);
                        messageVisibility.set(View.GONE);
                    }
                    else{
                        //not sessions found
                        setMessage(context.getString(R.string.no_session_confirmed));
                    }

                }else{

                    response.handleResponseErrorCode(context , SessionConfirmedFragmentVM.this);
                }

                animationViewVisibility.set(View.GONE);


            }
        } ,this );
    }


    public void initializeViews() {
        animationViewVisibility.set(View.VISIBLE);
        messageVisibility.set(View.GONE);
    }


    private void changeDataSet(List<ConfirmedSessionFiltered> list) {
        dataList.clear();
        dataList.addAll(list);
        setChanged();
        notifyObservers();
    }

    public List<ConfirmedSessionFiltered> getList() {
        return dataList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }



    public void setMessage(String message) {
        animationViewVisibility.set(View.GONE);
        messageVisibility.set(View.VISIBLE);
        this.message.set(message);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {

        animationViewVisibility.set(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        setMessage(message);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        animationViewVisibility.set(View.GONE);
    }
}
