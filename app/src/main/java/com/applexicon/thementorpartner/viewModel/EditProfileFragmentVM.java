package com.applexicon.thementorpartner.viewModel;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.EditProfile;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.response.ResponseValidationErrors;
import com.applexicon.thementorpartner.utilities.FileUtils;
import com.applexicon.thementorpartner.utilities.ImageLoading;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.views.adapters.GooglePlacesAutocompleteAdapter;
import com.applexicon.thementorpartner.views.fragments.ChangePasswordDialogFragment;
import com.applexicon.thementorpartner.views.fragments.EditProfileFragment;
import com.applexicon.thementorpartner.databinding.FragmentEditProfileBinding;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

import vendor.Errors.IVewError;
import vendor.Errors.MyEditText;
import vendor.Errors.TextInput;
import vendor.Errors.ValidationError;
import vendor.Validation.Email;
import vendor.Validation.IValidationHandler;
import vendor.Validation.IValidationRule;
import vendor.Validation.Max;
import vendor.Validation.Required;
import vendor.Validation.Validation;

import static com.applexicon.thementorpartner.viewModel.SignUpFragmentViewModel.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class EditProfileFragmentVM extends Observable
        implements
        IValidationHandler,
        Response.ResponseErrorCodeHandler,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {


    public static final int PROFILE_IMAGE_SELECT_CODE = 1;
    public  EditProfile editProfile;

    private  FragmentEditProfileBinding binding;
    private  EditProfileFragment editProfileFragment;


    public ObservableField<String> message;
    public String TAG  = "SignInFragmentViewModel";


    private Context context;
    public GoogleApiClient mGoogleApiClient;

    public ObservableInt  animationViewVisibility;
    public ObservableInt  messageViewVisibility;
    public ObservableInt  progressBar;
    public static boolean isEditing = false;
    private View view;
    private String apiKey = "AIzaSyDZZNRKAws3SM17bZHrBYLRGR6Vf0VSfpU";


    public EditProfileFragmentVM(@NonNull EditProfileFragment editProfileFragment) {

        this.editProfileFragment = editProfileFragment;

        this.context = this.editProfileFragment.getActivity();

        animationViewVisibility = new ObservableInt(View.GONE);
        messageViewVisibility = new ObservableInt(View.GONE);
        progressBar = new ObservableInt(View.GONE);
        message = new ObservableField<>();

        this.binding = editProfileFragment.binding;

        editProfile = new EditProfile();

        editProfile.setFullname(Mentor.getInstance().name);
        editProfile.setEmail(Mentor.getInstance().email);
        editProfile.setAddress(Mentor.getInstance().address);


        animationViewVisibility = new ObservableInt(View.GONE);


        actionListner();

        setUpGoogleAPIClient();

        setUpAutoComplete();

        if (!Places.isInitialized()) {
            // Initialize Places.
            Places.initialize(context, apiKey);
        }

        ImageLoading.loadImage(editProfileFragment.binding.activityEditProfileUploadImageCiv ,
                Mentor.getInstance().imageUrl ,
                R.drawable.avatar,
                progressBar);
    }

    private void setUpAutoComplete() {
       final GooglePlacesAutocompleteAdapter adapter = new GooglePlacesAutocompleteAdapter(context ,  R.layout.item_place_layout);

        binding.fragmentEditProfileAutoComplete.setAdapter(adapter);

        binding.fragmentEditProfileAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mGoogleApiClient.connect();


                if( adapter.getList().size() > 0){
                    String mId = adapter.getList().get(position).getId();
                    if (adapter.getList().get(position).getName().length() > 38)
                        binding.fragmentEditProfileAutoComplete.setText(adapter.getList().get(position).getName().substring(0, 38));
                    try {
                        // Define a Place ID.
                        String placeId = mId;

                        // Specify the fields to return.
                        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME,
                                Place.Field.LAT_LNG);

                        // Construct a request object, passing the place ID and fields array.
                        FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields)
                                .build();

                        // Create a new Places client instance.
                        PlacesClient placesClient = Places.createClient(context);

                        placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
                            Place mPlace = response.getPlace();

                            LatLng qLoc = mPlace.getLatLng();
                            editProfile.setAddress(mPlace.getName().toString());
                            editProfile.setLatitude(qLoc.latitude);
                            editProfile.setLongitude(qLoc.longitude);

                        }).addOnFailureListener((exception) -> {
                            if (exception instanceof ApiException) {
                                ApiException apiException = (ApiException) exception;
                                int statusCode = apiException.getStatusCode();
                                // Handle error with given status code.
                                Log.e(TAG, "Place not found: " + exception.getMessage());
                            }
                        });
                    } catch (Exception e) {
                    }
                }
            }
        });

    }

    private void setUpGoogleAPIClient() {
        if(mGoogleApiClient == null) {

            mGoogleApiClient = new GoogleApiClient
                    .Builder(context)
//                    .addApi(Places.GEO_DATA_API)
//                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(editProfileFragment.getActivity(), this)
                    .addConnectionCallbacks(this)
                    .build();
        }
    }


    private void actionListner() {

        this.binding.fragmentEditProfileFullnameEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    EditText et = (EditText)v ;
                    validateFullnameOnBlur( et);
                }
            }
        });

        this.binding.fragmentEditProfileEmailEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    EditText et = (EditText)v ;
                    validateEmailOnBlur( et);
                }
            }
        });


    }

    private void validateFullnameOnBlur(EditText et) {

        Log.e(TAG , "validateFullnameOnBlur");
        final String fullname = et.getText().toString();

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new TextInput(binding.fragmentSignUpFullnameTil),
                new IValidationRule[]{
                        new Required( fullname),
                });



        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                editProfile.setFullname(fullname);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);

            }
        });
    }

    private void validateEmailOnBlur(EditText et) {
        Log.e(TAG , "validateEmailOnBlur");
        final String email = et.getText().toString().toLowerCase();

        Validation validation = new Validation();

        validation.addValidationField(

                new TextInput(binding.fragmentEditProfileEmailTil),
                new IValidationRule[]{
                        new Required(   email),
                });



        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                editProfile.setEmail(email);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);

            }
        });
    }


    public void setFullname(final String fullname) {

        Validation validation = new Validation();

        validation.addValidationField(

                new TextInput(binding.fragmentSignUpFullnameTil),
                new IValidationRule[]{
                        new Max(fullname, 20 , context.getString(R.string.validation_equal))
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                editProfile.setFullname(fullname);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {

                onFieldValidationFailed(validationErrors);

            }
        });

    }

    public void setEmail(String email) {
       editProfile.setEmail(email);
    }


    public String getFullname(){
        return  editProfile.getFullname();
    }
    public String getEmail(){
        return  editProfile.getEmail();
    }
    public String getAddress(){
        return  editProfile.getAddress();
    }


    private void onFieldValidationFailed(List<ValidationError> validationErrors) {
//        this.validationErrors.clear();
//        this.validationErrors = validationErrors;

        Log.e(TAG , "onFieldValidationFailed : " + validationErrors.toString());
        for (ValidationError validationError : validationErrors) {
            validationError.displayError();
        }
    }

    private void validateInputs() {
        Log.e(TAG , "validateInputs");
        final String fullname = binding.fragmentEditProfileFullnameEt.getText().toString();
        final String email = binding.fragmentEditProfileEmailEt.getText().toString();

        Validation validation = new Validation();

        validation.addValidationField(

                new TextInput(binding.fragmentSignUpFullnameTil),
                new IValidationRule[]{
                        new Required( fullname),
                       new Max( fullname,20)
                });

        validation.addValidationField(

                new TextInput(binding.fragmentEditProfileEmailTil),
                new IValidationRule[]{
                        new Required( email),
                        new Email( email)
                });



        validation.addValidationField(

                new MyEditText(binding.fragmentEditProfileAutoComplete),
                new IValidationRule[]{
                        new Required( editProfile.getAddress()),
                });



        validation.validate(context, this);
    }


//    public void onEditProfileResponseSuccessfull(Response<EditProfile.EditProfileDataResponse> response) {
//        Log.e(TAG , "onEditProfileResponseSuccessfull : " + response.toString());
//
//        if(response.getResult()){
//            enableView(view , true);
//            ((MainActivity)editProfileFragment.getActivity()).replaceFragment(ProfileFragment.newInstance());
//
//            if(!response.getContent().getData().profile_pic.equals("")) {
//                Mentor.getInstance().imageUrl = response.getContent().getData().profile_pic;
//                SharedPref.putString(context, Mentor.USER_PREF_IMAGE_URL, response.getContent().getData().profile_pic);
//                ((MainActivity)this.editProfileFragment.getActivity()).displayUserImage();
//            }
//        }
//        else{
//            if(response.getErrorCode() == ResponseErrorCode.INVALID_OR_MISSING_PARAMETERS ) {
//                onValidationFailed(getValidationErrors(response.getContent().getErrors()));
//            }else
//                response.handleResponseErrorCode(context ,this);
//
//        }
//
//       animationViewVisibility.set(View.GONE);
//    }

    @Override
    public void onValidationSuccessfull() {
        Log.e(TAG , "onValidationSuccessfull");

//        editProfile.edit(context ,new Consumer<Response<EditProfile.EditProfileDataResponse>>() {
//            @Override
//            public void accept(Response<EditProfile.EditProfileDataResponse> response) throws Exception {
//                Log.e(TAG , "Consumer<SignUp.SignUpDataResponse>()");
//                onEditProfileResponseSuccessfull(response);
//                animationViewVisibility.set(View.GONE);
//            }
//        }, this);
    }

    @Override
    public void onValidationFailed(List<ValidationError> validationErrors) {
        Log.e(TAG , "onValidationFailed");

        onFieldValidationFailed(validationErrors);
       animationViewVisibility.set(View.GONE);
        enableView(view , true);
    }




    public void setMessage(String message) {
        animationViewVisibility.set(View.GONE);
        messageViewVisibility.set(View.VISIBLE);
        this.message.set( message);

    }

    public List<ValidationError> getValidationErrors(ResponseValidationErrors signUpErrors) {

         List<ValidationError> validationErrors = new ArrayList<>();
        addVadationErrors(validationErrors ,signUpErrors.emailErrors , new TextInput(binding.fragmentEditProfileEmailTil));
        addVadationErrors(validationErrors , signUpErrors.nameErrors , new TextInput(binding.fragmentSignUpFullnameTil));
        return validationErrors;
    }

    private void addVadationErrors( List<ValidationError> validationErrors ,  List<String> errors , IVewError iVewError) {

        if(errors != null) {
            for (String error : errors) {
                validationErrors.add(new ValidationError(error, iVewError));
            }
        }

        animationViewVisibility.set(View.VISIBLE);

    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        animationViewVisibility.set(View.GONE);
        setMessage(message);
        enableView(view , true);

    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "onEditProfileResponseSuccessfull : " + message);
        animationViewVisibility.set(View.GONE);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG , "Google API client onConnectionFailed");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG , "Google API client onConnected");

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG , "Google API client onConnectionSuspended");

    }




    public void onEditClick(View view){
        Log.e(TAG , "onEditClick");

        this.view = view;
        enableView(view , false);
        animationViewVisibility.set(View.VISIBLE);
        messageViewVisibility = new ObservableInt(View.GONE);
        validateInputs();


    }

    public void onUploadImageClick(View v){
        Log.e(TAG , "onUploadIdClick");



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (editProfileFragment.getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                editProfileFragment.getActivity().requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                // app-defined int constant that should be quite unique
            }
            else{
                openFileChooser();
            }
        }
        else{
            openFileChooser();
        }

    }

    public void oneChangePasswordClick(View view){
        Log.e(TAG , "onEditClick");

        ChangePasswordDialogFragment.newInstance().show(editProfileFragment.getChildFragmentManager() , "ChangePasswordDialogFragment");
    }


    public void openFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            ((MainActivity)context).startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    PROFILE_IMAGE_SELECT_CODE);
        } catch (ActivityNotFoundException ex) {
            // Potentially direct the mentor to the Market with a Dialog
            Toast.makeText(context, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void setImageProfileFile(Intent data) {
        Uri uri = data.getData();
        Log.e(TAG, "File Uri: " + uri.toString());
        String path = FileUtils.getPath(context , uri);
        Log.e(TAG, "File Path: " + path);

        File file = new File(path);

        editProfile.setImageProfileFile(file);

        displayProfileImage(uri);
    }

    private void displayProfileImage(Uri uri) {

        try {
            Bitmap bitmapImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            Bitmap.createScaledBitmap(bitmapImage, 600, 600, false);
            binding.activityEditProfileUploadImageCiv.setImageBitmap(bitmapImage);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG , "displayProfileImage : " + e.getMessage());
        }

    }

    private void enableView(View view , boolean enable) {

        if(enable){
            isEditing = false;
            editProfileFragment.getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
        else {
            isEditing = true;
            editProfileFragment.getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        animationViewVisibility.set(enable ? View.GONE : View.VISIBLE);
        binding.getRoot().setOnTouchListener(null);
        view.setAlpha(enable ? 1f : 0.5f);
    }


}
