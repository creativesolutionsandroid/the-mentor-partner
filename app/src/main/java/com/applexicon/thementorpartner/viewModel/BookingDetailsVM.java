/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.SessionRequestModel;
import com.applexicon.thementorpartner.views.fragments.BookingDetailsDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;


public class BookingDetailsVM extends BaseObservable {

    private SessionRequestModel sessionRequest;
    private Context context;
    private String TAG;

    public BookingDetailsVM(BookingDetailsDialogFragment bookingDetailsDialogFragment) {
        this.sessionRequest = bookingDetailsDialogFragment.sessionRequest;
        this.context = bookingDetailsDialogFragment.getActivity();
        bookingDetailsDialogFragment.binding.fragmentBookingDetailsDialogSessionDateValueTv.setMovementMethod(new ScrollingMovementMethod());
    }

    public String getStudentName() {
        return sessionRequest.studentName;
    }

    public String getSessionLocation() {
        return sessionRequest.sessionLocation;
    }

//  public String getTimeFrom() {
//    return sessionRequest.timeFrom;
//  }
//
//  public String getTimeTo() {
//    return sessionRequest.timeTo;
//  }

    public String getDate() {

//      List<SessionsTime> times = new ArrayList<>();
//
//      SessionsTime  sessionsTime = new SessionsTime();
//      sessionsTime.setSession_date("2017-12-12");
//      sessionsTime.setTime_from("1:00 pm");
//      sessionsTime.setTime_to("1:30 pm");
//
//      SessionsTime  sessionsTime1 = new SessionsTime();
//      sessionsTime1.setSession_date("2017-12-13");
//      sessionsTime1.setTime_from("2:00 pm");
//      sessionsTime1.setTime_to("3:00 pm");
//
//      times.add(sessionsTime);
//      times.add(sessionsTime1);

        Collections.reverse(sessionRequest.sessions);
        String mess = "";
        for (Session model : sessionRequest.sessions) {
            mess += getSessionTime(model) + "\n\n";
        }

        return mess;
    }

    private String getSessionTime(Session time) {
        String message = getData(time.date) + "\n";

        message += time.timeFrom + " -> " + time.timeTo;

        return message;
    }

    private String getData(String deliverydate) {
        Log.e(TAG, "deliverydate:" + deliverydate);

        SimpleDateFormat incoming = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = null;
        try {
            currentDate = (Date) incoming.parse(deliverydate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat converted = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());

        Log.e(TAG, "format(date):" + converted.format(currentDate));

        return converted.format(currentDate);
    }

    public String getSchedule() {
//    return sessionRequest.schedule;

        String schedule = "";
        switch (sessionRequest.schedule) {
            case "0":
                schedule = context.getString(R.string.once);
                break;

            case "1":
                schedule = context.getString(R.string.monthly);
                break;

        }

        return schedule;
    }

    public void btnClicked(View view) {
        Log.e(TAG, "btnClicked");
    }

    public void refuseBtnClick(View view) {
        Log.e(TAG, "refuseBtnClick");

    }

}
