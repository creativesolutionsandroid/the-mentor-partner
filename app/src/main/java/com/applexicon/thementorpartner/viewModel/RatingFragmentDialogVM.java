package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.databinding.FragmentSessionRateDialogBinding;
import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.fragments.RatingDialogFragment;

import io.reactivex.functions.Consumer;

/**
 * Created by AppleXIcon on 6/9/2017.
 */

public class RatingFragmentDialogVM implements Response.ResponseErrorCodeHandler {

    private final Session session;
    private final String teacherImageUrl;
    Context context;
    FragmentSessionRateDialogBinding binding;
    RatingDialogFragment dialogFragment;
    private String TAG = "CongratsFragmDialogVM";
//    private String comment;


    public RatingFragmentDialogVM(RatingDialogFragment dialogFragment) {
        this.context = dialogFragment.getActivity();
        this.binding = dialogFragment.binding;
        this.teacherImageUrl = dialogFragment.teacherImageUrl;
        this.dialogFragment = dialogFragment;

//        Log.e(TAG , "image url : " + teacherImageUrl);
//        String fullUrl = "public/" + teacherImageUrl;
//        ImageLoading.loadImage(binding.uploadImageCiv,
//                fullUrl,
//                R.drawable.avatar,
//                binding.imageLoading);

        session = new Session();
        session.id = dialogFragment.sessionId;
        Log.d(TAG, "RatingFragmentDialogVM: "+session.id);
    }

    public void onNoThanksClicked(View view) {
        Log.e(TAG , "onNoThanksClicked");
        dialogFragment.dismiss();
//        dialogFragment.getActivity().finish();
    }

    public void  setComment(String comment){
        Log.e(TAG , "setComment");

        session.comment = comment.trim();

    }

    public String getComment(){
        return session.comment;

    }

    public void onSendClicked(View view) {

        Log.e(TAG , "onSendClicked");

        float rating =  binding.myBookingsRateMentorRateBar.getRating();

        session.rate = rating;
        Log.e(TAG , "comment : " + session.comment  + " rate : " + rating);

        if(rating > 0.0){
            session.rateStudent(context, new Consumer<Response>() {
                @Override
                public void accept(Response response) throws Exception {

                    if(response.getResult()){
                        dialogFragment.dismiss();
//                        dialogFragment.getActivity().finish();
                    }

                    handleResponseErrorCodeForUser(response.getContent().getMessage());
                }
            } , this);
        }else
            handleResponseErrorCodeForUser(context.getString(R.string.you_must_rate_teacher));

    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);

    }
}
