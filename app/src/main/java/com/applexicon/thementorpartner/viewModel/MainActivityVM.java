package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;

import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.databinding.ActivityMainBinding;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class MainActivityVM extends BaseObservable  {


    private final ActivityMainBinding binding;
    private String TAG  = "MainActivityVM";
    private Context context;




    public MainActivityVM(MainActivity mainActivity) {
        this.context = mainActivity.getApplicationContext();
        this.binding = mainActivity.mainBinding;

    }



}
