package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.ObservableField;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.applexicon.thementorpartner.models.CanceledSession;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.fragments.MyCanceledSessionsFragment;
import com.applexicon.thementorpartner.databinding.FragmentMySessionsCanceledBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class SessionCanceledFragmentVM extends Observable implements Response.ResponseErrorCodeHandler {

    private final FragmentMySessionsCanceledBinding binding;
    public final ObservableField<Integer> animationViewVisibility;
    public final ObservableField<Integer> messageVisibility;
    public ObservableField<String> message;
    private final ArrayList<CanceledSession> dataList;
    private String TAG = "VerificationVM";
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    public SessionCanceledFragmentVM(MyCanceledSessionsFragment myCanceledSessionsFragment) {
        this.context = myCanceledSessionsFragment.getActivity();
        this.binding = myCanceledSessionsFragment.binding;

        this.dataList = new ArrayList<>();

        animationViewVisibility = new ObservableField<>(View.GONE);
        messageVisibility = new ObservableField<>(View.GONE);

        message = new ObservableField<>();



    }

    public void fetchSessionsCanceled() {

        initializeViews();

        Mentor.getInstance().fetchSessionsCanceled(context, new Consumer<Response<Mentor.SessionsCanceledContentData>>() {

            @Override
            public void accept(Response<Mentor.SessionsCanceledContentData> response) throws Exception {

                Log.e(TAG, "fetchRequestList response success ");
                if (response.getResult()) {
                    if (response.getContent().getData().list.size() > 0) {
                        changeDataSet(response.getContent().getData().list);
                        messageVisibility.set(View.GONE);
                    } else {
                        //not sessions found
                        setMessage(context.getString(R.string.no_session_Canceled));
                    }

                } else {

                    response.handleResponseErrorCode(context, SessionCanceledFragmentVM.this);
                }

                animationViewVisibility.set(View.GONE);


            }
        }, this);
    }


    public void initializeViews() {
        animationViewVisibility.set(View.VISIBLE);
        messageVisibility.set(View.GONE);
    }


    private void changeDataSet(List<CanceledSession> list) {
        dataList.clear();
        dataList.addAll(list);
        setChanged();
        notifyObservers();
    }

    public List<CanceledSession> getSessionsleList() {
        return dataList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }


    public void setMessage(String message) {
        animationViewVisibility.set(View.GONE);
        messageVisibility.set(View.VISIBLE);
        this.message.set(message);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {

        animationViewVisibility.set(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        setMessage(message);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG, "handleResponseErrorCodeForDebug : " + message);
        animationViewVisibility.set(View.GONE);
    }
}
