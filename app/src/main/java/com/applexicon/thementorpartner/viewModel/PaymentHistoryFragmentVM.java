package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.ObservableField;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.models.PaymentHistory;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.PaymentHistoryFiltered;
import com.applexicon.thementorpartner.network.ApiClient;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.applexicon.thementorpartner.views.fragments.PaymentHistoryFragment;
import com.applexicon.thementorpartner.databinding.FragmentPaymentHistoryBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class PaymentHistoryFragmentVM extends Observable implements Response.ResponseErrorCodeHandler {

    private final FragmentPaymentHistoryBinding binding;
    public final ObservableField<Integer> animationViewVisibility;
    public final ObservableField<Integer> messageVisibility;
    public   ObservableField<String> message;
    private final ArrayList<PaymentHistoryFiltered> paymentHistoryList;
    private String TAG  = "VerificationVM";
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public PaymentHistoryFragmentVM(PaymentHistoryFragment paymentHistoryFragment) {
        this.context = paymentHistoryFragment.getActivity();
        this.binding = paymentHistoryFragment.binding;

        this.paymentHistoryList = new ArrayList<>();

        animationViewVisibility = new ObservableField<>(View.GONE);
        messageVisibility = new ObservableField<>(View.GONE);

        message = new ObservableField<>();

        fetchPaymentHistory();
    }

    private void fetchPaymentHistory() {
        initializeViews();

        PaymentHistory.APIInterface apiService =
                ApiClient.getClient(context).create(PaymentHistory.APIInterface.class);
        Call<PaymentHistory> call = apiService.fetchPaymentHistory(Mentor.getInstance().token);

        call.enqueue(new Callback<PaymentHistory>() {
            @Override
            public void onResponse(Call<PaymentHistory> call, retrofit2.Response<PaymentHistory> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: " + response);
                    PaymentHistory verifyMobileResponse = response.body();
                    try {
                        if (verifyMobileResponse.getResult()) {
                            if(verifyMobileResponse.getContent().getData().getPaymentHistory().getPayments().size() > 0) {
                                List<PaymentHistory.Payments> paymentHistoryList = new ArrayList<>();
                                List<PaymentHistoryFiltered> filteredPaymentHistoryList = new ArrayList<>();
                                paymentHistoryList = verifyMobileResponse.getContent().getData().getPaymentHistory().getPayments();
                                for (int i = 0; i < paymentHistoryList.size(); i++) {
                                    boolean isGroupIdExists = false;
                                    for (int j = 0; j < filteredPaymentHistoryList.size(); j++) {
                                        if (paymentHistoryList.get(i).bookingName.equals(filteredPaymentHistoryList.get(j).bookingName)
                                                && (!paymentHistoryList.get(i).sessionName.equals(filteredPaymentHistoryList.get(j).sessionName))) {
                                            isGroupIdExists = true;
                                        }
                                    }
                                    if (!isGroupIdExists) {
                                        PaymentHistoryFiltered paymentHistoryFiltered = new PaymentHistoryFiltered();
                                        paymentHistoryFiltered.name = paymentHistoryList.get(i).name;
                                        paymentHistoryFiltered.amount = paymentHistoryList.get(i).amount;
                                        paymentHistoryFiltered.method_type = paymentHistoryList.get(i).method_type;
                                        paymentHistoryFiltered.currency = paymentHistoryList.get(i).currency;
                                        paymentHistoryFiltered.month = paymentHistoryList.get(i).month;
                                        paymentHistoryFiltered.day = paymentHistoryList.get(i).getUpdated_at();
                                        paymentHistoryFiltered.bookingName = paymentHistoryList.get(i).bookingName;
                                        paymentHistoryFiltered.sessionName = paymentHistoryList.get(i).sessionName;
                                        paymentHistoryFiltered.sessionId = paymentHistoryList.get(i).sessionId;
                                        filteredPaymentHistoryList.add(paymentHistoryFiltered);
                                    }
                                }
                                setPaymentsData(verifyMobileResponse.getContent().getData().getPaymentHistory().getTotalPayments());
                                changePeopleDataSet(filteredPaymentHistoryList);
                                messageVisibility.set(View.GONE);
                            }
                            else{
//                        //not sessions found
                                displayMessage(context.getString(R.string.no_payment_history_found));
                                binding.paymentTypeLayout.setVisibility(View.GONE);
                                binding.upcomingLayout.setVisibility(View.GONE);
                                binding.view1.setVisibility(View.GONE);
                            }
                        } else {
//                                                      status false case
//                            String failureResponse = response.body().getContent().getMessage();
//                            Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                            displayMessage(context.getString(R.string.no_payment_history_found));
                            binding.paymentTypeLayout.setVisibility(View.GONE);
                            binding.upcomingLayout.setVisibility(View.GONE);
                            binding.view1.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        displayMessage(context.getString(R.string.no_payment_history_found));
                        binding.paymentTypeLayout.setVisibility(View.GONE);
                        binding.upcomingLayout.setVisibility(View.GONE);
                        binding.view1.setVisibility(View.GONE);
//                        Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    displayMessage(context.getString(R.string.no_payment_history_found));
                    binding.paymentTypeLayout.setVisibility(View.GONE);
                    binding.upcomingLayout.setVisibility(View.GONE);
                    binding.view1.setVisibility(View.GONE);
//                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                animationViewVisibility.set(View.GONE);
            }

            @Override
            public void onFailure(Call<PaymentHistory> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.toString());
                animationViewVisibility.set(View.GONE);
                displayMessage(context.getString(R.string.no_payment_history_found));
                binding.paymentTypeLayout.setVisibility(View.GONE);
                binding.upcomingLayout.setVisibility(View.GONE);
                binding.view1.setVisibility(View.GONE);
            }
        });

//        Mentor.getInstance().fetchPaymentHistory(context , new Consumer<Response<Mentor.FetchPaymentHisContentData>>(){
//
//            @Override
//            public void accept(Response<Mentor.FetchPaymentHisContentData> response) throws Exception {
//
//                Log.e(TAG , "fetchRequestList response success " );
//                if(response.getResult()){
//                    if(response.getContent().getData().paymentHistoryList.size() > 0){
//                        List<PaymentHistory> paymentHistoryList = new ArrayList<>();
//                        List<PaymentHistoryFiltered> filteredPaymentHistoryList = new ArrayList<>();
//                        paymentHistoryList = response.getContent().getData().paymentHistoryList;
//                        for (int i = 0; i < paymentHistoryList.size(); i++) {
//                            boolean isGroupIdExists = false;
//                            for (int j = 0; j < filteredPaymentHistoryList.size(); j++) {
//                                if (paymentHistoryList.get(i).bookingName.equals(filteredPaymentHistoryList.get(j).bookingName)) {
//                                    isGroupIdExists = true;
//                                }
//                            }
//                            if (!isGroupIdExists) {
//                                PaymentHistoryFiltered paymentHistoryFiltered = new PaymentHistoryFiltered();
//                                paymentHistoryFiltered.name = paymentHistoryList.get(i).name;
//                                paymentHistoryFiltered.amount = paymentHistoryList.get(i).amount;
//                                paymentHistoryFiltered.method_type = paymentHistoryList.get(i).method_type;
//                                paymentHistoryFiltered.currency = paymentHistoryList.get(i).currency;
//                                paymentHistoryFiltered.month = paymentHistoryList.get(i).month;
//                                paymentHistoryFiltered.day = paymentHistoryList.get(i).day;
//                                paymentHistoryFiltered.bookingName = paymentHistoryList.get(i).bookingName;
//                                paymentHistoryFiltered.sessionName = paymentHistoryList.get(i).sessionName;
//                                paymentHistoryFiltered.sessionId = paymentHistoryList.get(i).sessionId;
//                                filteredPaymentHistoryList.add(paymentHistoryFiltered);
//                            }
//                        }
//                        changePeopleDataSet(filteredPaymentHistoryList);
//                        animationViewVisibility.set(View.GONE);
//                        messageVisibility.set(View.GONE);
//                    }
//                    else{
//                        //not sessions found
//                        displayMessage(context.getString(R.string.no_payment_history_found));
//                    }
//
//                }else{
//
//                    response.handleResponseErrorCode(context , PaymentHistoryFragmentVM.this);
//                }
//
//            }
//        } ,this );
    }


    public void initializeViews() {
        animationViewVisibility.set(View.VISIBLE);
        messageVisibility.set(View.GONE);
    }



    private void changePeopleDataSet(List<PaymentHistoryFiltered> PaymentHistoryList) {
        paymentHistoryList.addAll(PaymentHistoryList);
        setChanged();
        notifyObservers();
    }

    public List<PaymentHistoryFiltered> getSessionsleList() {
        return paymentHistoryList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }



    public void displayMessage(String message) {
        animationViewVisibility.set(View.GONE);
        messageVisibility.set(View.VISIBLE);
        this.message.set(message);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {

        animationViewVisibility.set(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        displayMessage(message);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        animationViewVisibility.set(View.GONE);
    }

    private void setPaymentsData(PaymentHistory.TotalPayments payments){
        binding.cash.setText(Constants.convertToArabic(Constants.distanceFormat.format(payments.getPaid_cash_amount())));
        binding.card.setText(Constants.convertToArabic(Constants.distanceFormat.format(payments.getPaid_card_amount())));
        binding.upcomingCash.setText(Constants.convertToArabic(Constants.distanceFormat.format(payments.getPending_cash_amount())));
        binding.upcomingCard.setText(Constants.convertToArabic(Constants.distanceFormat.format(payments.getPending_card_amount())));
    }
}
