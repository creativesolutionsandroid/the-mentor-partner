/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.fragments.PaymentRequestDialogFragment;

import io.reactivex.functions.Consumer;


public class PaymentRequestVM extends BaseObservable implements Response.ResponseErrorCodeHandler {


  private final PaymentRequestDialogFragment fragment;
  private final Session session;
  private Context context;
  private String TAG = "PaymentRequestVM";
  public String alertMessage;
  public static boolean isEditing = false;
  private View view;
    public PaymentRequestVM(PaymentRequestDialogFragment fragment) {
      this.fragment = fragment;
      this.context = fragment.getActivity();
      this.session = fragment.session;
      alertMessage = String.format( context.getString(R.string.session_start_alert), session.id );
    }

  public void onPaymentRequestClicked(View view){
    Log.e(TAG , "onStartClicked");

    this.view = view;
    disableDialog();

    session.paymentRequest(context, new Consumer<Response>() {
      @Override
      public void accept(Response response) throws Exception {
        Log.e(TAG , "paymentRequest accept successful");

//        if(response.getResult()){
//          cancelAlarm(context);
//        }

        Toast.makeText(context, response.getContent().getMessage(), Toast.LENGTH_SHORT).show();

//        session.setEndSessionAlarm(context);
        enableDialog();
        fragment.dismiss();
      }
    } , this);


  }

  private void disableDialog() {
    enableView(view, false);
    fragment.binding.fragmentBookingDetailsDialogRejectBtn.setEnabled(false);
    fragment.binding.fragmentBookingDetailsDialogRejectBtn.setAlpha(0.5f);
  }

  private void enableDialog() {
    enableView(view, true);
    fragment.binding.fragmentBookingDetailsDialogRejectBtn.setEnabled(true);
    fragment.binding.fragmentBookingDetailsDialogRejectBtn.setAlpha(1);
  }

  private void enableView(View view, boolean enable) {

    if (enable) {
      isEditing = false;
      fragment.getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    } else {
      isEditing = true;
      fragment.getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
              WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    fragment.binding.getRoot().setOnTouchListener(null);
    view.setAlpha(enable ? 1f : 0.5f);
  }

//  public void cancelAlarm(Context context)
//  {
//    Intent i = new Intent(context, StartSessionAlarmBroadCast.class);
//    i.setAction(Session.START_SESSION_ACTION);
//    PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
//
//    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//    alarmManager.cancel(pi);
//  }


  @Override
  public void handleResponseErrorCodeForUser(String message) {
    Log.e(TAG , "handleResponseErrorCodeForUser");
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    fragment.dismiss();
  }

  @Override
  public void handleResponseErrorCodeForDebug(String message) {
    Log.e(TAG , "handleResponseErrorCodeForDebug");

  }
}
