/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import androidx.databinding.BaseObservable;
import android.util.Log;
import android.view.View;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.CompletedBooking;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.views.fragments.MyCompletedSessionsFragment;
import com.applexicon.thementorpartner.databinding.MySessionsCompletedItemBinding;
import com.applexicon.thementorpartner.views.fragments.RatingDialogFragment;


public class ItemSessionCompletedVM extends BaseObservable {

  private final MyCompletedSessionsFragment fragment;
  private final MySessionsCompletedItemBinding binding;
  private CompletedBooking model;
  private String language = "en";

  public ItemSessionCompletedVM(CompletedBooking model, MyCompletedSessionsFragment fragment, MySessionsCompletedItemBinding itemBinding) {
    this.model = model;
    this.fragment = fragment;
    this.binding = itemBinding;
    language = LanguageUtils.getUsedLang(fragment.getActivity());

    binding.myBookingsCompletedItemMentorRateBar.setRating(model.teacher_rate);
  }

  public String getSessionId() {
    return "Session"+model.id;
  }

  public String getSubjectName() {
    String name = "";
    if (language.equalsIgnoreCase("en")) {
      name = model.subjectName;
    }
    else {
      name = model.subjectNameInUrdu;
    }
    return name;
  }

  public String getLocation() {
    return model.location;
  }

  public String getStudentName() {
    return model.studentName;
  }

  public String getId() {
    return model.session_name;
  }

  public String getBookingId() {
    return model.booking_id;
  }

  public String getAmount() {
    return model.amount;
  }

  public String getCurrency() {
    return model.currency;
  }

  public String getDate() {
    return model.date;
  }

  public String getSessionLocation() {
    return fetchLocation(model.session_location);
  }

  public String getSessionType() {
    return fetchSessionType(model.session_type);
  }

  public String getGrade() {
    String name = "";
    if (language.equalsIgnoreCase("en")) {
      name = model.grade_name;
    }
    else {
      name = model.grade_name_in_ar;
    }
    return name;
  }

  public String getLevel() {
    String name = "";
    if (language.equalsIgnoreCase("en")) {
      name = model.level_name;
    }
    else {
      name = model.level_name_in_ar;
    }
    return name;
  }

  public String getTimeFrom() {
    return (model.time_from + " - " + model.time_to);
  }

  private String fetchLocation(String location){
    if(location.equals("0")){
      return fragment.getResources().getString(R.string.student_house);
    }
    else if(location.equals("1")){
      return fragment.getResources().getString(R.string.teacher_house);
    }
    return "";
  }

  private String fetchSessionType(String location){
    if(location.equals("0")){
      return fragment.getResources().getString(R.string.once);
    }
    else if(location.equals("1")){
      return fragment.getResources().getString(R.string.monthly);
    }
    return "";
  }

  public void setModel(CompletedBooking paymentHistory) {
    this.model = paymentHistory;
    notifyChange();

  }

  public void onRateBarClicked(View view){
    Log.d("TAG", "rate bar clicked: "+model.id);
    if(model.teacher_rate == 0){
      RatingDialogFragment.newInstance(model.id, "").show(fragment.getActivity().getSupportFragmentManager(), "RatingDialogFragment");
    }
  }
}
