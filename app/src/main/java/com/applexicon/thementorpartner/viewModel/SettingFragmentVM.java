package com.applexicon.thementorpartner.viewModel;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableField;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.utilities.SharedPref;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.views.activities.UserActivity;
import com.applexicon.thementorpartner.views.fragments.SettingsFragment;
import com.applexicon.thementorpartner.databinding.FragmentSettingsBinding;

import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class SettingFragmentVM extends BaseObservable implements Response.ResponseErrorCodeHandler {


    private final FragmentSettingsBinding binding;
    private final SettingsFragment settingsFragment;
    private String TAG  = "SettingFragmentVM";
    private Context context;
    public ObservableField<String> languageTag;




    public SettingFragmentVM(SettingsFragment settingsFragment) {
        this.settingsFragment = settingsFragment ;
        this.context = settingsFragment.getActivity();
        this.binding = settingsFragment.binding;

        languageTag = new ObservableField<>(LanguageUtils.getUsedLang(context.getApplicationContext()));


        fetchNotificationStatus();

        binding.activitySettingNotifSw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setNotificationStatus(!binding.activitySettingNotifSw.isChecked()? 1 : 0);

                changeNotificationStatus(!binding.activitySettingNotifSw.isChecked());
            }
        });
    }

    private void fetchNotificationStatus() {
        Mentor.getInstance().fetchNotificationStatus(context, new Consumer<Response<Mentor.FetchStatusContentData>>() {
            @Override
            public void accept(Response<Mentor.FetchStatusContentData> response) throws Exception {

                int status = 0;

                if (response.getResult()) {
                    status = response.getContent().getData().status;
                }

                setNotificationStatus(status);
//                Toast.makeText(context , status == 1?  context.getString(R.string.notification_enabled) : context.getString(R.string.notification_disabled), Toast.LENGTH_SHORT).show();

            }
        }, this);
    }


    private void changeNotificationStatus(final boolean b) {

        binding.activitySettingNotifSw.setEnabled(false);
        Mentor.getInstance().status = b? 1 : 0;

        Mentor.getInstance().changeNotificationStatus(context, new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                if (response.getResult()) {
                    setNotificationStatus(b ? 1 : 0);
                    Toast.makeText(context , b?  context.getString(R.string.notification_enabled) : context.getString(R.string.notification_disabled), Toast.LENGTH_SHORT).show();
                }

                binding.activitySettingNotifSw.setEnabled(true);

            }
        }, this);
    }

    private void setNotificationStatus(int status) {
        binding.activitySettingNotifSw.setChecked(status == 1 ? true : false);
    }


    public void switchLanguageClick(View view){
        Log.e(TAG , "switchLanguageClick : tag : " + view.getTag());

        if(view.getTag().equals("en")){
            LanguageUtils.changeToArabic(context.getApplicationContext());
           languageTag.set("ar");
        }else {
            LanguageUtils.changeToEnglish(context.getApplicationContext());
            languageTag.set("en");
        }

        Mentor.getInstance().changeLanguage(context , LanguageUtils.getUsedLang(context) , new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                Log.e(TAG , "switchLanguageClick : " + response.getContent().getMessage());
            }
        }, new Response.ResponseErrorCodeHandler(){

            @Override
            public void handleResponseErrorCodeForUser(String message) {
                Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
            }

            @Override
            public void handleResponseErrorCodeForDebug(String message) {
                Log.e(TAG , "handleResponseErrorCodeForUser : " + message);

            }
        }  );

        MainActivity.start(context);

        settingsFragment.getActivity().finish();

    }

    public void logoutClick(View view){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(context.getString(R.string.logoutConfirmation));

        builder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                logout();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);

    }

    private void logout() {
        Mentor.getInstance().logout(context, new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                if (response.getResult()) {
                    SharedPref.clear(context);
                    UserActivity.start(context);
                    Toast.makeText(context, response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this);
    }
}
