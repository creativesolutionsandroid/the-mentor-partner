/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableField;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.views.fragments.HelpItemDialogFragment;
import com.applexicon.thementorpartner.databinding.FragmentHelpItemDialogBinding;


public class HelpItemDialogFragmentVM extends BaseObservable {


  private final HelpItemDialogFragment fragment;
  private final String url;
  private final FragmentHelpItemDialogBinding binding;
  private final WebView webView;
  public final ObservableField<Object> message;
  public final ObservableField<Integer> messageVisibility;
  private Context context;
  private String TAG = "PaymentRequestVM";
  public final ObservableField<Integer> animationViewVisibility;


  public HelpItemDialogFragmentVM(HelpItemDialogFragment fragment) {
    this.fragment = fragment;
    this.context = fragment.getActivity();
    this.url = fragment.url;
    this.binding = fragment.binding;
    webView = binding.fragmentHelpItemDialogWebView;
    animationViewVisibility = new ObservableField<>(View.GONE);
    message = new ObservableField<>();
    messageVisibility = new ObservableField<>(View.GONE);

    setUpWebView();
  }

  private void setUpWebView() {

    webView.setWebChromeClient(new WebChromeClient() {
      public void onProgressChanged(WebView view, int progress) {
        // Activities and WebViews measure progress with different scales.
        // The progress meter will automatically disappear when we reach 100%
        Log.e(TAG , "onProgressChanged : " + progress);

      }
    });

    webView.setWebViewClient(new WebViewClient() {

      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        Log.e(TAG , "onPageStarted ");
        animationViewVisibility.set(View.VISIBLE);
      }

      @Override
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        Log.e(TAG , "onPageFinished ");
        animationViewVisibility.set(View.GONE);

      }

      @Override
      public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        Log.e(TAG , "onReceivedError : " + description);
//        Toast.makeText(context, "Oh no! " + error., Toast.LENGTH_SHORT).show();
        setMessage(context.getString(R.string.an_error_occured));
      }

      @Override
      public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        Log.e(TAG , "onReceivedError : " + error.toString());
//        Toast.makeText(context, "Oh no! " + error.getDescription(), Toast.LENGTH_SHORT).show();
        setMessage(context.getString(R.string.an_error_occured));
      }
    });

//    webView.loadUrl("https://developer.androi.com/");
    webView.loadUrl(url);
  }

  public void setMessage(String message) {
    Log.e(TAG , "displayMessage : " + message);
    animationViewVisibility.set(View.GONE);
    messageVisibility.set(View.VISIBLE);
    this.message.set( message);
  }


}
