/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.BaseObservable;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import android.view.View;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.SessionRequestModel;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.views.fragments.BookingDetailsDialogFragment;
import com.applexicon.thementorpartner.views.fragments.HomeFragment;

import java.text.DecimalFormat;


public class ItemSessionRequestVM extends BaseObservable {

  private final HomeFragment homeFragment;
  private SessionRequestModel request;
  private Context context;
  public static final DecimalFormat distanceFormat = new DecimalFormat("##,##,###.##");
  private String language = "en";

  public ItemSessionRequestVM(SessionRequestModel request, HomeFragment homeFragment) {
    this.request = request;
    this.homeFragment = homeFragment;
    language = LanguageUtils.getUsedLang(homeFragment.getActivity());
  }

  public String getStudentName() {
    return request.studentName;
  }

  public String getStudentGender() {
    return fetchGender(request.studentGender);
  }

  public String getUserName() {
    return request.userName;
  }

  public String getStudentMobile() {
    return request.studentName;
  }

  public String getSubject() {
    String name = "";
    if (language.equalsIgnoreCase("en")) {
      name = request.subject;
    }
    else {
      name = request.course_name_in_ar;
    }
    return name;
  }

  public String getLevel() {
    String name = "";
    if (language.equalsIgnoreCase("en")) {
      name = request.level_name;
    }
    else {
      name = request.level_name_in_ar;
    }
    return name;
  }

  public String getSessionLocation() {
    return fetchLocation(request.session_location);
  }

  public String getLocation() {
    return request.sessionLocation;
  }

  public String getGrade() {
    String name = "";
    if (language.equalsIgnoreCase("en")) {
      name = request.grade_name;
    }
    else {
      name = request.grade_name_in_ar;
    }
    return name;
  }

  public String getDistance() {
    return distanceFormat.format(Float.parseFloat(request.distance));
  }

  private String fetchGender(String genderId){
    if(genderId.equals("0")){
      return homeFragment.getResources().getString(R.string.male);
    }
    else if(genderId.equals("1")){
      return homeFragment.getResources().getString(R.string.female);
    }
    return "";
  }

  private String fetchLocation(String location){
    if(location.equals("0")){
      return homeFragment.getContext().getResources().getString(R.string.student_house);
    }
    else if(location.equals("1")){
      return homeFragment.getContext().getResources().getString(R.string.teacher_house);
    }
    return "";
  }

  public void mentorItClick(View view) {

    BookingDetailsDialogFragment.newInstance(request).show(homeFragment.getChildFragmentManager() , "BookingDetailsDialogFragment");
  }

  public void setRequest(SessionRequestModel request) {
    this.request = request;
    notifyChange();
  }

  public void onCallClicked(View view){
    if (ActivityCompat.checkSelfPermission(homeFragment.getActivity(), Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        homeFragment.getActivity().requestPermissions(
                new String[]{android.Manifest.permission.CALL_PHONE},
                0);
      }
    }
    else{
      Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+request.studentMobile));
      homeFragment.getActivity().startActivity(intent);
    }
  }
}
