package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableInt;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.models.SignIn;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.response.ResponseErrorCode;
import com.applexicon.thementorpartner.services.MyFirebaseMessagingService;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.views.activities.VerificationActivity;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.views.activities.OfficeDetailsActivity;
import com.applexicon.thementorpartner.views.activities.UserActivity;
import com.applexicon.thementorpartner.views.fragments.ForgotPasswordDialogFragment;
import com.applexicon.thementorpartner.views.fragments.SignInFragment;
import com.applexicon.thementorpartner.views.fragments.SignUpFragment;
import com.applexicon.thementorpartner.databinding.FragmentSignInBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.List;

import vendor.Errors.ValidationError;
import vendor.Errors.TextInput;
import vendor.Validation.Equal;
import vendor.Validation.IValidationHandler;
import vendor.Validation.IValidationRule;
import vendor.Validation.Max;
import vendor.Validation.Minimum;
import vendor.Validation.Mobile;
import vendor.Validation.MobileMax;
import vendor.Validation.Required;
import vendor.Validation.Validation;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class SignInFragmentViewModel extends BaseObservable implements IValidationHandler, Response.ResponseErrorCodeHandler {


    private final SignIn signIn;
    public final ObservableInt signInMessageVisibility;
    private final FragmentSignInBinding binding;
    private final Mentor mentor;
    private final UserActivityViewModel viewModel;
    private String signInMessage;
    public String TAG  = "SignInFragmentViewModel";

    private Context context;
//    private ArrayList<ValidationError> errors;

    public SignInFragmentViewModel(@NonNull SignInFragment signInFragment, FragmentSignInBinding binding) {
        this.context = signInFragment.getActivity();

        this.viewModel = ((UserActivity)signInFragment.getActivity()).viewModel;

         mentor = Mentor.getInstance();

        signInMessageVisibility = new ObservableInt(View.GONE);

        this.binding = binding;

         signIn = new SignIn();

        actionListner();

        passwordTextWatcher();

    }

    private void passwordTextWatcher(){
        this.binding.fragmentSignInPasswordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                EditText et = (EditText) binding.fragmentSignInPasswordEt;
                validateInputPassword(et);
            }
        });
    }

    private void actionListner() {
        this.binding.fragmentSignInPasswordEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    EditText et = (EditText)v ;
                    validateInputPassword( et);
                }
            }
        });

        this.binding.fragmentSignInPhoneEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    EditText et = (EditText)v ;
                    validateInputPhone( et);
                }
            }
        });


        this.binding.fragmentSignInPhoneKeysSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (view !=null)
                ((TextView) view).setTextColor(Color.GRAY);
                signIn.setPhoneKey(context.getResources().getStringArray(R.array.phone_keys)[i].replace("00","+"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                signIn.setPhoneKey( context.getResources().getStringArray(R.array.phone_keys)[0].replace("00","+"));
            }
        });

    }

    private void validateInputPhone(EditText et) {

        Log.e(TAG , "validateInputPhone");
        final String phone = et.getText().toString();

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new TextInput(binding.fragmentSignInPhoneTil),
                new IValidationRule[]{
                        new Required(phone),
                        new Mobile(phone),
                        new Equal(phone,9)
                });



        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                onPhoneValidationSuccessfull(phone);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);

            }
        });
    }

    private void validateInputPassword(EditText v) {

        final String password = v.getText().toString();

        Validation validation = new Validation();


        validation.addValidationField(

                new TextInput(binding.fragmentSignInPasswordTil),
                new IValidationRule[]{
                        new Required( password),
                        new Minimum( password,5)
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                onPassordValidationSuccessfull(password);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);
            }
        });
    }

    private void onPassordValidationSuccessfull(String password) {
        signIn.setPassword(password);
//        errors.clear();
    }

    public void setPhone(final String phone){
//        Log.e(TAG , "setCode : " + phone);

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new TextInput(binding.fragmentSignInPhoneTil),
                new IValidationRule[]{
                        new MobileMax( phone,9 , context.getString(R.string.validation_mobile_equal))
                });



        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                onPhoneValidationSuccessfull(phone);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {

                onFieldValidationFailed(validationErrors);

            }
        });
    }

    private void onFieldValidationFailed(List<ValidationError> validationErrors) {
//        this.validationErrors.clear();
//        this.validationErrors = validationErrors;
        for (ValidationError validationError : validationErrors) {
            validationError.displayError();
        }
    }

    private void onPhoneValidationSuccessfull(String phone) {
        signIn.setPhone(phone);
//        errors.clear();
    }

    public void setPassword(final String password){

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new TextInput(binding.fragmentSignInPasswordTil),
                new IValidationRule[]{
                        new Max( password,30)
                });


        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                signIn.setPassword(password);
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {

                onFieldValidationFailed(validationErrors);

            }
        });

    }

    public void setSignInMessage(String signInMessage){
        signInMessageVisibility.set(View.VISIBLE);
        this.signInMessage = signInMessage;
        notifyChange();
    }


    public String getSignInMessage(){
        return signInMessage;
    }

    public String getPhone(){
        return  signIn.getPhone();
    }

    public String getPassword(){
        return signIn.getPassword();
    }

    public void onSignInClick(View view){
        Log.e(TAG , "onSignInClick");

       viewModel.animationViewVisibility.set(View.VISIBLE);

        validateInputs();
    }

    private void validateInputs() {
        Log.e(TAG , "validateInputs");
        final String phone = binding.fragmentSignInPhoneEt.getText().toString();
        final String password = binding.fragmentSignInPasswordEt.getText().toString();

//        this.binding.fragmentSignInPhoneKeysSp.setPrompt("sdgdsgfsdg");//will be removed;

        Validation validation = new Validation();

        //phone
        validation.addValidationField(

                new TextInput(binding.fragmentSignInPhoneTil),
                new IValidationRule[]{
                        new Required(  phone),
                        new Mobile(  phone),
                        new Equal(  phone,9)
                });


        validation.addValidationField(

                new TextInput(binding.fragmentSignInPasswordTil),
                new IValidationRule[]{
                        new Required( password),
                        new Minimum( password,5)
                });


        validation.validate(context, this);
    }

    public void onSignUpClick(View view){
        Log.e(TAG , "onSignUpClick");

        SignUpFragment signUpFragment =  SignUpFragment.newInstance();

        FragmentTransaction bt =
                ((UserActivity) this.context).getSupportFragmentManager().beginTransaction();
        bt.replace(R.id.activityLogin_container_fl , signUpFragment , "SignUpFragment");
        bt.commit();
        bt.addToBackStack("SignUpFragment");

    }

    public void onPasswordClick(View view){
        Log.e(TAG , "onPasswordClick");

        ForgotPasswordDialogFragment.newInstance().show(((UserActivity)context).getSupportFragmentManager() , "ForgotPasswordDialogFragment");

    }

    public void onResponseSuccessfull(Response<SignIn.SignInContentData> response) {
        Log.e(TAG , "onEditProfileResponseSuccessfull : " + response.toString());

        if(response.getResult()){

            Mentor mentor = response.getContent().getData().mentor;
            mentor.token = response.getContent().getData().token;

            mentor.login(context);

            mentor.setmMentor(mentor);

            Log.e(TAG , "onResponseSuccessfull : " + Mentor.getInstance().token);

             getFirebaseId();

             mentor.changeLanguage(context , LanguageUtils.getUsedLang(context) , new Consumer<Response>() {
                 @Override
                 public void accept(Response response) throws Exception {
                     Log.e(TAG , "onResponseSuccessfull : " + response.getContent().getMessage());
                 }
             }, new Response.ResponseErrorCodeHandler(){

                 @Override
                 public void handleResponseErrorCodeForUser(String message) {
                     Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
                 }

                 @Override
                 public void handleResponseErrorCodeForDebug(String message) {
                     Log.e(TAG , "handleResponseErrorCodeForUser : " + message);

                 }
             }  );

            MainActivity.start(context);

        }else{
            if(response.getErrorCode() == ResponseErrorCode.ACCOUNT_NOT_ACTIVED){
                OfficeDetailsActivity.start(context);
                ((UserActivity)context).finish();
            }else if(response.getErrorCode() == ResponseErrorCode.PHONE_NOT_VERIFIED){
                VerificationActivity.start(context , signIn.getPhoneKey() + getPhone() , false);
                ((UserActivity)context).finish();
            }else if(response.getErrorCode() == ResponseErrorCode.INVALID_CREDENTIALS) {
                setSignInMessage(response.getContent().getMessage());

            }

            response.handleResponseErrorCode(context, this);
        }
        viewModel.animationViewVisibility.set(View.GONE);
    }

    private void getFirebaseId() {

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "getInstanceId failed", task.getException());
                            return;
                        }

                        MyFirebaseMessagingService myFirebaseMessagingService = new MyFirebaseMessagingService();
                        myFirebaseMessagingService.sendTokenToServer(task.getResult().getToken(), context);
                    }
                });

//        if ( FirebaseInstanceId.getInstance().getToken()!=null){
//            MyFirebaseInstanceIDService myFirebaseInstanceIDService = new MyFirebaseInstanceIDService();
//            myFirebaseInstanceIDService.sendTokenToServer(FirebaseInstanceId.getInstance().getToken(),context);
//        }
    }

    @Override
    public void onValidationSuccessfull() {
        Log.e(TAG , "onValidationSuccessfull");

        signIn.signIn(context , new Consumer<Response<SignIn.SignInContentData>>() {
            @Override
            public void accept(Response<SignIn.SignInContentData> response) throws Exception {
                onResponseSuccessfull(response);
            }
        }, this);
    }

    @Override
    public void onValidationFailed(List<ValidationError> validationErrors) {
        Log.e(TAG , "onValidationFailed");
        onFieldValidationFailed(validationErrors);
        viewModel.animationViewVisibility.set(View.GONE);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        viewModel.animationViewVisibility.set(View.GONE);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        viewModel.animationViewVisibility.set(View.GONE);
    }
}
