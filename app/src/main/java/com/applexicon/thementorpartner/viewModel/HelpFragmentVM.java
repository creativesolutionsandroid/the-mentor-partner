/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.BaseObservable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.activities.WebViewActivity;
import com.applexicon.thementorpartner.views.fragments.HelpFragment;
import com.applexicon.thementorpartner.databinding.FragmentHelpBinding;


public class HelpFragmentVM extends BaseObservable implements Response.ResponseErrorCodeHandler {


  private final HelpFragment fragment;
  private final FragmentHelpBinding binding;
  private Context context;
  private String TAG = "PaymentRequestVM";

  public HelpFragmentVM(HelpFragment fragment) {
    this.fragment = fragment;
    this.binding = fragment.binding;
    this.context = fragment.getActivity();
  }

  public void onAboutClicked(View view) {
    Log.e(TAG, "onAboutClicked");

//      String url = String.format(Constants.HELP_URL, "about" , LanguageUtils.getUsedLang(fragment.getActivity().getApplicationContext()));
//
//      Log.e(TAG , "url : " + url);
      Intent intent = new Intent(context, WebViewActivity.class);
      intent.putExtra("class",context.getResources().getString(R.string.about_the_mentor));
      context.startActivity(intent);

//      HelpItemDialogFragment.newInstance(url).show(fragment.getChildFragmentManager() , "HelpItemDialogFragment");
  }
   public void onContactClicked(View view) {
//    Log.e(TAG, "onContactClicked");
//       String url = String.format(Constants.HELP_URL, "contact" , LanguageUtils.getUsedLang(fragment.getActivity().getApplicationContext()));
//       Log.e(TAG , "url : " + url);
       Intent intent = new Intent(context, WebViewActivity.class);
       intent.putExtra("class",context.getResources().getString(R.string.contact));
       context.startActivity(intent);
//       HelpItemDialogFragment.newInstance(url).show(fragment.getChildFragmentManager() , "HelpItemDialogFragment");
  }
   public void onTermsAndConditionClicked(View view) {
//    Log.e(TAG, "onTermsAndConditionClicked");
//       String url = String.format(Constants.HELP_URL, "terms" , LanguageUtils.getUsedLang(fragment.getActivity().getApplicationContext()));
//       Log.e(TAG , "url : " + url);
       Intent intent = new Intent(context, WebViewActivity.class);
       intent.putExtra("class",context.getResources().getString(R.string.terms_and_conditions));
       context.startActivity(intent);
//       HelpItemDialogFragment.newInstance(url).show(fragment.getChildFragmentManager() , "HelpItemDialogFragment");
  }
   public void onPrivacyPolicyClicked(View view) {
//    Log.e(TAG, "onPrivacyPolicyClicked");
//       String url = String.format(Constants.HELP_URL, "policy" , LanguageUtils.getUsedLang(fragment.getActivity().getApplicationContext()));
//       Log.e(TAG , "url : " + url);
       Intent intent = new Intent(context, WebViewActivity.class);
       intent.putExtra("class",context.getResources().getString(R.string.privacy_policy));
       context.startActivity(intent);
//       HelpItemDialogFragment.newInstance(url).show(fragment.getChildFragmentManager() , "HelpItemDialogFragment");
  }


  @Override
  public void handleResponseErrorCodeForUser(String message) {
    Log.e(TAG, "handleResponseErrorCodeForUser");
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void handleResponseErrorCodeForDebug(String message) {
    Log.e(TAG, "handleResponseErrorCodeForDebug");

  }
}
