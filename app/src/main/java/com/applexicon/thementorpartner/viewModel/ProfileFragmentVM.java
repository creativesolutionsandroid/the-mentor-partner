package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.models.Profile;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.ImageLoading;
import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.views.fragments.EditProfileFragment;
import com.applexicon.thementorpartner.views.fragments.ProfileFragment;
import com.applexicon.thementorpartner.databinding.FragmentMyProfileBinding;

import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class ProfileFragmentVM extends BaseObservable implements Response.ResponseErrorCodeHandler {


    private final FragmentMyProfileBinding binding;
    public final ObservableField<Integer> animationViewVisibility;
    public final ObservableField<Integer> messageVisibility;
    private final ProfileFragment profileFragment;
    private  String message;

    private String TAG  = "ProfileFragmentVM";
    private Context context;

    public final ObservableInt progressBar;
//    private Mentor mentor = new Mentor();

    private Profile profile = new Profile();


    public ProfileFragmentVM(@NonNull final ProfileFragment profileFragment, FragmentMyProfileBinding binding) {

        this.profileFragment = profileFragment;
        this.context = profileFragment.getActivity();
        this.binding = binding;

        progressBar = new ObservableInt(View.GONE);

        animationViewVisibility = new ObservableField<>(View.GONE);
        messageVisibility = new ObservableField<>(View.GONE);


        fetchMentorProfile();


        ImageLoading.loadImage(binding.activityEditProfileUploadImageCiv ,
                Mentor.getInstance().imageUrl ,
                R.drawable.avatar,
                progressBar);



    }

    private void fetchMentorProfile() {

        animationViewVisibility.set(View.VISIBLE);
        messageVisibility.set(View.GONE);

       profile.fetchProfile(context, new Consumer<Response<Profile>>() {
            @Override
            public void accept(Response<Profile> response) throws Exception {
                if(response.getResult()){
                    setProfile(response.getContent().getData());
                }
                else{
                    response.handleResponseErrorCode(context , ProfileFragmentVM.this);
                }

                animationViewVisibility.set(View.GONE);
            }
        } , this);

    }

    private void setProfile(Profile profile) {

        Mentor.getInstance().name  = profile.name;
        Mentor.getInstance().email  = profile.email;
        Mentor.getInstance().phone  = profile.phone;
        Mentor.getInstance().address  = profile.address;

        Mentor.getInstance().setDataToSharedPref(context);

        this.profile = profile;
        ((MainActivity)this.profileFragment.getActivity()).nameTV.setText(Mentor.getInstance().name);

        notifyChange();
    }


    public String getImageUrl() {
        return profile.imageUrl;
    }

    public String getFullname() {
        return profile.name;
    }


    public String getPhone() {
        return profile.phone;
    }


    public String getEmail() {
        return profile.email;
    }

    public String getAddress() {
        return profile.address;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        animationViewVisibility.set(View.GONE);
        messageVisibility.set(View.VISIBLE);
        this.message = message;
    }

    public void onEditClick(View view){
        Log.e(TAG , "onEditClick");
        ((MainActivity)profileFragment.getActivity()).addFragment(EditProfileFragment.newInstance());

    }




    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        animationViewVisibility.set(View.GONE);
        setMessage(message);
        notifyChange();
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        animationViewVisibility.set(View.GONE);

    }
}
