/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.BaseObservable;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.ConfirmedSessionFiltered;
import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.utilities.Constants;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.views.fragments.MyConfirmedSessionsFragment;
import com.applexicon.thementorpartner.databinding.MySessionsConfirmedItemBinding;

import java.text.DecimalFormat;

import io.reactivex.functions.Consumer;


public class ItemSessionConfirmedVM extends BaseObservable implements Response.ResponseErrorCodeHandler {

  private final MyConfirmedSessionsFragment fragment;
  private final MySessionsConfirmedItemBinding binding;
  private final Context context;
  private ConfirmedSessionFiltered model;
  private String TAG = "ItemSessionConfirmedVM";
  private View view;
  public static final DecimalFormat distanceFormat = new DecimalFormat("##,##,##0.00");
  private String language = "en";

  public ItemSessionConfirmedVM(ConfirmedSessionFiltered model, MyConfirmedSessionsFragment fragment, MySessionsConfirmedItemBinding itemBinding) {
    this.model = model;
    this.fragment = fragment;
    this.binding = itemBinding;
    language = LanguageUtils.getUsedLang(fragment.getActivity());

    this.context = fragment.getActivity();

//    if (model.availabeForStart){
//      binding.mySessionsConfirmedItemRequestForPayBtn.setEnabled(true);
//      binding.mySessionsConfirmedItemRequestForPayBtn.setAlpha(1);
//    }
  }

  public String getStatus() {
    return status();
  }

  public String getSessionId() {
    return "Session"+model.id;
  }

  public String getBookingId() {
    return model.booking_name;
  }

  public String getSubjectName() {
    String name = "";
    if (language.equalsIgnoreCase("en")) {
      name = model.subjectName;
    }
    else {
      name = model.course_name_in_ar;
    }
    return name;
  }

  public String getLocation() {
    return model.location;
  }

  public String getStudentName() {
    return model.studentName;
  }

  public String getUserName() {
    return model.userName;
  }

  public String getId() {
    return model.booking_name;
  }

  public String getAmount() {
    return Constants.convertToArabic(distanceFormat.format(Float.parseFloat(model.amount)));
  }

  public String getGradeName() {
    String name = "";
    if (language.equalsIgnoreCase("en")) {
      name = model.grade_name;
    }
    else {
      name = model.grade_name_in_ar;
    }
    return name;
  }

  public String getLevelName() {
    String name = "";
    if (language.equalsIgnoreCase("en")) {
      name = model.level_name;
    }
    else {
      name = model.level_name_in_ar;
    }
    return name;
  }

  public String getCurrency() {
    return model.currency;
  }

  public String getStudentGender() {
    return fetchGender(model.studentGender);
  }

  public String getPaymentType() {
    return fetchPaymentType(model.payment_type);
  }

  public String getSessionType() {
    return fetchSessionType(model.session_type);
  }

  public String getSessionAt() {
    return fetchSessionAt(model.session_location);
  }

  public String getStartDate() {
    return fetchStartDate(model.date) ;
  }

  public String getEndDate() {
    return fetchEndDate(model.date) ;
  }

  public String getDistance() {
    return Constants.convertToArabic(distanceFormat.format(model.distance_in_km));
  }

  public String getTime() {
    return model.timeFrom + " - " + model.timeTo;
  }

  private String status(){
    if(model.paidAmount == null || model.paidAmount.equals("0")) {
      return "Payment pending";
    }
    else {
      return context.getResources().getString(R.string.confirmed);
    }
  }

  private String fetchGender(String genderId){
    if(genderId.equals("0")){
      return context.getResources().getString(R.string.male);
    }
    else if(genderId.equals("1")){
      return context.getResources().getString(R.string.female);
    }
    return "";
  }

  private String fetchPaymentType(String paymentType){
    if(paymentType.equals("0")){
      return context.getResources().getString(R.string.cash_text);
    }
    else if(paymentType.equals("1")){
      return context.getResources().getString(R.string.credit);
    }
    return "";
  }

  private String fetchSessionType(String genderId){
    if(genderId.equals("0")){
      return context.getResources().getString(R.string.once);
    }
    else if(genderId.equals("1")){
      return context.getResources().getString(R.string.monthly);
    }
    return "";
  }

  private String fetchSessionAt(String genderId){
    if(genderId.equals("0")){
      return context.getResources().getString(R.string.student_house);
    }
    else if(genderId.equals("1")){
      return context.getResources().getString(R.string.teacher_house);
    }
    return "";
  }

  private String fetchStartDate(String date){
    String[] dates = date.split(",");
    return dates[0];
  }

  private String fetchEndDate(String date){
    String[] dates = date.split(",");
    return dates[(dates.length-1)];
  }

  private String fetchtimeFrom(String date){
    String[] dates = date.split(",");
    return dates[0];
  }

  private String fetchTimeto(String date){
    String[] dates = date.split(",");
    return dates[0];
  }

  public void onSessionDetailsClicked(View view){
            com.applexicon.thementorpartner.views.fragments.SessionsListFragment.newInstance(context, model);
  }

  public void setModel(ConfirmedSessionFiltered model) {
    this.model = model;
    notifyChange();
  }

  public void onCallClicked(View view){
    if (ActivityCompat.checkSelfPermission(fragment.getActivity(), Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        fragment.getActivity().requestPermissions(
                new String[]{android.Manifest.permission.CALL_PHONE},
                0);
      }
    }
    else{
      Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+model.studentPhone));
      fragment.getActivity().startActivity(intent);
    }
  }

  public void onRequestForPayClicked(final View view){

    Log.e(TAG , "onRequestForPayClicked");

    this.view = view;

    enableButton(view , false);

    final Session session = new Session();
    session.id = model.id;

    session.paymentRequest(context, new Consumer<Response>() {
      @Override
      public void accept(Response response) throws Exception {
        Log.e(TAG , "paymentRequest accept successful");

        if(response.getResult()){
          refreshConfirmedSessions();
        }

        enableButton(view , true);

        Toast.makeText(context, response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
//        session.setEndSessionAlarm(context);
      }
    } , this);
  }

  public void navigateToMap(View view){
    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
            Uri.parse("http://maps.google.com/maps?saddr="+model.teacherLatitude+","+model.teacherLongitude+
                    "&daddr="+model.sessionLatitude+","+model.sessionLongitude));
    context.startActivity(intent);
  }

  private void enableButton(View view, boolean b) {

    view.setEnabled(b);

    view.setAlpha(b ? 1f : 0.5f);
  }

  private void refreshConfirmedSessions() {
    Log.e(TAG , "refreshConfirmedSessions");
    fragment.viewModel.fetchSessionsConfirmed();
  }


  @Override
  public void handleResponseErrorCodeForUser(String message) {
    Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    enableButton(view  , true);
  }

  @Override
  public void handleResponseErrorCodeForDebug(String message) {
    Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
  }
}
