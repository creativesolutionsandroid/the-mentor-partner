package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.ObservableField;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.SessionRequestModel;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.databinding.FragmentHomeBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class HomeFragmentVM extends Observable implements Response.ResponseErrorCodeHandler {


    private final FragmentHomeBinding binding;
    public final ObservableField<Integer> animationViewVisibility;
    public final ObservableField<Integer> messageVisibility;
    public   ObservableField<String> message;
    private final ArrayList<SessionRequestModel> sessionRequests;
    private String TAG  = "VerificationVM";
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public HomeFragmentVM(@NonNull final Context context, FragmentHomeBinding binding) {
        this.context = context;
        this.binding = binding;

        this.context = context;
        this.sessionRequests = new ArrayList<SessionRequestModel>();

        animationViewVisibility = new ObservableField<>(View.GONE);
        message = new ObservableField<>();
        messageVisibility = new ObservableField<>(View.GONE);

        fetchSessions();
    }

    public void fetchSessions() {
        initializeViews();

        Mentor.getInstance().fetchRequestList(context , new Consumer<Response<Mentor.FetchRequestsContentData>>(){

            @Override
            public void accept(Response<Mentor.FetchRequestsContentData> response) throws Exception {

                Log.e(TAG , "fetchRequestList response success " );
                if(response.getResult()){
                    if(response.getContent().getData().sessionRequestsList.size() > 0){
                        changePeopleDataSet(response.getContent().getData().sessionRequestsList);
                        messageVisibility.set(View.GONE);
                    }
                    else{
                        //not sessions found
                        Log.e(TAG , "not sessions found");
                        handleResponseErrorCodeForUser(context.getString(R.string.no_requests_found));
                    }
                }else{
                    response.handleResponseErrorCode(context , HomeFragmentVM.this);
                }
                animationViewVisibility.set(View.GONE);
            }
        } ,this );
    }


    public void initializeViews() {
        animationViewVisibility.set(View.VISIBLE);
        messageVisibility.set(View.GONE);
    }



    private void changePeopleDataSet(List<SessionRequestModel> list) {
        sessionRequests.clear();
        sessionRequests.addAll(list);
        setChanged();
        notifyObservers();
    }

    public ArrayList<SessionRequestModel> getSessionRequests() {
        return sessionRequests;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }



    public void setMessage(String message) {
        Log.e(TAG , "displayMessage : " + message);
        animationViewVisibility.set(View.GONE);
        messageVisibility.set(View.VISIBLE);
        this.message.set( message);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {

        animationViewVisibility.set(View.GONE);
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        setMessage(message);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        animationViewVisibility.set(View.GONE);
    }
}
