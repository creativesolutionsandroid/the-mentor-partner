package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableInt;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.Verification;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.activities.VerificationActivity;
import com.applexicon.thementorpartner.views.activities.OfficeDetailsActivity;
import com.applexicon.thementorpartner.views.fragments.NewPasswordDialogFragment;
import com.applexicon.thementorpartner.databinding.ActivityVerificationBinding;

import java.util.List;

import vendor.Errors.ValidationError;
import vendor.Errors.MyEditText;
import vendor.Validation.Equal;
import vendor.Validation.IValidationHandler;
import vendor.Validation.IValidationRule;
import vendor.Validation.Required;
import vendor.Validation.Validation;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class VerificationVM extends BaseObservable implements Response.ResponseErrorCodeHandler {


    private final Verification verification;
    private final ActivityVerificationBinding binding;
    private final String phone;
    public final ObservableInt animationViewVisibility;
    private final VerificationActivity verificationActivity;
    private String TAG  = "VerificationVM";
    private Context context;

    private String verifyMessage;
    private String resendMessage;

    public ObservableInt verifyMessageVisibility;
    public ObservableInt resendMessageVisibility;


    public VerificationVM(@NonNull VerificationActivity verificationActivity, ActivityVerificationBinding binding, String phone) {
        this.verificationActivity = verificationActivity;
        this.context = verificationActivity;
        this.phone = phone;
        this.binding = binding;

        verifyMessageVisibility = new ObservableInt(View.GONE);
        resendMessageVisibility = new ObservableInt(View.GONE);
        animationViewVisibility = new ObservableInt(View.GONE);

         verification = new Verification(phone);

    }

    public void onActivateClick(View view){

        animationViewVisibility.set(View.VISIBLE);
        Log.e(TAG , "onActivateClick");

        String code = binding.activityActivationCodeEt.getText().toString();

        setActivationCode(code);

        Validation validation = new Validation();
        //phone
        validation.addValidationField(

                new MyEditText(binding.activityActivationCodeEt),
                new IValidationRule[]{
                        new Required( code),
                        new Equal( code,4)
                });

        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                onActivateValidationSuccessful();
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onVerificationValidationFailed(validationErrors);
            }
        });
    }

    private void onVerificationValidationFailed(List<ValidationError> validationErrors) {

        animationViewVisibility.set(View.GONE);

        for(ValidationError error : validationErrors){
            error.displayError();
        }
    }

    private void onActivateValidationSuccessful() {
        Log.e(TAG , "onActivateValidationSuccessful");

        verification.activate(context , new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                Log.e(TAG, "Consumer<Verification.ActivationResponse>()");

                if(response.getResult()){
                    if(verificationActivity.isForgotPassword) {
                        NewPasswordDialogFragment.newInstance(getPhone()).show(verificationActivity.getSupportFragmentManager(), "NewPasswordDialogFragment");
                        animationViewVisibility.set(View.GONE);
                    }
                    else {
//                        UserActivity.start(context);
                        OfficeDetailsActivity.start(context);
                        ((VerificationActivity) context).finish();
//                        animationViewVisibility.set(View.GONE);
                    }

                }else {
                    handleResponseErrorCodeForUser(response.getContent().getMessage());
                    animationViewVisibility.set(View.GONE);
                }

            }
        },this);
    }

    public void onResendClick(View view){
        animationViewVisibility.set(View.VISIBLE);

        Log.e(TAG , "onResendClick");

        verification.resend(context , new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                Log.e(TAG, "onResendClick : Consumer<Verification.ActivationResponse>()");

                if(response.getResult()){
                    Toast.makeText(context, response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
                }else {
                    handleResponseErrorCodeForUser(response.getContent().getMessage());
                }

                animationViewVisibility.set(View.GONE);

            }
        },this);
    }

    public String getActivationCode() {
        return verification.getCode();
    }
    public String getPhone() {
        return verification.getPhone();
    }

    public void setActivationCode(String activationCode) {
        verification.setCode(activationCode);
    }

    public String getVerifyMessage() {
        return verifyMessage;
    }

    public String getResendMessage() {
        return resendMessage;
    }


    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
        animationViewVisibility.set(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);

    }
}
