package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.ObservableField;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.applexicon.thementorpartner.models.CompletedBooking;
import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.fragments.MyCompletedSessionsFragment;
import com.applexicon.thementorpartner.databinding.FragmentMySessionsCompletedBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class SessionCompletedFragmentVM extends Observable implements Response.ResponseErrorCodeHandler {


    private final FragmentMySessionsCompletedBinding binding;
    public final ObservableField<Integer> animationViewVisibility;
    public final ObservableField<Integer> messageVisibility;
    public   ObservableField<String> message;
    private final ArrayList<CompletedBooking> dataList;
    private String TAG  = "BookingCompletedFragVM";
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();



    public SessionCompletedFragmentVM(MyCompletedSessionsFragment completedBookingsFragment) {
        this.context = completedBookingsFragment.getActivity();
        this.binding = completedBookingsFragment.binding;

        this.dataList = new ArrayList<>();

        animationViewVisibility = new ObservableField<>(View.GONE);
        messageVisibility = new ObservableField<>(View.GONE);

        message = new ObservableField<>();

    }



    public void fetchSessionsCompleted() {

        initializeViews();

        Mentor.getInstance().fetchSessionsCompleted(context , new Consumer<Response<Mentor.SessionsCompletedContentData>>(){

            @Override
            public void accept(Response<Mentor.SessionsCompletedContentData> response) throws Exception {

                Log.e(TAG , "fetchRequestList response success " );
                if(response.getResult()){
                    if (response.getContent().getData().completedBookingList.size() > 0) {
                        changeDataSet(response.getContent().getData().completedBookingList);
                        messageVisibility.set(View.GONE);
                    }
                    else{
                        //not sessions found
                        setMessage(context.getString(R.string.no_session_completed));
                    }

                }else{

                    response.handleResponseErrorCode(context , SessionCompletedFragmentVM.this);
                }

                animationViewVisibility.set(View.GONE);


            }
        } ,this );
    }


    public void initializeViews() {
        animationViewVisibility.set(View.VISIBLE);
        messageVisibility.set(View.GONE);
    }


    private void changeDataSet(List<CompletedBooking> completedBookingList) {
        dataList.clear();
        dataList.addAll(completedBookingList);
        setChanged();
        notifyObservers();
    }

    public List<CompletedBooking> getSessionsleList() {
        return dataList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }



    public void setMessage(String message) {
        animationViewVisibility.set(View.GONE);
        messageVisibility.set(View.VISIBLE);
        this.message.set(message);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {

        animationViewVisibility.set(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        setMessage(message);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        animationViewVisibility.set(View.GONE);
    }
}
