package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import android.util.Log;
import android.view.View;

import com.applexicon.thementorpartner.views.activities.MainActivity;
import com.applexicon.thementorpartner.databinding.ActivityMainBinding;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class NavHeaderVM extends BaseObservable  {


    private final ActivityMainBinding binding;
    private String TAG  = "NavHeaderVM";
    private Context context;




    public NavHeaderVM(MainActivity mainActivity) {
        this.context = mainActivity.getApplicationContext();
        this.binding = mainActivity.mainBinding;

    }

    public void onViewProfileClick(View view){
        Log.e(TAG , "onViewProfileClick");
    }

}
