/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import androidx.databinding.BaseObservable;

import com.applexicon.thementorpartner.models.Session;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.views.fragments.SessionListDialogFragment;


public class ItemSessionListDialogVM extends BaseObservable {

  private Session item;
  private SessionListDialogFragment fragment;



  public ItemSessionListDialogVM(Session item, SessionListDialogFragment fragment) {
    this.item = item;
    this.fragment = fragment;
  }

  public String getSession() {
    return fragment.getString(R.string.session) + " " + item.session_name;
  }


  public void setItem(Session item) {
    this.item = item;
    notifyChange();

  }
}
