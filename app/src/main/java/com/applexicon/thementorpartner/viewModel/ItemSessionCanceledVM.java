/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applexicon.thementorpartner.viewModel;

import androidx.databinding.BaseObservable;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.models.CanceledSession;
import com.applexicon.thementorpartner.utilities.Constants;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.views.fragments.MyCanceledSessionsFragment;

import static com.applexicon.thementorpartner.utilities.Constants.distanceFormat;


public class ItemSessionCanceledVM extends BaseObservable {

    private final MyCanceledSessionsFragment fragment;
    private CanceledSession model;
    private String TAG = "ItemSessionCanceledVM";
    private String language = "en";

    public ItemSessionCanceledVM(CanceledSession model, MyCanceledSessionsFragment fragment) {
        this.model = model;
        this.fragment = fragment;
        language = LanguageUtils.getUsedLang(fragment.getActivity());

    }

    public String getSessionId() {
        return "Session"+model.id;
    }

    public String getSubjectName() {
        return model.subjectName;
    }


    public String getLocation() {
        return model.location;
    }

    public String getDate() {
        return model.date;
    }

    public String getStudentName() {
        return model.studentName;
    }

    public String getId() {
        return model.session_name;
    }

    public String getCancelAmount() {
        if(model.cancel_by != null && model.cancel_by.equalsIgnoreCase("teacher")) {
            return Constants.convertToArabic(distanceFormat.format(Float.parseFloat(model.cancel_amount)));
        }
        else {
            return "0.00";
        }
    }

    public String getReason(){
        return model.reason;
    }

    public String getCancelledBy(){
        String name = "";
        if(model.cancel_by == null || model.cancel_by.equalsIgnoreCase("")) {
            if (language.equalsIgnoreCase("en")) {
                name = "System";
            }
            else {
                name = "نظام";
            }
        }
        else  {
            if (language.equalsIgnoreCase("en")) {
                name = model.cancel_by;
            }
            else {
                if (model.cancel_by.equalsIgnoreCase("student")) {
                    name = "الطالب";
                }
                else {
                    name = "المعلم";
                }
            }
        }
        return name;
    }

    public String getSessionLocation() {
        return fetchLocation(model.session_location);
    }

    public String getSessionType() {
        return fetchSessionType(model.session_type);
    }

    public String getGrade() {
        return model.grade_name;
    }

    public String getLevel() {
        return model.level_name;
    }

    public String getTimeFrom() {
        return (model.time_from + " - " + model.time_to);
    }

    private String fetchLocation(String location){
        if(location.equals("0")){
            return fragment.getResources().getString(R.string.student_house);
        }
        else if(location.equals("1")){
            return fragment.getResources().getString(R.string.teacher_house);
        }
        return "";
    }

    private String fetchSessionType(String location){
        if(location.equals("0")){
            return fragment.getResources().getString(R.string.once);
        }
        else if(location.equals("1")){
            return fragment.getResources().getString(R.string.monthly);
        }
        return "";
    }


    public void setModel(CanceledSession confirmedBooking) {
        this.model = confirmedBooking;
        notifyChange();

    }
}
