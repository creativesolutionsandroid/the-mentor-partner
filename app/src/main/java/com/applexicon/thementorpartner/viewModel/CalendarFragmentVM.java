package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableField;
import android.graphics.Color;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.Mentor;
import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.fragments.MyCalendarFragment;
import com.applexicon.thementorpartner.views.fragments.SessionListDialogFragment;
import com.applexicon.thementorpartner.databinding.FragmentMyCalendarBinding;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.functions.Consumer;


/**
 * Created by Hamdy on 7/24/2017.
 */

public class CalendarFragmentVM extends BaseObservable implements Response.ResponseErrorCodeHandler {


    private final FragmentMyCalendarBinding binding;
    private final MyCalendarFragment myCalendarFragment;
    private String TAG  = "CalendarFragmentVM";
    private Context context;
    private SimpleDateFormat dateFormatMonth = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());

    public final ObservableField<Integer> animationViewVisibility;
    public final ObservableField<Integer> messageVisibility;
    public ObservableField<String> message;


    public CalendarFragmentVM(@NonNull MyCalendarFragment myCalendarFragment, FragmentMyCalendarBinding binding) {

        this.myCalendarFragment = myCalendarFragment;
        this.context = myCalendarFragment.getActivity();
        this.binding = binding;

        message = new ObservableField<>();

        animationViewVisibility = new ObservableField<>(View.GONE);
        messageVisibility = new ObservableField<>(View.GONE);

        binding.compactcalendarView.setFirstDayOfWeek(1);

        setUpCalendar();
        fetchEventDates();

    }
    public void onNextClick(View view) {

        binding.compactcalendarView.showNextMonth();

    }

    public void onPreviousClick(View view) {

        binding.compactcalendarView.showPreviousMonth();

    }
    private void fetchEventDates() {

        animationViewVisibility.set(View.VISIBLE);
        Mentor.getInstance().fetchEventDates(context, new Consumer<Response<Mentor.FetchEventDatesContentData>>() {
            @Override
            public void accept(Response<Mentor.FetchEventDatesContentData> response) throws Exception {
                Log.e(TAG , "fetchDaySessions response success " );
                if(response.getResult()){
                    if(response.getContent().getData().eventsList.size() > 0){
                        Log.e(TAG , "event size : " + response.getContent().getData().eventsList.size() );
                        setUpCalendar();
                        addEventsToCalendar(response.getContent().getData().eventsList);
                        messageVisibility.set(View.GONE);
                    }
                    else{
                       // not sessions found
                        setMessage(context.getString(R.string.no_events_found));
                    }



                }else{

                    response.handleResponseErrorCode(context , CalendarFragmentVM.this);
                }

                animationViewVisibility.set(View.GONE);

            }
        } , this);
    }

    private void addEventsToCalendar(List<Mentor.FetchEventDatesContentData.Event> events) {

        for(Mentor.FetchEventDatesContentData.Event event : events){
            Event ev1 = new Event(Color.GREEN, milliseconds( event.eventDate ), "Teachers' Professional Day");
            binding.compactcalendarView.addEvent(ev1);
        }
    }

    private void setUpCalendar() {

        binding.fragmentMyCalenderMonthNameTv.setText(dateFormatMonth.format(binding.compactcalendarView.getFirstDayOfCurrentMonth()));

        Log.e(TAG, "firstDayOfNewMonth ; " + dateFormatMonth.format(binding.compactcalendarView.getFirstDayOfCurrentMonth()));

        binding.compactcalendarView.setUseThreeLetterAbbreviation(true);

        binding.compactcalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

                Log.e(TAG, "dateClicked:" + dateClicked.toString());
                Log.e(TAG, "dateClicked format:" + getDate( dateClicked.getTime(),"yyyy-MM-dd"));

                if(binding.compactcalendarView.getEvents(dateClicked).size() > 0){
                    Log.e(TAG, "getEvents : " + binding.compactcalendarView.getEvents(dateClicked).size());
//                    Toast.makeText(context, "Teachers' Professional Day", Toast.LENGTH_SHORT).show();
                    showSessionsListDialog( getDate(dateClicked.getTime() , "yyyy-MM-dd"));
                }else
                    Toast.makeText(context, context.getString(R.string.no_events_found), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {

                 String month =dateFormatMonth.format(firstDayOfNewMonth);

                Log.e(TAG , "firstDayOfNewMonth ; " + firstDayOfNewMonth);
                Log.e(TAG , "month ; " + month);

                binding.fragmentMyCalenderMonthNameTv.setText(month);

            }
        });
    }


    private void showSessionsListDialog(String time) {

        SessionListDialogFragment.newInstance(time).show(myCalendarFragment.getChildFragmentManager()  , "SessionListDialogFragment" );
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat ,  Locale.ENGLISH);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        return formatter.format(calendar.getTime());
    }

        public long milliseconds(String date) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd" , Locale.ENGLISH);
            try {
                Date mDate = sdf.parse(date);
                long timeInMilliseconds = mDate.getTime();
                System.out.println("Date in milli :: " + timeInMilliseconds);
                return timeInMilliseconds;
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return 0;
        }

    public void setMessage(String message) {
        Log.e(TAG , "displayMessage : " + message);
        animationViewVisibility.set(View.GONE);
        messageVisibility.set(View.VISIBLE);
        this.message.set( message);
    }

    @Override
    public void handleResponseErrorCodeForUser(String message) {

        animationViewVisibility.set(View.GONE);
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        setMessage(message);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);
        animationViewVisibility.set(View.GONE);
    }
}
