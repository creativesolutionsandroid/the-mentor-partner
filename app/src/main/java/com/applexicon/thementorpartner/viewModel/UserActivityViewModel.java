package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.applexicon.thementorpartner.R;
import com.applexicon.thementorpartner.utilities.LanguageUtils;
import com.applexicon.thementorpartner.views.activities.UserActivity;
import com.applexicon.thementorpartner.views.fragments.HelpFragment;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class UserActivityViewModel {


    private final UserActivity userActivity;
    private String TAG  = "SignInFragmentViewModel";
    public ObservableInt animationViewVisibility;

    public ObservableField<String>  lang;


    private Context context;

    public UserActivityViewModel(@NonNull UserActivity userActivity) {

        this.userActivity = userActivity;
        this.context = userActivity.getApplicationContext();
        animationViewVisibility = new ObservableInt(View.GONE);
        lang = new ObservableField<>(LanguageUtils.getUsedLang(context).equals("ar") ? "en" : "ع");
    }

    public void onHelpClicked(View view){
        Log.e(TAG , "onHelpClick");

        HelpFragment helpFragment = HelpFragment.newInstance();
        FragmentTransaction bt = userActivity.getSupportFragmentManager().beginTransaction();

        bt.replace(R.id.activityUser_container_rl , helpFragment , null);
        bt.addToBackStack("HelpFragment");
        bt.commit();
    }

    public void onLangClicked(View view){
        Log.e(TAG , "onLanguageClick");

        if(LanguageUtils.getUsedLang(context).equals("ar")){
            LanguageUtils.changeToEnglish(context.getApplicationContext());
        }else {
            LanguageUtils.changeToArabic(context.getApplicationContext());
        }

        UserActivity.start(userActivity);

        userActivity.finish();
    }


}
