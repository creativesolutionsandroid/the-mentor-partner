package com.applexicon.thementorpartner.viewModel;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableInt;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.applexicon.thementorpartner.models.ChangePassword;
import com.applexicon.thementorpartner.response.Response;
import com.applexicon.thementorpartner.views.fragments.ChangePasswordDialogFragment;
import com.applexicon.thementorpartner.databinding.FragmentDialogChangePasswordBinding;

import java.util.List;

import vendor.Errors.MyEditText;
import vendor.Errors.ValidationError;
import vendor.Validation.ConfirmPassword;
import vendor.Validation.IValidationHandler;
import vendor.Validation.IValidationRule;
import vendor.Validation.Minimum;
import vendor.Validation.Required;
import vendor.Validation.Validation;
import io.reactivex.functions.Consumer;

/**
 * Created by Hamdy on 7/24/2017.
 */

public class ChangePasswordFragmentDialogVM extends BaseObservable implements Response.ResponseErrorCodeHandler {


    private final FragmentDialogChangePasswordBinding binding;
    private final ChangePassword model;
    private final ChangePasswordDialogFragment changePasswordDialogFragment;
    private String TAG  = "NewPassFragDialogVM";
    private Context context;
    public final ObservableInt progressBar;
    private String confirmPassword;


    public ChangePasswordFragmentDialogVM(@NonNull final ChangePasswordDialogFragment changePasswordDialogFragment) {

        this.changePasswordDialogFragment = changePasswordDialogFragment;
        this.context = this.changePasswordDialogFragment.getActivity();
        this.binding = changePasswordDialogFragment.binding;

        progressBar = new ObservableInt(View.GONE);

         model = new ChangePassword();
    }

    public void setNewPassword(final String newPassword){
//        Log.e(TAG , "setCode : " + phone);

       model.setNewPassword(newPassword);

    }

    public void setOldPassword(final String oldPassword){
//        Log.e(TAG , "setCode : " + phone);

        model.setOldPassword(oldPassword);

    }
    public void setConfirmPassword(final String confirmPassword){
//        Log.e(TAG , "setCode : " + phone);

       this.confirmPassword = confirmPassword;

    }

    public  String getNewPassword(){
        return  model.getNewPassword();
    }
    public  String getOldPassword(){
        return  model.getOldPassword();
    }

    public  String getConfirmPassword(){
        return  confirmPassword;
    }

    private void onFieldValidationFailed(List<ValidationError> validationErrors) {
//        this.validationErrors.clear();
//        this.validationErrors = validationErrors;

        Log.e(TAG , "onFieldValidationFailed : " + validationErrors.toString());
        for (ValidationError validationError : validationErrors) {
            validationError.displayError();
        }
    }


    public void onSendClick(View view){

        progressBar.set(View.VISIBLE);

        Log.e(TAG , "onSendClick");

        Validation validation = new Validation();

        validation.addValidationField(

                new MyEditText(binding.fragmentDialogChangePassOldPasswordEt),
                new IValidationRule[]{
                        new Required( getOldPassword()),
                        new Minimum( getOldPassword(),5)
                });

        validation.addValidationField(

                new MyEditText(binding.fragmentDialogChangePassNewPasswordEt),
                new IValidationRule[]{
                        new Required(getNewPassword()),
                        new Minimum(getNewPassword(),5)
                });

        validation.addValidationField(
                new MyEditText(binding.fragmentDialogChangePassConfirmPasswordEt),
                new IValidationRule[]{
                        new Required( getConfirmPassword()),
                       new ConfirmPassword( getConfirmPassword(),getNewPassword())
                });

        validation.validate(context, new IValidationHandler() {
            @Override
            public void onValidationSuccessfull() {
                onNewPassValidationSuccessfull();
            }

            @Override
            public void onValidationFailed(List<ValidationError> validationErrors) {
                onFieldValidationFailed(validationErrors);
                progressBar.set(View.VISIBLE);
            }
        });
    }

    private void onNewPassValidationSuccessfull() {

        Log.e(TAG , "onValidationSuccessfull");

        model.send(context ,new Consumer<Response>() {
            @Override
            public void accept(Response response) throws Exception {
                Log.e(TAG , "Consumer<response>");
                onNewPasswordResponseSuccessfull(response);
                progressBar.set(View.GONE);
            }
        }, this);
    }

    private void onNewPasswordResponseSuccessfull(Response response) {
        Log.e(TAG , "onEditProfileResponseSuccessfull : " + response.toString());

        if(response.getResult()){
            Toast.makeText(context, response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
            this.changePasswordDialogFragment.dismiss();
        }
        else{

            Toast.makeText(context, response.getContent().getMessage(), Toast.LENGTH_SHORT).show();
        }
        progressBar.set(View.GONE);
    }


    @Override
    public void handleResponseErrorCodeForUser(String message) {
        Log.e(TAG , "handleResponseErrorCodeForUser : " + message);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        progressBar.set(View.GONE);
    }

    @Override
    public void handleResponseErrorCodeForDebug(String message) {
        Log.e(TAG , "handleResponseErrorCodeForDebug : " + message);


    }
}
