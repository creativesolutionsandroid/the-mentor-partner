package com.applexicon.thementorpartner.response;

import android.content.Context;
import android.util.Log;

import com.applexicon.thementorpartner.R;
import com.google.gson.annotations.SerializedName;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

public class Response<DATA> {

  @SerializedName("result")
  private Boolean result;

  @SerializedName("content")
  private ContentResponse<DATA> content;

  @SerializedName("errorCode")
  private int errorCode;
  private Throwable throwable;


  public Boolean getResult() {
    return result;
  }

  public void setResult(Boolean result) {
    this.result = result;
  }

  public ContentResponse<DATA> getContent() {
    return content;
  }

  public void setContent(ContentResponse<DATA> content) {
    this.content = content;
  }

  public int getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(int errorCode) {
    this.errorCode = errorCode;
  }

  public void handleResponseErrorCode(Context context , ResponseErrorCodeHandler responseErrorCodeHandler) {

    Log.e("TAG", "errorCode: "+errorCode);
    switch (errorCode){


      case ResponseErrorCode.ERROR_CONNECTION:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(throwable.toString());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(context.getString(R.string.no_internet_connection));
        break;

      case ResponseErrorCode.INVALID_OR_MISSING_APP_TOKEN:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(context.getString(R.string.an_error_occured));

        break;

      case ResponseErrorCode.ACCOUNT_NOT_ACTIVED:
        responseErrorCodeHandler.handleResponseErrorCodeForUser(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());

        break;

      case ResponseErrorCode.ACCOUNT_ALREADY_ACTIVE:
        responseErrorCodeHandler.handleResponseErrorCodeForUser(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        break;

      case ResponseErrorCode.INVALID_ACTIVE_CODE:
        responseErrorCodeHandler.handleResponseErrorCodeForUser(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        break;

      case ResponseErrorCode.INVALID_OR_MISSING_Data:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(context.getString(R.string.an_error_occured));

        break;

      case ResponseErrorCode.INVALID_OR_MISSING_PARAMETERS:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(context.getString(R.string.an_error_occured));

        break;

      case ResponseErrorCode.INVALID_OR_MISSING_User_TOKEN:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(context.getString(R.string.an_error_occured));
        break;

      case ResponseErrorCode.INVALID_PASSWORD:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        break;

      case ResponseErrorCode.Not_Available:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        break;
      case ResponseErrorCode.USED_BEFORE:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        break;

      case ResponseErrorCode.USER_NOT_FOUND:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(getContent().getMessage());
        break;

      case ResponseErrorCode.INVALID_CREDENTIALS:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(getContent().getMessage());
        break;

      case ResponseErrorCode.PHONE_NOT_VERIFIED:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(getContent().getMessage());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(getContent().getMessage());
        break;

      case ResponseErrorCode.HTTP_EXCEPTION:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(throwable.toString());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(context.getString(R.string.an_error_occured));
        break;

      case ResponseErrorCode.SERVER_ERROR:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(throwable.toString());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(context.getString(R.string.an_error_occured));
        break;

      case ResponseErrorCode.TIMEOUT_EXCEPTION:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(throwable.toString());
        responseErrorCodeHandler.handleResponseErrorCodeForUser(context.getString(R.string.an_error_occured));
        break;

      default:
        responseErrorCodeHandler.handleResponseErrorCodeForDebug(context.getString(R.string.an_error_occured));
        responseErrorCodeHandler.handleResponseErrorCodeForUser(context.getString(R.string.an_error_occured));
    }

  }

  public void setThroeable(Throwable throwable) {

    this.throwable = throwable;
    if(throwable instanceof HttpException){
      errorCode = ResponseErrorCode.HTTP_EXCEPTION;
    }

    if(throwable instanceof InternalError){
      errorCode = ResponseErrorCode.SERVER_ERROR;
    }

    if(throwable instanceof SocketTimeoutException){
      errorCode = ResponseErrorCode.TIMEOUT_EXCEPTION;
    }

    if(throwable instanceof ConnectException){
      errorCode = ResponseErrorCode.ERROR_CONNECTION;
    }
  }

    public interface ResponseErrorCodeHandler{

    public void handleResponseErrorCodeForUser(String message);
    public void handleResponseErrorCodeForDebug(String message);
  }

}
