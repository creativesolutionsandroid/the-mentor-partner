package com.applexicon.thementorpartner.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hamdy on 8/3/2017.
 */

public class ContentResponse<DATA> {

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private DATA data;

    @SerializedName("errors")
    private ResponseValidationErrors errors;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DATA getData() {
        return data;
    }

    public void setData(DATA data) {
        this.data = data;
    }

    public ResponseValidationErrors getErrors() {
        return errors;
    }

    public void setErrors(ResponseValidationErrors errors) {
        this.errors = errors;
    }
}
