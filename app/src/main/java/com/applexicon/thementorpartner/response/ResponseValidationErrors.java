package com.applexicon.thementorpartner.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hamdy on 8/6/2017.
 */

public class ResponseValidationErrors {

    @SerializedName("email")
    public List<String> emailErrors =new ArrayList<>() ;

    @SerializedName("name")
    public List<String> nameErrors;

    @SerializedName("phone")
    public List<String> phoneErrors = new ArrayList<>();

    @SerializedName("password")
    public List<String> passwordErrors;

    @SerializedName("yearsOfExperience")
    public List<String> yearsOfExperienceErrors = new ArrayList<>();
}
