package com.applexicon.thementorpartner.response;

/**
 * Created by Hamdy on 8/3/2017.
 */

public class ResponseErrorCode {


    public static final int  NO_ERRORS = 0;
    public static final int  INVALID_OR_MISSING_APP_TOKEN = 101;
    public static final int  INVALID_OR_MISSING_User_TOKEN = 102;
    public static final int  INVALID_OR_MISSING_Data = 103;
    public static final int  INVALID_CREDENTIALS = 104;
    public static final int  INVALID_OR_MISSING_PARAMETERS = 109;
    public static final int  ACCOUNT_NOT_ACTIVED = 301;
    public static final int  PHONE_NOT_VERIFIED = 108;
    public static final int  INVALID_ACTIVE_CODE = 303;
    public static final int  ACCOUNT_ALREADY_ACTIVE = 304;
    public static final int  INVALID_PASSWORD = 306;
    public static final int  USER_NOT_FOUND = 305;
    public static final int  USED_BEFORE = 308;
    public static final int  No_Data = 309;
    public static final int  Not_Available = 310;

    public static final int ACCOUNT_NOT_APPROVED = 6000;
    public static final int HTTP_EXCEPTION = 701;
    public static final int SERVER_ERROR = 702;
    public static final int TIMEOUT_EXCEPTION = 703;
    public static final int ERROR_CONNECTION = 0;
}
