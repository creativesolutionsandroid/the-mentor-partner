package vendor.Validation;

import android.content.Context;

/**
 * Created by Hamdy on 4/23/2017.
 */



/*
* to use this validation rule you must insert
* strings in strings file
*     <string name="validation_confirm_password">Confirm password not match</string>

*     <string name="validation_confirm_password">كلمة المرور غير مطابقة</string>

* */

public class ConfirmPassword implements IValidationRule {


    String password;
    String ConfirmPassword;

    public ConfirmPassword(String password , String ConfirmPassword) {
        this.password = password;
        this.ConfirmPassword = ConfirmPassword;
    }

    @Override
    public String validate( Context context) {

        String message = null;

        if(password != null && ConfirmPassword != null){

            if(!ConfirmPassword.equals(password))
                message = context.getString(context.getResources().getIdentifier("validation_confirm_password" , "string" , context.getPackageName()));

        }


        return message;
    }

}
