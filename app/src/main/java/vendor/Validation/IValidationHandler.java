package vendor.Validation;

import java.util.List;

import vendor.Errors.ValidationError;


/**
 * Created by Hamdy on 4/23/2017.
 */

public interface IValidationHandler {


    void onValidationSuccessfull();
    void onValidationFailed(List<ValidationError> validationErrors);
}
