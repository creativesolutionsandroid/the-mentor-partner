package vendor.Validation;


import vendor.Errors.IVewError;

/**
 * Created by Hamdy on 4/23/2017.
 */

public class ValidationField {


    public IVewError view;
    public IValidationRule[] validationRules;

    public ValidationField(IVewError view, IValidationRule[] validationRules) {

        this.view = view;
        this.validationRules = validationRules;
    }


}
