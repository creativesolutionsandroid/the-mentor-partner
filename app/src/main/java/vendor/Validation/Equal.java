package vendor.Validation;

import android.content.Context;


/**
 * Created by Hamdy on 4/23/2017.
 */


/*
* to use this validation rule you must insert
* strings in strings file
 *     <string name="validation_max">The number of characters must be less than or equal to</string>
 *     <string name="validation_max">عدد الحروف يجب ان تكون أقل من او تساوي</string>
* */

public class Equal implements IValidationRule {


    private  String messsage;
    private  String value;
    private int equal;

    public Equal(String value ,int equal) {
        this.equal = equal;
        this.value = value;
    }

    public Equal(String value  ,int equal , String message) {
        this.equal = equal;
        this.messsage = message;
        this.value = value;
    }

    @Override
    public String validate( Context context) {

        String message = null;
        if(value != null){

            if(value.length() != equal)
                message = context.getString(context.getResources().getIdentifier("validation_equal" , "string" , context.getPackageName())) + " " + equal;
        }

        return message;
    }


}
