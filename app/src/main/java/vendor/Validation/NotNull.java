package vendor.Validation;

import android.content.Context;
import android.util.Log;


/**
 * Created by Hamdy on 4/23/2017.
 */


/*
* to use this validation rule you must insert
* strings in strings file
 *          <string name="validation_required">هذا الحقل مطلوب</string>
 *           <string name="validation_required">This field is required</string>
* */


public class NotNull implements IValidationRule {
    private  final String TAG = "Required" ;
    private final Object value;


    public NotNull(Object value) {
        this.value =value;
    }


    @Override
    public String validate(Context context) {

        Log.e(TAG , "value : " + value);

        String message = null;

        if(value == null){
            message = context.getString(context.getResources().getIdentifier("validation_required" , "string" , context.getPackageName()));
        }

        return message;
    }
}
