package vendor.Validation;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import vendor.Errors.ValidationError;
import vendor.Errors.IVewError;


/**
 * Created by Hamdy on 4/23/2017.
 */

public class Validation {


    private  final String TAG = "Validation";
    private ArrayList<ValidationField> validationFields = new ArrayList<>();

    private ArrayList<ValidationError> validationErrors;

    public Validation addValidationField(IVewError vewError , IValidationRule[] rules){

        validationFields.add( new ValidationField( vewError , rules));

        return this;
    }



    public void validate(Context context , IValidationHandler iValidationHandler){

        int errorNo = 0;
        validationErrors = new ArrayList<>();

        for(int i = 0 ; i < validationFields.size() ; i++ ){

            ValidationField validationField =  validationFields.get(i);

            IValidationRule[] rules = validationField.validationRules;

            String error = "";
            for(int j = 0 ; j < rules.length ; j++){
                 String mess = rules[j].validate(context);
                Log.e(TAG , "error : " + mess);
                if(mess != null) {
                    errorNo++;
                    error = mess;
//                    error += mess + "\n";
                    break;
                }
            }

            if(!error.equals(""))
                validationErrors.add(new ValidationError(error , validationField.view));
            else
                validationErrors.add(new ValidationError(null , validationField.view));
        }


        if (errorNo > 0)
            iValidationHandler.onValidationFailed(validationErrors);
        else {
            resetErrors();
            iValidationHandler.onValidationSuccessfull();
        }

    }

    private void resetErrors() {

        for(int i = 0; i < validationErrors.size() ; i++){
            validationErrors.get(i).displayError();
        }
    }

}
