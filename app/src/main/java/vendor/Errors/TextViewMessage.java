package vendor.Errors;

import android.widget.TextView;


/**
 * Created by Hamdy on 4/24/2017.
 */

public class TextViewMessage implements IVewError {

    private TextView textView;

    public TextViewMessage(TextView textView) {
        this.textView = textView;
    }

    @Override
    public void setError(String message) {
        textView.setText(message);
    }
}
